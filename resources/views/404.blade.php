@extends('base')
@section('content')
	@asset('css/page-404.min.css')
	<!-- page-404.twig -->
	<div class="page-404">
		<div class="wrapper">
			<div class="container">
				<div class="col-12">

					{{-- Page title --}}
					<div class="page__heading">
						<h1>Ops... qualcosa è andato storto!</h1>
					</div>

					{{-- Page content --}}
					<div class="page__content editorial">
						<p>La pagina che stavi cercando non esiste...</p>
						<p><strong>Cerca</strong> un contenuto</p>
						<div class="search-wrapper">
							<form method="get" action="{!! home_url( '/' ) !!}" role="search">
								@include('components.partials.partial-innersearch-form',['innersearch_type' => 'page-404','search_text' => 'Cerca','search_placeholder' => 'Cerca in Agrodolce'])
							</form>
						</div>
						<p>oppure, puoi <a class="cta" href="/">tornare alla Homepage</a>.</p>
					</div>

				</div>
			</div>
		</div>
	</div>

@endsection

@extends('base')
@section('content')
	<!-- archive.twig -->

	@asset('css/archive.min.css')


	<div class="archive">
		<div class="heading__info">
			{!! $get_breadcrumb !!}
		</div>
		<div class="archive__heading">
			<h1>{!! get_the_archive_title() !!}</h1>
			@if(get_the_archive_description())
				<p class="abstract">{!! get_the_archive_description() !!}</p>
			@endif
		</div>
		<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
			<div class="col-8">
				<section class="section-1-columns">
					<div class="col-12">
						@if ( have_posts() )
							@while (have_posts()) @php the_post() @endphp
							@php global $wp_query; $ad_loop = is_object( $wp_query ) && property_exists( $wp_query, 'current_post' ) ? $wp_query->current_post : ''; @endphp
							@includeFirst(['components.partials.partial-card-'.ad_get_post_type().'-list','components.partials.partial-card-post-list'], ['ad_loop' => $ad_loop] )
							@endwhile
						@endif
					</div>
				</section>
			</div>
			<aside class="col-4">
				@include('components.partials.partial-sticky-adv')
			</aside>
		</div>

		<div class="container">
			@include('components.partials.partial-pagination')
		</div>
	</div>

@endsection

@extends('base')
@section('content')
	@asset('css/page-menu.min.css')
	<div class="page page-menu">
		<div class="container">
			<div
				class="col-12">

				{{-- Page title --}}
				<div class="page__heading">
					<h1>{!! get_the_archive_title() !!}</h1>
				</div>

				<div class="page__index">
					Passa a:
					{!! $get_nav !!}
				</div>

				{{-- Page content --}}
				<div class="page__content editorial">
					{!! $get_menu !!}
				</div>
			</div>
		</div>
	</div>

@endsection

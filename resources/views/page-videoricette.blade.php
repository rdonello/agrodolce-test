{{-- /* Template Name: Videoricette */ --}}
@extends('base')
@section('content')
	<!-- archive.twig -->
	@asset('css/archive.min.css')
	<div class="archive">
		<div class="archive__heading">
			<h1>{!! get_the_title() !!}</h1>
			@if( get_the_excerpt() )
				<p class="abstract">{!! get_the_excerpt() !!}</p>
			@endif
		</div>
		<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
			<div class="col-8">
				<section class="section-1-columns">
					<div class="col-12">
						@if ( $get_videoricetta->have_posts() )
							@while ($get_videoricetta->have_posts()) @php $get_videoricetta->the_post() @endphp
							@includeFirst(['components.partials.partial-card-'.ad_get_post_type().'-list','components.partials.partial-card-post-list'],['ad_loop' => $get_videoricetta->current_post])
							@endwhile
							@php wp_reset_postdata(); @endphp
						@endif
					</div>
				</section>
			</div>
			<aside class="col-4">
				@include('components.partials.partial-sticky-adv')
			</aside>
		</div>

		<div class="container">
			@include('components.partials.partial-pagination')
		</div>
	</div>

@endsection

@extends('base')
@if($paged > 1)
@section('content')
	@asset('css/archive.min.css')
	@include('components.custom.custom-archive-loop')
@endsection
@else
	{{-- HEAD --}}
@section('content')
	@asset('css/homepage-categoria.min.css')

	<!-- homepage-categoria.twig -->
	<div class="homepage-categoria">

		{{-- Static adv - Mobile top --}}
		@include('components.partials.partial-static-adv')

		{{-- Homepage Title --}}
		<section class="homepage-categoria__section">
			<div class="container title">
				<h1>{!! get_the_archive_title() !!}</h1>
				@if(get_the_archive_description())
				<p class="abstract">{!! get_the_archive_description() !!}</p>
				@endif
			</div>
		</section>

		{{-- Hero --}}
		<section class="homepage-categoria__section">
			<div class="container">
				@include('components.sections.section-1-columns', array('blocks' => array(['template' => 'partial-card-post_type-hero','posts' => $get_post_top,'lazyload' => false])))
			</div>
		</section>

		{{-- Card big loop + card list loop + adv --}}
		<section class="homepage-categoria__section">
			<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
				<div class="col-8">
					<div class="container">
						@include('components.sections.section-2-columns', array('blocks' => array(['template' => 'partial-card-post_type-big', 'posts' => $get_latest_spa, 'lazyload' => false])))
					</div>
					<div class="container">
						@include('components.sections.section-2-columns', array('blocks' => array(['template' => 'partial-card-post_type-big', 'posts' => $get_latest_spb, 'lazyload' => false])))
					</div>
				</div>
				<div class="col-4">
					{{-- @include('components.partials.partial-promobox') --}}
					@include('components.partials.partial-sticky-adv')
				</div>
			</div>
		</section>

		@if($featured_specials)
		<!-- Speciali -->
			<section class="homepage-categoria__section speciali">
				<div class="container">
					<section class="section-title--block">
						<div class="partial-title-medium">
							@asset('css/components/partials/partial-title-medium.min.css')
							<div class="title-medium__content">
								<h2 class="title-medium__title">Gli speciali di Agrodolce</h2>
								<span class="bottom_line"></span>
							</div>
						</div>
					</section>
				</div>
				<div class="container">
					@include('components.partials.partial-list-stories')
				</div>
			</section>
		@endif

		<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
			<div class="col-8">
				@include('components.sections.section-1-columns',array('blocks' => array(
							['template' => 'partial-card-post-list--k2','posts' => $get_archive_posts,'post_number' => 10]
							)))
			</div>
			<aside class="col-4">
				@include('components.partials.partial-sticky-adv')
			</aside>
		</div>

		<div class="container">
			@include('components.partials.partial-pagination')
		</div>

	</div>
@endsection
@endif

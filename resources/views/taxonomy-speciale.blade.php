@if($paged > 1)
	@include('archive')
	@php exit() @endphp
@else
@extends('base-essential')
@section('content')
	<!-- single-speciale.twig -->
	@asset('css/single-speciale.min.css')

	<div class="single-speciale">
		<div class="single-speciale__info">
			<div class="wrapper">
				<div class="container">
					<div class="single-speciale__heading">
						<span class="label">speciale</span>
						{{-- speciale title --}}
						<h1>{!! get_the_archive_title() !!}</h1>
					</div>
					{{-- Descrizione speciale --}}
					<p class="abstract">{!! get_the_archive_description() !!}</p>
					@if($sponsor)
						{{-- Sponsor speciale --}}
						<p class="sponsor">sponsored by
							<strong>{!! $sponsor['name'] !!}</strong>
						</p>
					@endif
				</div>
			</div>
		</div>

		<section class="single-speciale__section">
			<div class="single-speciale__image">
				<div
						class="container">
					{{-- speciale featured image --}}
					<div class="featured-image">
						<picture>
							<!--[if IE 9]>
							<video style="display: none;"><![endif]-->
							<source srcset="{!! tbm_wp_get_attachment_image_url($term_image_id,array(430,327)) !!}, {!! tbm_wp_get_attachment_image_url($term_image_id,array(860,654)) !!} 2x"
									media="(max-width: 736px)"/>
							<!--[if IE 9]></video><![endif]-->
							<img srcset="{!! tbm_wp_get_attachment_image_url($term_image_id,array(1150,647)) !!}, {!! tbm_wp_get_attachment_image_url($term_image_id,array(2300,1294)) !!} 2x"
								 src="{!! tbm_wp_get_attachment_image_url($term_image_id,array(1150,647)) !!}"
								 alt="{!! tbm_get_the_post_thumbnail_alt($term_image_id) !!}"/>
						</picture>
						{{-- Social share --}}
						@include('components.partials.partial-social')

					</div>
				</div>
			</div>
			<div class="single-speciale__top-posts">
				<div class="wrapper">
					<div class="container">
						@include('components.sections.section-2-3-columns', array('blocks' => array(['template' => 'partial-card-post_type-big', 'posts' => $special_featured_post, 'lazyload' => false])))
					</div>
				</div>
			</div>
		</section>
		<div class="wrapper">
			<div class="container">
				<div class="col-12">
					<div
							class="editorial">
						{{-- Term content --}}
						{!! $term_content !!}
					</div>
				</div>
			</div>
			<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
				<div class="col-8">
					<section class="section-1-columns">
						<div class="col-12">
							@if ( have_posts() )
								@while (have_posts()) @php the_post() @endphp
								@includeFirst(['components.partials.partial-card-'.ad_get_post_type().'-list','components.partials.partial-card-post-list'])
								@endwhile
							@endif
						</div>
					</section>
				</div>
				<div class="col-4">
					@include('components.partials.partial-sticky-adv')
				</div>
			</div>
		</div>
		<div class="container">
			@include('components.partials.partial-pagination')
		</div>
	</div>

@endsection
@endif

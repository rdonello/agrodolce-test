@extends('base')
@section('content')
	@asset('css/search.min.css')
	<!-- search.twig -->
	<div class="search">
		<div class="search__heading">
			<h1>Risultati della ricerca
				@if (is_paged())
					<span>pagina {!! $paged !!}</span>
				@endif
			</h1>
			@if (!have_posts())
				<p class="abstract">Hai cercato <strong>{!! get_search_query(); !!}</strong>. Siamo spiacenti, non
					abbiamo trovato nulla. Prova ad utilizzare parole più generiche o a modificare la ricerca.</p>
			@else
				<p class="abstract">Hai cercato
					<strong>{!! get_search_query(); !!}</strong>. La tua ricerca ha prodotto
					<strong>{!! App::tbm_count_posts() !!}</strong> risultati.</p>
			@endif
		</div>

		<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
			<div class="col-8">
				@while (have_posts()) @php the_post() @endphp
				@includeFirst([
				'components.partials.partial-card-'.ad_get_post_type().'-list',
				'components.partials.partial-card-post-list'])
				@endwhile
			</div>
			<aside class="col-4">
				@include('components.partials.partial-sticky-adv')
			</aside>
		</div>

		<div class="container">
			@include('components.partials.partial-pagination')
		</div>
	</div>

@endsection

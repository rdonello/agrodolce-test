@extends('base')
@section('content')
	<!-- archive.twig -->
	@asset('css/archive.min.css')
	@while(have_posts()) @php the_post() @endphp
	<div class="archive">
		<div class="archive__heading">
			<h1>{!! get_the_title() !!}</h1>

		</div>
		@if(get_the_content())
			<div class="editorial">
				{!! the_content() !!}
			</div>
		@endif
		<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
			<div class="col-8">
				<section class="section-1-columns">
					<div class="col-12">
						@if ( $menu_posts->have_posts() )
							@while ($menu_posts->have_posts()) @php $menu_posts->the_post() @endphp
							@includeFirst(['components.partials.partial-card-'.ad_get_post_type().'-list','components.partials.partial-card-post-list'],['ad_loop' =>  $menu_posts->current_post])
							@endwhile
						@endif
					</div>
				</section>
			</div>
			<aside class="col-4">
				@include('components.partials.partial-sticky-adv')
			</aside>
		</div>

		<div class="container">
			@include('components.partials.partial-pagination')
		</div>
	</div>
	@endwhile
@endsection

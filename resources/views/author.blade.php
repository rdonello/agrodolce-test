@extends('base-essential')
@section('content')
	<!-- single-autore.twig -->
	@asset('css/author.min.css')
	<div class="single-autore">
		<div class="single-autore__info">
			<div class="wrapper">
				<div class="container">
					<div class="col-12">
						<div class="single-autore__heading">
							<div class="heading__info">
								<ul>
									<li class="info__category">
										<a href="#">Autori</a>
									</li>
								</ul>
							</div>

							{{-- autore title --}}
							<h1>{!! $author_data->display_name !!}</h1>
						</div>
					</div>
				</div>

				<div class="container">
					<div
							class="col-4">
						{{-- autore featured image --}}
						<div class="featured-image">
							<picture>
								<!--[if IE 9]>
								<video style="display: none;"><![endif]-->
								<source srcset="{!! $author_data->thumb !!}, {!! $author_data->thumb_2x !!} 2x"
										media="(max-width: 736px)"/>
								<!--[if IE 9]></video><![endif]-->
								<img srcset="{!! $author_data->thumb !!}, {!! $author_data->thumb_2x !!} 2x"
									 src="{!! $author_data->thumb !!}"
									 alt="{!! $author_data->display_name !!}"
								/>
							</picture>
						</div>
					</div>
					<div
							class="col-8">
						{{-- Descrizione autore --}}
						<p class="abstract">{!! $author_data->description !!}</p>
					</div>
				</div>

			</div>
		</div>
		<div class="wrapper">
			<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
				<div class="col-8">
					@if ( have_posts() )
						<section class="section-1-columns">
							<div class="col-12">
								@while (have_posts()) @php the_post() @endphp
								@includeFirst(['components.partials.partial-card-'.ad_get_post_type().'-list', 'components.partials.partial-card-post-list'])
								@endwhile
							</div>
						</section>
					@endif
					{{-- Pagination --}}
					@include('components.partials.partial-pagination')
				</div>
				<aside class="col-4">
					@include('components.partials.partial-sticky-adv')
				</aside>
			</div>
		</div>
	</div>
@endsection

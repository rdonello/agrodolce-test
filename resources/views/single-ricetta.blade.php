@extends('base-essential')
@section('content')
	<!-- single-ricetta.twig -->
	@asset('css/single-ricetta.min.css')
	<div class="single-ricetta">

		<article class="single-ricetta__article">
			<div class="single-ricetta__info">
				<div class="wrapper">
					<div class="container">
						<div class="col-12">
							<div class="single-ricetta__heading">
								<div class="heading__info">
									{!! $get_breadcrumb !!}
								</div>

								{{-- ricetta title --}}
								<h1>{!! get_the_title() !!}</h1>
							</div>

							{{-- Descrizione ricetta --}}
							<p class="abstract">@php the_excerpt() @endphp</p>
						</div>
					</div>
				</div>
			</div>

			<div class="single-ricetta__ingredients">
				<div class="container">
					<div class="col-12">
						@if($print_highlight_video)
							{{-- Video embed --}}
							<div class="single-ricetta__hero">
								<div class="single-ricetta__embed">
									{!! $print_highlight_video !!}
								</div>
								{{-- Social share --}}
								@include('components.partials.partial-social')
							</div>
						@else
							{{-- Immagine ricetta --}}
							<div class="featured-image">
								<picture>
									<!--[if IE 9]>
									<video style="display: none;"><![endif]-->
									<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x"
											media="(max-width: 736px)"/>
									<!--[if IE 9]></video><![endif]-->
									<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1150,647)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(2300,1294)) !!} 2x"
										 alt="{!! tbm_get_the_post_thumbnail_alt(get_the_ID()) !!}"/>
								</picture>
								{{-- Social share --}}
								@include('components.partials.partial-social')
								@if ($image_credit)
									{!! $image_credit !!}
								@endif
							</div>
						@endif
					</div>
				</div>
				<div class="wrapper">
					<div class="container">
						<div class="col-12">
							{{-- Ingredienti ricetta --}}
							<div class="ingredients__content">
								<div class="ingredients__heading">
									<h2>Ingredienti
										@if($ricette_ingrediente_persone)
											per {!! $ricette_ingrediente_persone !!} persone
										@endif</h2>
									<ul>
										<li>
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
												 viewbox="0 0 20 24">
												<g transform="translate(-2)">
													<path d="M13,4.051V2h3a1,1,0,0,0,0-2H8A1,1,0,0,0,8,2h3V4.051A10.013,10.013,0,0,0,2,14v9a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V14a10.013,10.013,0,0,0-9-9.949ZM12,6a8,8,0,1,1-8,8,8,8,0,0,1,8-8Z"
														  fill="#ff007b"/>
													<rect width="7" height="2" transform="translate(11 13)"
														  fill="#ff007b"/>
												</g>
											</svg>
											<span>{!! $tempo_preparazione !!}</span>
										</li>
										<li>
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
												 viewbox="0 0 28 24">
												<g transform="translate(-0.6 -2)">
													<rect width="4" height="24" rx="1" transform="translate(0.6 2)"
														  fill="#ff007b"/>
													<rect width="4" height="18" rx="1" transform="translate(6.6 8)"
														  fill="#ff007b"/>
													<rect width="4" height="18" rx="1" transform="translate(18.6 8)"
														  fill="#ff007b"/>
													<rect width="4" height="12" rx="1" transform="translate(12.6 14)"
														  fill="#ff007b"/>
													<rect width="4" height="12" rx="1" transform="translate(24.6 14)"
														  fill="#ff007b"/>
												</g>
											</svg>
											<span>{!! $calorie !!} cal x 100g</span>
										</li>
										<li>
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
												 viewbox="0 0 24 24">
												<path d="M13,21H5v2a.945.945,0,0,0,1,1H18a.945.945,0,0,0,1-1V21Z"
													  fill="#ff007b"/>
												<path d="M18,4h-.3A5.99,5.99,0,0,0,12,0,6.087,6.087,0,0,0,6.3,4H6A5.991,5.991,0,0,0,5,15.9V19h6V15h2v4h6V15.9A5.991,5.991,0,0,0,18,4Z"
													  fill="#ff007b"/>
											</svg>
											<span>{!! $difficolta !!}</span>
										</li>
									</ul>
								</div>
								<ul class="ingredients__list">
									@foreach($get_ingredients as $ingredient)
										<li class="ingredients__item">
											<a href="{!! $ingredient['url'] !!}">{!! $ingredient['nome'] !!}</a>
											@if($ingredient['spec'])
												{!! $ingredient['spec'] !!}
											@endif
											@if($ingredient['quantita'])
												{!! $ingredient['quantita'] !!} {!!$ingredient['unita'] !!}
											@else
												quanto basta
											@endif

										</li>
									@endforeach
								</ul>
							</div>
							@if($get_products_list)
								@include('components.partials.partial-innerrelated-products', ['products' => $get_products_list])
							@endif

							{{-- Autore e data ricetta --}}
							<div class="heading__detail">
								<span class="author__name">di {!! tbm_get_the_author() !!}</span>
								<span > • {!! tbm_get_pub_date() !!}</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			{{-- Testo ricetta --}}
			<div class="single-ricetta__content">
				<div class="wrapper">
					<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
						<div class="col-8">
							<div class="editorial">
								{{-- Post content --}}
								@php the_content() @endphp

								{{-- Preparazione ricette --}}
								@if($ricette_preparazione)
									<div class="partial-innerrelated-ricetta">
										@asset('css/components/partials/partial-innerrelated-ricetta.min.css')
										<h2>Preparazione {!! $nome_ricetta !!}</h2>
										{!! $ricette_preparazione !!}
									</div>
								@endif

								{{-- Conclusione --}}
								@if($ricette_conclusione)
									<p id="conclusione">{!! $ricette_conclusione !!}</p>
								@endif

								{{-- Preparazione variante --}}
								@if($ricette_variante)
									<h2>Variante {!! $nome_ricetta !!}</h2>
									<p id="variante">{!! $ricette_variante !!}</p>
								@endif

								{{-- Fonte della notizia --}}
								@if($get_source)
									{!! $get_source !!}
								@endif
							</div>
							@include('components.partials.partial-newsletter')
						</div>
						<div class="col-4">
							@include('components.partials.partial-sticky-adv')
						</div>
					</div>
				</div>
			</div>
		</article>
		@if ($related_recipes_object && $related_recipes_object->have_posts())
			<div class="wrapper">
				<div class="container">
					<section class="section-title--block">
						<div class="partial-title-medium">
							@asset('css/components/partials/partial-title-medium.min.css')
							<div class="title-medium__content">
								<h2 class="title-medium__title">Altre ricette
									@if($related_recipes_term)
										con
										<a href="{!!$related_recipes_term['url'] !!}">{!! $related_recipes_term['name'] !!}</a>
									@endif
								</h2>
								<span class="bottom_line"></span>
							</div>
						</div>
					</section>
					<div class="container-cycle-col-3">
						@while($related_recipes_object->have_posts()) @php $related_recipes_object->the_post() @endphp
						@include('components.partials.partial-card-ricetta-small')
						@endwhile
						@php wp_reset_postdata(); @endphp
					</div>
				</div>
			</div>
		@endif
	</div>

@endsection

@extends('base-essential')
@section('content')
	<!-- single-gallery.twig -->

	@asset('css/single-gallery.min.css')
	@while (have_posts()) @php the_post() @endphp

	<div class="single-gallery">
		<article class="single-gallery__article">

			<div class="single-gallery__info">
				<div class="wrapper">
					<div class="container">
						<div class="col-12">
							{{-- Gallery heading --}}
							<div class="single-gallery__heading">
								{{-- Gallery category and trend --}}
								<div class="heading__info">
									{!! $get_breadcrumb !!}
								</div>

								{{-- Gallery title --}}
								<h1>{!! get_the_title() !!}</h1>

								{{-- Gallery author and datetime --}}
								<div class="heading__detail">
									<span class="author__name">di {!! tbm_get_the_author() !!}</span>
									<span > • {!! tbm_get_pub_date() !!}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="single-gallery__gallery">
						<section>
							<div class="single-gallery-slider gallery-slider" data-slidesperview="1">
								<!-- featured image -->
								@include('components.sections.fullwidth-gallery')
								<!-- end featured image -->
							</div>
						</section>
					</div>
				</div>
			</div>
			<div class="single-gallery__abstract">
				<div class="wrapper">
					<div class="container">
						<div class="col-12">
							{{-- Gallery abstract --}}
							<p class="abstract">@php the_excerpt() @endphp</p>
						</div>
					</div>
				</div>
			</div>
			<div class="wrapper">
				<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
					<div class="col-8">
						<div class="editorial">
							{{-- Gallery content --}}
							@php the_content() @endphp
						</div>
						@include('components.partials.partial-newsletter')
					</div>
					<aside class="col-4">
						@include('components.partials.partial-sticky-adv')
					</aside>
				</div>
			</div>
		</article>
	</div>
	@endwhile
@endsection

@extends('base-essential')

@section('content')
	<!-- single-podcast.twig -->

	@asset('css/single-podcast.min.css')

	<div class="single-podcast">
		<article class="single-podcast__article">

			<div class="single-podcast__info">
				<div class="wrapper">
					<div class="container">
						<div
								class="col-12">
							{{-- Podcast heading --}}
							<div
									class="single-podcast__heading">
								{{-- Podcast category and trend --}}
								<div class="heading__info">
									{!! $get_breadcrumb !!}
								</div>

								{{-- Podcast title --}}
								<h1>{!! get_the_title() !!}</h1>

								{{-- Podcast author and datetime --}}
								<div class="heading__detail">
									<span class="author__name">di {!! tbm_get_the_author() !!}</span>
									<span > • {!! tbm_get_pub_date() !!}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div
						class="container">
					{{-- Podcast player --}}
					<div class="single-podcast__podcast">
						<div class="podcast__podcast-wrapper">

							<!-- just for testing - embed -->
							{!! $get_podcast_embed !!}
						</div>
					</div>
				</div>
			</div>


			<div class="single-podcast__abstract">
				<div class="wrapper">
					<div class="container">
						<div
								class="col-12">
							{{-- Podcast abstract --}}
							<p class="abstract">@php the_excerpt() @endphp</p>
						</div>
					</div>
				</div>
			</div>
			<div class="wrapper">
				<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
					<div class="col-8">
						<div
								class="editorial">
							{{-- Podcast content --}}
							@php the_content() @endphp
							@if ($get_source)
								{!! $get_source !!}
							@endif
						</div>
						@include('components.partials.partial-newsletter')
					</div>
					<aside class="col-4">
						@include('components.partials.partial-sticky-adv')
					</aside>
				</div>
			</div>
		</article>

	</div>
@endsection

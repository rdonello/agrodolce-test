@if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() )
	@include('components.partials.partial-innerrelated-amp')
@else
	@php
		global $post;
		$ad_code = '';
		$related = array();

		/**
		 * Print a random video related to $post
		 *
		 * @param $post Post object
		 */
		function _tbm_print_random_video( $post ) {

			$ad_code = '';

			$related = tbm_get_custom_related_posts( $post, 1, null, 'storia', 'video' );

			if ( $related && isset( $related[0] ) && ! empty( $related[0]->ID ) ) {

				$post_id = $related[0]->ID;

				if ( function_exists( 'html_video_generate_brid_video_player' ) && get_field( 'video_cnvid_id', 'option' ) ) {
					$ad_code = html_video_generate_brid_video_player( $post_id, '', false );
				} else if ( get_field( 'tbm_videoid', $post_id ) && filter_var( get_field( 'tbm_videoid', $post_id )['url'], FILTER_VALIDATE_URL ) ) {
					$ad_code = '<video src="' . get_field( 'tbm_videoid', $post_id )['url'] . '" controls controlsList="nodownload"></video>';
				}

				if ( $ad_code ) {
					echo '<div><div class="video-post video-post--aside">' . $ad_code . '</div><h3>' . get_the_title($post_id) . '</h3></div>';
				}

			}
		}

		// Se non ho disabilitato la visualizzazione del video, eseguo la funzione
		if ( ! get_field( 'video_single_disabled', $post->ID ) ) {
			_tbm_print_random_video( $post );
		}


	@endphp
@endif

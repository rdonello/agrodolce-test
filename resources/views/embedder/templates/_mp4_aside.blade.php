@if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() )
	@include('components.partials.partial-innerrelated-amp')
@else
	@include('components.partials.partial-innerrelated-playervideo--aside')
@endif

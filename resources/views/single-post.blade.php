@extends('base-essential')
@section('content')
	<!-- single-post.twig -->
	@asset('css/single-post.min.css')
	@while(have_posts()) @php the_post() @endphp
	<div class="single-post">
		<article class="single-post__article">
			<div class="wrapper">
				<div class="container">
					<div class="col-12">
						<div class="single-post__heading">
							{{-- Post category and trend --}}
							<div class="heading__info">
								{!! $get_breadcrumb !!}
							</div>

							{{-- Post title --}}
							<h1>{!! get_the_title() !!}</h1>

							{{-- Post author and datetime --}}
							<div class="heading__detail">
								<span class="author__name">di {!! tbm_get_the_author() !!}</span>
								<span > • {!! tbm_get_pub_date() !!}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="col-12">

					@if($print_highlight_video)
						{{-- Video embed --}}
						<div class="single-post__hero">
							<div class="single-post__embed">
								{!! $print_highlight_video !!}
							</div>
							{{-- Social share --}}
							@include('components.partials.partial-social')
						</div>

					@else

						{{-- Post featured image --}}
						<div class="featured-image">
							{!! tbm_agrodolce_get_rubrica() !!}
							<picture>
								<!--[if IE 9]>
								<video style="display: none;"><![endif]-->
								<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x"
										media="(max-width: 736px)"/>
								<!--[if IE 9]></video><![endif]-->
								<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1150,647)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(2300,1294)) !!} 2x"
									 alt="{!! tbm_get_the_post_thumbnail_alt(get_the_ID()) !!}"/>
							</picture>
							{{-- Social share --}}
							@include('components.partials.partial-social')
							@if ($image_credit)
								{!! $image_credit !!}
							@endif
						</div>
				</div>
				@endif
			</div>

			<div class="wrapper">
				<div class="container">
					<div class="col-12">
						<div class="single-post__abstract">
							{{-- Post abstract --}}
							<p class="abstract">@php the_excerpt() @endphp</p>
						</div>
					</div>
				</div>
			</div>
			<div class="wrapper">
				<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
					<div class="col-8">
						<div class="editorial">
							{{-- Post content --}}
							@php the_content() @endphp
							@if ($get_source)
								{!! $get_source !!}
							@endif
						</div>
						@include('components.partials.partial-newsletter')

						{{-- Content Revolution --}}
						<div id="div-gpt-ad-cr"></div>

					</div>
					<aside class="col-4">
						@include('components.partials.partial-sticky-adv',['banner' => 'desktop_top'])
					</aside>
				</div>
			</div>
		</article>
		@endwhile

		@if($related_object && $related_object->have_posts())
			<div class="wrapper">
				<div class="container">
					<section class="section-title--block">
						<div class="partial-title-medium">
							@asset('css/components/partials/partial-title-medium.min.css')
							<div class="title-medium__content">
								<h2 class="title-medium__title">Ti potrebbe interessare</h2>
								<span class="bottom_line"></span>
							</div>
						</div>
					</section>
					<div class="container-cycle-col-3">
						@while($related_object->have_posts()) @php $related_object->the_post() @endphp
						@include('components.partials.partial-card-post-big')
						@endwhile
						@php wp_reset_postdata(); @endphp

					</div>
				</div>
			</div>

	</div>
	@endif

@endsection

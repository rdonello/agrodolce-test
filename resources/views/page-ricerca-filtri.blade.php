<!-- page-ricerca-filtri.twig -->
@extends('base')
{% block wpbodyclass %}page ricerca
@endsection
{% import "components/partials/forms.twig" as forms %}

@section('content')
	<link
	rel="stylesheet" href="@@url/css/page-ricerca-filtri@css_extension.css"/>

	<!-- page-ricerca-filtri.twig -->
	<div
		class="page-ricerca-filtri">

		{{-- Static adv - Mobile top --}}
		@include('components.partials.partial-static-adv')

		{{-- Homepage Title --}}
		<section class="page-ricerca-filtri__section">
			<div class="wrapper">
				<div class="container">
					<h1>Ricette</h1>
				</div>
			</div>
		</section>

		{{-- Card Big Loop --}}
		<section class="page-ricerca-filtri__section">
			<div class="wrapper">
				<div class="container">
					<div class="search-wrapper">
						@include('components.partials.partial-search-filters-form')
					</div>
					<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
						<div class="col-4">
							@include('components.partials.partial-search-filters-list')

							@include('components.partials.partial-sticky-adv')
						</div>
						<div class="col-8">
							<div class="container-cycle-col-2">
								{% for queryItem in ricette.items.children %}
									{% if loop.index <= 6 %}
										@include('components.partials.partial-card-ricetta-small')
									{% endif %}
								{% endfor %}
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="wrapper">
				<div class="container">
					<section class="section-title--block">
						<div class="partial-title-medium">
							@asset('css/components/partials/partial-title-medium.min.css')
							<div class="title-medium__content">
								<h2 class="title-medium__title">Altre ricette con
									<a href="#">spigola</a>
								</h2>
								<span class="bottom_line"></span>
							</div>
						</div>
					</section>
					<div class="container-cycle-col-3">
						{% for queryItem in ricette.items.children %}
							{% if loop.index <= 6 %}
								@include('components.partials.partial-card-ricetta-small')
							{% endif %}
						{% endfor %}
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection

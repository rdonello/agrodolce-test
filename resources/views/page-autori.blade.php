@extends('base')

@section('content')
	@asset('css/page.min.css')

	<!-- page.twig -->
	<div class="page">
		<div class="container">
			<div
					class="col-12">

				{{-- Page title --}}
				<div class="page__heading">
					<h1>Tutti gli autori</h1>
				</div>

				{{-- Page content --}}
				<div class="col-8">
					<div class="container">
						<section class="section-1-columns">
							<div class="col-12">
								@asset('css/components/partials/partial-card-author-list.min.css')
								@foreach($authors_list as $author)
									<div class="partial-card-author-list">
										@if(!empty($author->image) && !empty($author->image['url']))
											<div class="card-author-list__figure">
												<div class="card-author-list__image-wrapper">
													<picture data-link="{!! $author->page_url !!}">
														<img class="lazyload"
															 data-srcset="{!! tbm_wp_get_attachment_image_url($author->image['ID'],array(430,327)) !!}, {!! tbm_wp_get_attachment_image_url($author->image['ID'],array(860,654)) !!} 2x"
															 alt="{!! $author->display_name !!}"/>
													</picture>
												</div>
											</div>
										@endif
										<div class="card-author-list__content">
											<a class="card-author-list__title" href="{!! $author->page_url !!}">
												<h3>{!! $author->display_name !!}</h3>
											</a>
											<p class="card-author-list__abstract">{!! $author->description !!}</p>
										</div>
									</div>
								@endforeach
							</div>
						</section>
					</div>
				</div>
				<div class="col-4">
					{{-- @include('components.partials.partial-promobox') --}}

					@include('components.partials.partial-sticky-adv')
				</div>
			</div>
		</div>
		<div class="container">
			@include('components.partials.partial-pagination')
		</div>
	</div>

@endsection

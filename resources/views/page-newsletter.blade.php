@php /* Template Name: Newsletter */ @endphp
@extends('base')
@section('content')
	@asset('css/page-newsletter.min.css')
	@while(have_posts()) @php the_post() @endphp
	<!-- page-newsletter.twig -->
	<div class="page-newsletter">
		<div class="container">
			<div class="col-12">
				<main class="page-newsletter__main">

					{{-- Page newsletter --}}
					<article class="editorial">
						{{-- Page title --}}
						<h1>{!! get_the_title() !!}</h1>
					</article>
					@if(function_exists('html_newsletter_get_page_template'))
						{!! html_newsletter_get_page_template() !!}
					@else
						@if(is_user_logged_in())
							<p class="warning">Plugin Newsletter non attivato</p>
						@else
							<p class="warning">L'iscrizione alla newsletter è attualmente sospesa. Ci scusiamo per il disagio.</p>
						@endif
					@endif
				</main>
			</div>
		</div>
	</div>
	@endwhile
@endsection

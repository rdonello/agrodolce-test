{{-- /* Template Name: Mappa del sito */ --}}
@extends('base')
@section('content')
	@asset('css/archive-categorie.min.css')
	<!-- archive-categorie.twig -->
	<div class="archive-categorie">
		<div class="archive-categorie__heading">
			<h1>{!! get_the_title() !!}</h1>
		</div>

		<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
			<div class="col-12">
					{!! $print_parent_categories !!}
					{!! $print_child_categories !!}
					{!! $print_orphan_stories !!}
			</div>
		</div>
	</div>

@endsection

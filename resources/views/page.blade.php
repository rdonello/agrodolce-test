@extends('base')
@section('content')
	@asset('css/page.min.css')
	<!-- page.twig -->
	<div class="page">
		<div class="container">
			<div class="col-12">
				{{-- Page title --}}
				<div class="page__heading">
					<h1>{!! get_the_title() !!}</h1>
				</div>

				{{-- Page content --}}
				<div class="page__content editorial">
					@php the_content() @endphp
				</div>
			</div>
		</div>
	</div>
@endsection

@extends('base-essential')
@section('content')
	<!-- single-video.twig -->

	@asset('css/single-video.min.css')

	<div class="single-video">
		<article class="single-video__article">

			<div class="single-video__info">
				<div class="wrapper">
					<div class="container">
						<div
							class="col-12">
							{{-- video heading --}}
							<div
								class="single-video__heading">
								{{-- video category and trend --}}
								<div class="heading__info">
									{!! $get_breadcrumb !!}
								</div>

								{{-- video title --}}
								<h1>{!! get_the_title() !!}</h1>

								{{-- video author and datetime --}}
								<div class="heading__detail">
									<span class="author__name">di {!! tbm_get_the_author() !!}</span>
									<span > • {!! tbm_get_pub_date() !!}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div
					class="container">
					{{-- Video embed --}}
					<div class="single-video__hero">
						<div class="single-video__embed">
							{!! $print_video !!}
						</div>
						{{-- Social share --}}
						@include('components.partials.partial-social')

					</div>
				</div>
			</div>
			<div class="single-video__abstract">
				<div class="wrapper">
					<div class="container">
						<div class="col-12">
							{{-- video abstract --}}
							<p class="abstract">@php the_excerpt() @endphp</p>
						</div>
					</div>
				</div>
			</div>
			<div class="wrapper">
				<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
					<div class="col-8">
						<div class="editorial">
							{{-- video content --}}
							@php the_content() @endphp
						</div>
						@include('components.partials.partial-newsletter')
					</div>
					<aside class="col-4">
						@include('components.partials.partial-sticky-adv')
					</aside>
				</div>
			</div>
		</article>

	</div>
@endsection

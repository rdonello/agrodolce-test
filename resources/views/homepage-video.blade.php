<!-- homepage-video.twig -->
@extends('base')
{% block wpbodyclass %}home video
@endsection
{% import "components/partials/forms.twig" as forms %}

@section('content')
	<link
			rel="stylesheet" href="@@url/css/homepage-video@css_extension.css"/>

	<!-- homepage-video.twig -->
	<div
			class="homepage-video">

		{{-- Static adv - Mobile top --}}
		@include('components.partials.partial-static-adv')

		{{-- Homepage Title --}}
		<section class="homepage-video__section">
			<div class="wrapper">
				<div class="container">
					<h1>Video</h1>
				</div>
			</div>
		</section>

		{{-- Card Big Loop --}}
		<section class="homepage-video__section">
			<div class="wrapper">
				<div class="container">
					<div class="search-wrapper">
						<form method="get" action="some_php" role="search">
							{{ forms.innersearch('search', '', 'text', 'Cerca...') }}
						</form>
						<p>
							<strong>32</strong>
							risultati per
							<a href="#" alt="">Pizza Romana</a>
						</p>
					</div>
					<div class="container-cycle-col-3">
						{% for queryItem in videos.items.children %}
						{% if loop.index <= 12 %}
						@include('components.partials.partial-card-video-big')
						{% endif %}
						{% endfor %}
					</div>
					{{-- Pagination --}}
					@include('components.partials.partial-pagination')
				</div>
			</div>
		</section>
	</div>
@endsection

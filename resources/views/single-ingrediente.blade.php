@extends('base-essential')
{% import "components/partials/forms.twig" as forms %}
{% block wpbodyclass %}single single-ingrediente
@endsection

@section('content')
	<!-- single-ingrediente.twig -->
	{% set queryItem = ingredienti.items.children[0] %}

	@asset('css/single-ingrediente.min.css')

	<div class="single-ingrediente">
		<article class="single-ingrediente__article">

			<div class="single-ingrediente__info">
				<div class="wrapper">

					<div class="container">
						<div class="single-ingrediente__heading">
							<div class="heading__info">
								<ul>
									<li class="info__category">
										<a href="#">Ricette</a>
									</li>
									<li class="info__trend">
										<a href="#">Ingredienti</a>
									</li>
								</ul>
							</div>

							{{-- ingrediente title --}}
							<h1>{{ queryItem.name }}</h1>
						</div>
					</div>

					<div class="container">
						<div class="single-ingrediente__abstract">
							<div
								class="col-8">
								{{-- Descrizione ingrediente --}}
								<p class="abstract">@php the_excerpt() @endphp</p>
							</div>
							<div
								class="col-4">
								{{-- Ingrediente featured image --}}
								<div class="featured-image">
									<picture>
										<!--[if IE 9]><video style="display: none;"><![endif]-->
										<source
										data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,300)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,600)) !!} 2x" media="(max-width: 736px)"/>
										<!--[if IE 9]></video><![endif]-->
										<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,300)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,600)) !!} 2x" alt="{!! get_the_title() !!}"/>
									</picture>
									{{-- Social share --}}
									@include('components.partials.partial-social')

								</div>
							</div>
						</div>
					</div>

					<div class="container">
						<div class="single-ingrediente__search">
							<div class="col-12">
								<div class="inner-search">
									<form method="get" action="some_php" role="search">
										{{ forms.innersearch('search', '', 'text', 'Cerca ricette') }}
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="wrapper">
				<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
					<div class="col-8">
						<div
							class="editorial">
							{{-- Post content --}}
							{{ queryItem.content }}
						</div>
						@include('components.partials.partial-newsletter')
						{#% include 'components/partials/partial-video-adv.twig' %#}
					</div>
					<aside class="col-4">
						@include('components.partials.partial-sticky-adv')
					</aside>
				</div>
			</div>
		</article>

		<div class="wrapper">
			<div class="container">
				<section class="section-title--block">
					<div class="partial-title-medium">
						@asset('css/components/partials/partial-title-medium.min.css')
						<div class="title-medium__content">
							<h2 class="title-medium__title">Altre ricette con
								<a href="#">{{ queryItem.name }}</a>
							</h2>
							<span class="bottom_line"></span>
						</div>
					</div>
				</section>
				<div class="container-cycle-col-3">
					{% for queryItem in ricette.items.children %}
						{% if loop.index <= 6 %}
							@include('components.partials.partial-card-ricetta-small')
						{% endif %}
					{% endfor %}
				</div>
			</div>
		</div>
	</div>

@endsection

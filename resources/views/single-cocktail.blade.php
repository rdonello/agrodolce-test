@extends('base-essential')
@section('content')
	<!-- single-cocktail.twig -->
	@asset('css/single-cocktail.min.css')
	<div class="single-cocktail">

		<article class="single-cocktail__article">
			<div class="single-cocktail__info">
				<div class="wrapper">
					<div class="container">
						<div class="col-12">
							<div class="single-cocktail__heading">
								<div class="heading__info">
									{!! $get_breadcrumb !!}
								</div>

								{{-- cocktail title --}}
								<h1>{!! the_title() !!}</h1>
							</div>

							{{-- Descrizione cocktail --}}
							<p class="abstract">@php the_excerpt() @endphp</p>
						</div>
					</div>
				</div>
			</div>

			<div class="single-cocktail__ingredients">
				<div class="container">
					<div class="col-12">
						@if($print_highlight_video)
							{{-- Video embed --}}
							<div class="single-cocktail__hero">
								<div class="single-cocktail__embed">
									{!! $print_highlight_video !!}
								</div>
								{{-- Social share --}}
								@include('components.partials.partial-social')
							</div>
						@else
							{{-- Immagine ricetta --}}
							<div class="featured-image">
								<picture>
									<!--[if IE 9]>
									<video style="display: none;"><![endif]-->
									<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x"
											media="(max-width: 736px)"/>
									<!--[if IE 9]></video><![endif]-->
									<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1150,647)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(2300,1294)) !!} 2x"
										 alt="{!! get_the_title() !!}"/>
								</picture>
								{{-- Social share --}}
								@include('components.partials.partial-social')
								@if ($image_credit)
									{!! $image_credit !!}
								@endif
							</div>
						@endif
					</div>
					<div class="wrapper">
						<div class="container">
							<div class="col-12">
								{{-- Ingredienti cocktail --}}
								<div class="ingredients__content">
									<div class="ingredients__heading">
										<h2>Ingredienti</h2>
										<ul>
											<li>
												<svg xmlns:svg="http://www.w3.org/2000/svg" width="20" height="20"
													 id="svg2472" viewbox="0 0 362.48 363.27">
													<g id="g2482">
														<path id="path2484" fill="#ff007b"
															  d="m0 0h362.48l-157.52 194.57v124.61h87.2c29.25 0 29.5 44.09 0 44.09h-217.5c-29.504 0-29.504-44.09-0.004-44.09h85.714v-125.57l-160.37-193.61z"/>
													</g>
												</svg>
												<span>{!! $bicchiere !!}</span>
											</li>
											<li>
												<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
													 viewbox="0 0 24 24">
													<path d="M13,21H5v2a.945.945,0,0,0,1,1H18a.945.945,0,0,0,1-1V21Z"
														  fill="#ff007b"/>
													<path d="M18,4h-.3A5.99,5.99,0,0,0,12,0,6.087,6.087,0,0,0,6.3,4H6A5.991,5.991,0,0,0,5,15.9V19h6V15h2v4h6V15.9A5.991,5.991,0,0,0,18,4Z"
														  fill="#ff007b"/>
												</svg>
												<span>{!! $difficolta !!}</span>
											</li>
										</ul>
									</div>
									@if(!empty($get_ingredients))
										<ul class="ingredients__list">
											@foreach($get_ingredients as $ingredient)
												<li class="ingredients__item">
													<a href="{!! $ingredient['url'] !!}">{!! $ingredient['nome'] !!}</a>
													@if($ingredient['spec'])
														{!! $ingredient['spec'] !!}
													@endif
													@if($ingredient['quantita'])
														{!! $ingredient['quantita'] !!} {!!$ingredient['unita'] !!}
													@else
														quanto basta
													@endif

												</li>
											@endforeach
										</ul>
									@endif

								</div>
								{{-- Autore e data cocktail --}}
								<div class="heading__detail">
									<span class="author__name">di {!! tbm_get_the_author() !!}</span>
									<span > • {!! tbm_get_pub_date() !!}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			{{-- Testo cocktail --}}
			<div class="single-cocktail__content">
				<div class="wrapper">
					<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
						<div class="col-8">
							<div class="editorial">
								{{-- Post content --}}
								@php the_content() @endphp

								{{-- Preparazione cocktail --}}
								@if($ricette_preparazione)
									<div class="partial-innerrelated-cocktail">
										@asset('css/components/partials/partial-innerrelated-cocktail.min.css')
										<h2>Preparazione {!! get_the_title() !!}</h2>
										{!! $ricette_preparazione !!}
									</div>
								@endif

								{{-- Conclusione --}}
								@if($ricette_conclusione)
									<p id="conclusione">{!! $ricette_conclusione !!}</p>
								@endif

								{{-- Preparazione variante --}}
								@if($ricette_variante)
									<h2>Variante {!! $nome_ricetta !!}</h2>
									<p id="variante">{!! $ricette_variante !!}</p>
								@endif

								{{-- Fonte della notizia --}}
								@if($get_source)
									{!! $get_source !!}
								@endif
							</div>
							@include('components.partials.partial-newsletter')
						</div>
						<div class="col-4">
							@include('components.partials.partial-sticky-adv')
						</div>
					</div>
				</div>
			</div>
		</article>

		<div class="wrapper">
			<div class="container">
				<section class="section-title--block">
					<div class="partial-title-medium">
						@asset('css/components/partials/partial-title-medium.min.css')
						<div class="title-medium__content">
							<h2 class="title-medium__title">Altri cocktail
								@if($get_categoria_cocktail)
									di
									<a href="{!! $get_categoria_cocktail['url'] !!}">{!! $get_categoria_cocktail['name'] !!}</a>
								@endif
							</h2>
							<span class="bottom_line"></span>
						</div>
					</div>
				</section>
				<div class="container-cycle-col-3">
					@if ($related_cocktails && $related_cocktails->have_posts())
						@while($related_cocktails->have_posts()) @php $related_cocktails->the_post() @endphp
						@include('components.partials.partial-card-cocktail-small', ['lazyload' => false])
						@endwhile
						@php wp_reset_postdata(); @endphp
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection

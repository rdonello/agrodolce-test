{{-- /* Template Name: Lifestyle */ --}}
@if(is_paged())
	@include('archive')
	@php exit() @endphp
@else
	@extends('base')
@section('content')
	@asset('css/homepage-secondary.min.css')

	<!-- front-page.twig -->
	<div class="homepage-secondary">

		{{-- Static adv - Mobile top --}}
		@include('components.partials.partial-static-adv')

		<section class="homepage-secondary__section">
			<div class="wrapper">
				<div class="container">
					<h1>{!! get_the_title() !!}</h1>
				</div>
			</div>
		</section>

		{{-- Hero --}}
		<section class="homepage-secondary__section">
			<div class="wrapper">
				<div class="container">
					<div class="search-wrapper">
						@include('components.partials.partial-innersearch-form',['innersearch_type' => 'page-lifestyle','search_text' => 'Cerca','search_placeholder' => 'Cerca un articolo'])
					</div>
					@include('components.sections.section-1-columns', array('blocks' => array(['template' => 'partial-card-post_type-hero','posts' => $hp_lifestyle_pp,'lazyload' => false])))
				</div>
			</div>
		</section>

		{{-- Secondo piano --}}
		<section class="homepage-secondary__section">
			<div class="wrapper">
				<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
					<div class="col-8">
						<div class="container-cycle-col-2">
							@if($hp_lifestyle_sp->have_posts())
								@while ($hp_lifestyle_sp->have_posts()) @php /** @noinspection PhpUndefinedVariableInspection */ $hp_lifestyle_sp->the_post() @endphp
								@includeFirst(array('components.partials.partial-card-'.ad_get_post_type().'-small','components.partials.partial-card-post-small'), ['lazyload' => false])
								@endwhile
								@php wp_reset_postdata(); @endphp
							@endif
						</div>
					</div>
					<div class="col-4">
						@include('components.partials.partial-sticky-adv')
					</div>
				</div>
			</div>
		</section>

		{{-- Speciali --}}
		@if($featured_specials)
			<section class="homepage-magazine__section speciali">
				<div class="container">
					<section class="section-title--block">
						<div class="partial-title-medium">
							@asset('css/components/partials/partial-title-medium.min.css')
							<div class="title-medium__content">
								<h2 class="title-medium__title">Gli speciali di Agrodolce</h2>
								<span class="bottom_line"></span>
								<a class="more" href="{!! get_page_link( get_page_by_path( 'speciali' ) ) !!}">Leggi tutto</a>
							</div>
						</div>
					</section>
				</div>
				<div class="container">
					@include('components.partials.partial-list-stories')
				</div>
			</section>
		@endif

		{{-- Loop delle categorie piano --}}
		@if($hp_categories)
			@foreach($hp_categories as $hp_category)
			<!-- {!! $hp_category['name'] !!} -->
				<section class="homepage-secondary__section">
					<div class="container">
						<section class="section-title--block">
							<!-- partial-title-medium.twig -->
							<div class="partial-title-medium">
								@asset('css/components/partials/partial-title-medium.min.css')
								<div class="title-medium__content">
									<h2 class="title-medium__title">{!! $hp_category['name'] !!}</h2>
									<span class="bottom_line"></span>
									<a class="more" href="{!! $hp_category['url'] !!}">Leggi tutto</a>
								</div>
							</div>
						</section>
					</div>
					<div class="container container-cycle-col-2">
						@if(method_exists($hp_category['query'],'have_posts'))
							@while ($hp_category['query']->have_posts()) @php /** @noinspection PhpUndefinedVariableInspection */ $hp_category['query']->the_post() @endphp
							@includeFirst(array('components.partials.partial-card-'.ad_get_post_type().'-big','components.partials.partial-card-post-big'), ['lazyload' => false])
							@endwhile
							@php wp_reset_postdata(); @endphp
						@endif
					</div>
				</section>
			@endforeach
		@endif

		{{-- Paginazione --}}
		<div class="container">
			@include('components.partials.partial-pagination')
		</div>
	</div>
@endsection
@endif

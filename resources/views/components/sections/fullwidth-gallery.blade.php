	<div class="fullwidth-gallery fullwidth-gallery-js">
		@asset('css/components/sections/fullwidth-gallery.min.css')
	<!-- in page fotogallery slider -->

	<div class="featured-image__box-image featured-image__box-image--V2">
		<div class="single-slider-annuncio__container swiper-container" tabindex="0" aria-label="fotogallery slider">
			<div class="swiper-wrapper">
					@foreach($photos_acf as $photo)
					<div class="swiper-slide swiper-slide--small">
						<picture>
							<!--[if IE 9]><video style="display: none;"><![endif]-->
							<source data-srcset="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(430,327)) !!}, {!! tbm_wp_get_attachment_image_url($photo['ID'],array(860,654)) !!} 2x" media="(max-width: 736px)"/>
							<!--[if IE 9]></video><![endif]-->
							<img class="lazyload" data-srcset="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(1150,647)) !!}, {!! tbm_wp_get_attachment_image_url($photo_array['ID'],array(2300,1294)) !!} 2x" alt="Immagine di {!! get_the_title() !!}" aria-haspopup="true" aria-controls="featured-image--fullwidth-image" arial-labelledby=".swiper-slide-caption"/>
						</picture>
						@if(is_singular(HTML_GALLERY_POST_TYPE))
						<div class="swiper-slide--small__content">
							@if($photo['title'])
							<h4>{!! $photo['title'] !!}</h4>
							@endif
							@if($photo['caption'])
							<label class="swiper-slide-caption" role="caption">{!! $photo['caption'] !!}</label>
							@endif
						</div>
						@endif
					</div>
					@endforeach
			</div>
		</div>

		{{-- Social share --}}
		@include('components.partials.partial-social')

		<nav class="single-gallery-slider__nav">
			<ul>
				<li><span role="button" aria-label="successiva" title="successiva" class="button button--next"></span></li>
				<li><span href="#" role="button" aria-label="precedente" title="precedente" class="button button--prev"></span></li>
			</ul>
		</nav>

		<div class="swiper-pagination--small"></div>
	</div>


	<!-- popup fotogallery slider -->

	<div class="featured-image--fullwidth-image lazyload" aria-hidden="true" role="presentation">
		<div class="featured-image__box-image swiper-container" tabindex="0" aria-label="fotogallery popup slider" aria-labelledby=".fullwidth-image-caption">

			<div class="swiper-wrapper">
				@foreach($photos_acf as $photo)
					<div class="swiper-slide swiper-slide--big">
						<!-- <picture> -->
						<img data-src="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(3200,1800)) !!}" class="swiper-lazy" alt="{!! $photo['title'] !!}"/>
						<!-- </picture> -->
						<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
					</div>
				@endforeach
			</div>

		</div>

		<div class="fullwidth-image__footer fullwidth-image-caption">
			<div>
				<div class="swiper-pagination--big"></div>
				<h4 class="fullwidth-image__footer__titles"></h4>
				<label></label>
			</div>
		</div>
		<nav class="single-gallery-slider__nav">
			<ul>
				<li>
					<span role="button" aria-label="espandi" title="espandi" class="button button--collapse"></span>
				</li>
				<li>
					<span role="button" aria-label="successiva" title="successiva" class="button button--next button-slider--next"></span>
				</li>
				<li>
					<span role="button" aria-label="precedente" title="precedente" class="button button--prev button-slider--prev"></span>
				</li>
			</ul>
		</nav>
	</div>
	<div class="fullwidth-gallery__overlay"></div>

</div>

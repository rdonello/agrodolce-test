<!-- section-2-3-columns.twig -->
 <section class="section-2-3-columns">
	@foreach ($blocks as $block)
		@if(method_exists($block['posts'],'have_posts'))
			@php $count = 0; @endphp
			@while ($block['posts']->have_posts()) @php $block['posts']->the_post() @endphp
				@if ($count == 0)
	 <div class="col-8">
				@else
	 <div class="col-4">
				@endif
				@includeFirst(['components.partials.' . str_replace('post_type',ad_get_post_type(), $block['template']),'components.partials.' . str_replace('post_type', 'post', $block['template'])],['l    azyload'=> isset($block['lazyload']) ? $block['lazyload'] : false ])
	 </div>
				@php $count++; @endphp
			@endwhile
		@endif
	@endforeach
 </section>

<!-- section-2-columns.twig -->
<pre>
@php //print_r($blocks);exit;@endphp
</pre>
 <section class="section-2-columns">
    @foreach ($blocks as $block)
		@while ($block['posts']->have_posts()) @php $block['posts']->the_post() @endphp
	 <div class="col-6">
            @include('components.partials.' . str_replace('post_type', ad_get_post_type(), $block['template']))
	 </div>
        @endwhile
	@endforeach
 </section>

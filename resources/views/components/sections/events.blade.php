<!-- events.twig section -->
<div class="section-events">

	{% if loop.index <= 1 and sectionevents is not defined or loop.index is not defined and sectionevents is not defined %}

		@asset('css/components/sections/events.min.css')

	{% endif %}

	<div class="events__last">
		{% set queryItem = events.items.children[0] %}
		@include('components.partialspartial-card-evento-hero')
	</div>
	<div class="events__others">
		{% for queryItem in events.items.children %}
			{% if loop.index <= 2 %}
				@include('components.partialspartial-card-evento-big')
			{% endif %}
		{% endfor %}
	</div>

	<a class="btn" href="#" alt="">Tutti gli eventi</a>

</div>

<!-- section-title--block.twig -->

<section class="section-title--block">
    {% for key, queryItem in categories.items.children %}
        {% if key == query.categoryName %}
            @include('components.partialspartial-title-medium')
        {% endif %}
    {% endfor %}
</section>

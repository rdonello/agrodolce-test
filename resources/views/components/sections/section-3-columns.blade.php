<!-- section-3-columns.twig -->

 <section class="section-3-columns">
	 <div class="col-4">
         @foreach ($blocks as $block)
             {% if loop.index <= query.col1.componentNumber %}
                 {% include '../partials/' ~ query.col1.component ~ '.twig' %}
             {% endif %}
         {% endfor %}
	 </div>
	 <div class="col-4">
         {% for queryItem in query.col2.type.items.children %}
             {% if loop.index <= query.col2.componentNumber %}
                 {% include '../partials/' ~ query.col2.component ~ '.twig' %}
             {% endif %}
         {% endfor %}
	 </div>
     <div class="col-4">
         {% for queryItem in query.col3.type.items.children %}
             {% if loop.index <= query.col3.componentNumber %}
                 {% include '../partials/' ~ query.col3.component ~ '.twig' %}
             {% endif %}
         {% endfor %}
	 </div>
 </section>

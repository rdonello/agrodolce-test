<!-- section-1-columns.twig -->
 <section class="section-1-columns">
	 <div class="col-12">
         @foreach ($blocks as $block)
			 @if(method_exists($block['posts'],'have_posts'))
				@while ($block['posts']->have_posts()) @php $block['posts']->the_post() @endphp
					@includeFirst(['components.partials.' . str_replace('post_type',ad_get_post_type(), $block['template']),'components.partials.' . str_replace('post_type', 'post', $block['template'])],['l    azyload'=> isset($block['lazyload']) ? $block['lazyload'] : false ])
				@endwhile
			 @endif
         @endforeach
	 </div>
 </section>

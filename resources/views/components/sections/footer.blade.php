<!--footer.twig section -->
<footer class="footer">
	<div class="wrapper">
		<div class="container">
			<div class="span-8">
				<div class="container">
					<div class="col-4">
						@php dynamic_sidebar('footer_sidebar') @endphp
					</div>
					<div class="col-8">
						<h3>Rimani sempre aggiornato</h3>
						<a class="btn btn--newsletter" href="{!! get_page_link( get_page_by_path( 'newsletter' ) ) !!}">Iscriviti alla newsletter →</a>

						<h3>I nostri canali social</h3>
						<div class="social__list">
							<ul>
							@if(tbm_yoast_social_accounts('fb'))
								<li><a href="{!! tbm_yoast_social_accounts('fb'); !!}"><svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewbox="0 0 42 42"><g transform="translate(-1054 -561)"><g transform="translate(1054 561)" stroke="#fff" fill="#2D0008" stroke-width="1"><rect width="42" height="42" stroke="none"/><rect x="0.5" y="0.5" width="41" height="41" fill="none"/></g><g transform="translate(1067 573.999)"><path d="M15.116,0H.883A.883.883,0,0,0,0,.884V15.117A.883.883,0,0,0,.883,16H8.546V9.8H6.461V7.39H8.546V5.609a2.909,2.909,0,0,1,3.105-3.192,17.293,17.293,0,0,1,1.863.095v2.16H12.235c-1,0-1.2.477-1.2,1.176V7.389h2.392L13.118,9.8H11.039V16h4.077A.884.884,0,0,0,16,15.117V.884A.883.883,0,0,0,15.116,0Z" transform="translate(0 0)" fill="#fff"/></g></g></svg></a></li>
							@endif
							@if(tbm_yoast_social_accounts('tw'))
								<li><a href="https://twitter.com/{!! tbm_yoast_social_accounts('tw'); !!}"><svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewbox="0 0 42 42"><g transform="translate(-1104 -561)"><g transform="translate(1104 561)" stroke="#fff" fill="#2D0008" stroke-width="1"><rect width="42" height="42" stroke="none"/><rect x="0.5" y="0.5" width="41" height="41" fill="none"/></g><g transform="translate(1117 527.5)"><g transform="translate(0 48)"><path d="M16,49.539a6.839,6.839,0,0,1-1.89.518,3.262,3.262,0,0,0,1.443-1.813,6.555,6.555,0,0,1-2.08.794A3.28,3.28,0,0,0,7.8,51.281a3.377,3.377,0,0,0,.076.748A9.284,9.284,0,0,1,1.114,48.6a3.281,3.281,0,0,0,1.008,4.384,3.239,3.239,0,0,1-1.482-.4v.036a3.3,3.3,0,0,0,2.628,3.223,3.274,3.274,0,0,1-.86.108,2.9,2.9,0,0,1-.621-.056,3.311,3.311,0,0,0,3.065,2.285,6.591,6.591,0,0,1-4.067,1.4A6.144,6.144,0,0,1,0,59.528,9.234,9.234,0,0,0,5.032,61a9.272,9.272,0,0,0,9.336-9.334c0-.145-.005-.285-.012-.424A6.544,6.544,0,0,0,16,49.539Z" transform="translate(0 -48)" fill="#fff"/></g></g></g></svg></a></li>
							@endif
							@if(tbm_yoast_social_accounts('ig'))
								<li><a href="{!! tbm_yoast_social_accounts('ig'); !!}"><svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewbox="0 0 42 42"><g id="Raggruppa_1" data-name="Raggruppa 1" transform="translate(-1104 -561)"><g id="Rettangolo_1" data-name="Rettangolo 1" transform="translate(1104 561)" stroke="#fff" fill="#2D0008" stroke-width="1"><rect width="42" height="42" stroke="none"/><rect x="0.5" y="0.5" width="41" height="41" fill="none"/></g><g id="Raggruppa_2" data-name="Raggruppa 2" transform="translate(850.551 329.769)"><g id="Raggruppa_3" data-name="Raggruppa 3" transform="translate(266.449 244)"><path id="Tracciato_1" data-name="Tracciato 1" d="M16.408,4.7a5.847,5.847,0,0,0-.372-1.941A4.1,4.1,0,0,0,13.694.422,5.863,5.863,0,0,0,11.753.05C10.9.009,10.624,0,8.452,0s-2.445.009-3.3.047A5.849,5.849,0,0,0,3.212.419a3.9,3.9,0,0,0-1.419.925A3.94,3.94,0,0,0,.871,2.761,5.863,5.863,0,0,0,.5,4.7C.458,5.558.449,5.83.449,8S.458,10.448.5,11.3a5.847,5.847,0,0,0,.372,1.941,4.1,4.1,0,0,0,2.341,2.341,5.863,5.863,0,0,0,1.941.372C6,15.994,6.276,16,8.449,16s2.445-.009,3.3-.047a5.846,5.846,0,0,0,1.941-.372,4.093,4.093,0,0,0,2.342-2.341A5.867,5.867,0,0,0,16.4,11.3c.037-.853.047-1.125.047-3.3s0-2.445-.041-3.3Zm-1.441,6.534a4.386,4.386,0,0,1-.275,1.485,2.655,2.655,0,0,1-1.519,1.519,4.4,4.4,0,0,1-1.485.275c-.844.038-1.1.047-3.232.047s-2.392-.009-3.233-.047a4.384,4.384,0,0,1-1.485-.275,2.462,2.462,0,0,1-.919-.6,2.488,2.488,0,0,1-.6-.919,4.4,4.4,0,0,1-.275-1.485c-.038-.844-.047-1.1-.047-3.233s.009-2.392.047-3.232a4.384,4.384,0,0,1,.275-1.485,2.432,2.432,0,0,1,.6-.919,2.484,2.484,0,0,1,.919-.6A4.4,4.4,0,0,1,5.225,1.5c.844-.037,1.1-.047,3.232-.047s2.392.009,3.233.047a4.386,4.386,0,0,1,1.485.275,2.461,2.461,0,0,1,.919.6,2.487,2.487,0,0,1,.6.919,4.4,4.4,0,0,1,.275,1.485c.037.844.047,1.1.047,3.232s-.009,2.385-.047,3.229Zm0,0" transform="translate(-0.449 0)" fill="#fff"/><path id="Tracciato_2" data-name="Tracciato 2" d="M128.983,124.5a4.034,4.034,0,1,0,4.034,4.034A4.035,4.035,0,0,0,128.983,124.5Zm0,6.65a2.617,2.617,0,1,1,2.617-2.617,2.617,2.617,0,0,1-2.617,2.617Zm0,0" transform="translate(-120.98 -120.531)" fill="#fff"/><path id="Tracciato_3" data-name="Tracciato 3" d="M364.407,89.581a.979.979,0,1,1-.979-.979A.979.979,0,0,1,364.407,89.581Zm0,0" transform="translate(-351.163 -85.839)" fill="#fff"/></g></g></g></svg></a></li>
							@endif
							</ul>
						</div>
						<a class="btn" href="https://triboo.com">Pubblicità →</a>
					</div>
				</div>
			</div>
			<div class="span-4">
				<div class="container">
					<div class="logo">
						<a href="/">
							<svg xmlns="http://www.w3.org/2000/svg" width="28.968" height="47.437" viewbox="0 0 28.968 47.437"><path d="M20.93,2.62H8.038L0,50.057H12.457l.135-12.981A11.791,11.791,0,0,1,9.716,29.74c-.089-3.038.945-12.149.945-12.149,0-.354.071-.637.425-.637s.421.287.421.637l.216,9.914c0,.354.071.641.421.641s.425-.287.425-.641L13,17.591c0-.354.071-.637.425-.637s.421.287.421.637l.216,9.914c0,.354.071.641.425.641s.421-.287.421-.641l.216-9.914c0-.354.071-.637.421-.637s.425.287.425.637l.428,9.914c0,.354.071.641.425.641s.421-.287.421-.641l.216-9.914c0-.354.071-.637.421-.637s.425.287.425.637c0,0,.892,9.111.945,12.149a10.647,10.647,0,0,1-2.875,7.337l.2,12.981H28.968Z" transform="translate(0 -2.62)" fill="#fff"/></svg>
						</a>
					</div>
					<div class="disclaimer">
						<p>Agrodolce &egrave; un supplemento di <a href="https://www.blogo.it" target="_blank" rel="nofollow noopener">Blogo</a>. Blogo è una testata giornalistica registrata. Registrazione ROC n. 22649</p>
						<p>&copy; 2004-<?php echo date( "Y" ); ?> | T-Mediahouse – P. IVA 06933670967 | <?php echo wp_get_theme()->get( 'Version' ); ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

@asset('css/forms.min.css')
<div class="innersearch search__field">
	<svg xmlns="http://www.w3.org/2000/svg" width="36.74" height="37.74" viewbox="0 0 36.74 37.74">
		<g transform="translate(2.828 1.944)"><line y1="10.141" x2="10.141" transform="translate(0 22.827)" fill="none" stroke="#ff007b" stroke-linecap="square" stroke-miterlimit="10" stroke-width="4"/><circle cx="13" cy="13" r="13" transform="translate(5.911 0.056)" fill="none" stroke="#ff007b" stroke-linecap="square" stroke-miterlimit="10" stroke-width="4"/></g>
	</svg>
	<input type="text" name="s" value="{!!  get_search_query() !!}" autocomplete="off" placeholder="scrivi qui..." required />
	<button type="submit" class="search__field__submit btn">
		<a href="#">Cerca</a>
	</button>
</div>

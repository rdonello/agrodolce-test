<!-- archive.twig -->
<div class="archive">
	<div class="archive__heading">
		<h1>{!! get_the_archive_title() !!}</h1>
	</div>


	<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">

		@include('components.partials.partial-static-adv',['banner' => 'mobile_top'])

		<div class="col-8">
			<section class="section-1-columns">
				<div class="col-12">
					@if ( have_posts() )
						@while (have_posts()) @php the_post() @endphp
						@includeFirst(['components.partials.partial-card-'.ad_get_post_type().'-list','components.partials.partial-card-post-list'])
						@endwhile
					@endif
				</div>
			</section>
		</div>

		@include('components.partials.partial-static-adv',['banner' => 'mobile_bottom'])

		<aside class="col-4">
			@include('components.partials.partial-sticky-adv')
		</aside>
	</div>

	<div class="container">
		@include('components.partials.partial-pagination')
	</div>
</div>

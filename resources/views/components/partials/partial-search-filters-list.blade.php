<!--search-form.twig partial -->

<div class="partial-search-filters-list search-filters-list-js">
	{% if loop.index <= 1 and partialsearchfilterslist is not defined or loop.index is not defined and partialsearchfilterslist is not defined %}
		@asset('css/components/partials/partial-search-filters-list.min.css')
	{% endif %}
	<div class="filter-accordions">
		<div class="accordion filter-accordion__ingredients">
			<input id="accordion-ingredients" type="checkbox" class="editorial__toggle" hidden/>
			<div class="accordion__header">
				<label for="accordion-ingredients" class="editorial--collapsable__btn">
					<span>Ingredienti</span>
				</label>
			</div>
			<div id="ingredients" class="filter-group accordion__body editorial--collapsable collapsed" style="--minheight: 0">
				<ul>
					<div class="input-field input-field--checkbox">
						<input id="pollo" type="checkbox" class="input-field__checkbox" value="pollo">
						<label for="pollo">Pollo</label>
					</div>
					<div class="input-field input-field--checkbox">
						<input id="spigola" type="checkbox" class="input-field__checkbox" value="spigola">
						<label for="spigola">Spigola</label>
					</div>
                    <div class="input-field input-field--checkbox">
						<input id="salmone" type="checkbox" class="input-field__checkbox" value="salmone">
						<label for="salmone">Salmone</label>
					</div>
				</ul>
			</div>
		</div>
        <div class="accordion filter-accordion__pasti">
			<input id="accordion-pasti" type="checkbox" class="editorial__toggle" hidden/>
			<div class="accordion__header">
				<label for="accordion-pasti" class="editorial--collapsable__btn">
					<span>Pasti</span>
				</label>
			</div>
			<div id="pasti" class="filter-group accordion__body editorial--collapsable collapsed" style="--minheight: 0">
				<ul>
					<div class="input-field input-field--checkbox">
						<input id="pranzo" type="checkbox" class="input-field__checkbox" value="pranzo">
						<label for="pranzo">Pranzo</label>
					</div>
					<div class="input-field input-field--checkbox">
						<input id="cena" type="checkbox" class="input-field__checkbox" value="cena">
						<label for="cena">Cena</label>
					</div>
                    <div class="input-field input-field--checkbox">
						<input id="Colazione" type="checkbox" class="input-field__checkbox" value="Colazione">
						<label for="Colazione">Colazione</label>
					</div>
				</ul>
			</div>
		</div>
	</div>

</div>

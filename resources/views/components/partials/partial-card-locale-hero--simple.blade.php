<!-- partial-card-ristorante-hero--simple.twig -->

<article class="partial-card-ristorante-hero--simple">
	{% if loop.index <= 1 and partialcardristorantehero is not defined or loop.index is not defined and
	partialcardristorantehero is not defined %}

	@asset('css/components/partials/partial-card-ristorante-hero--simple.min.css')

	{% endif %}
	<div class="ristorante-hero--simple__content col-4">

		{!! agrodolce_tbm_get_label("ristorante-hero--simple__story","name") !!}


		<a class="ristorante-hero--simple__title" href="{!! get_permalink() !!}">
			<h2>{!! get_the_title() !!}</h2>
		</a>

		<div class="ristorante-hero--simple__details">
			<p class="ristorante-hero--simple__address">{{ queryItem.address }}</p>
			<p><a class="ristorante-hero--simple__tag" href="{{ queryItem.city.link }}">{{ queryItem.city.name }}</a>
			</p>

			<ul>
				<li>
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 24 24">
						<path d="M23 4H3a1 1 0 010-2h15v1h2V1a1 1 0 00-1-1H3a3 3 0 00-3 3v17a4 4 0 004 4h19a1 1 0 001-1V5a1 1 0 00-1-1zm-5 12a2 2 0 112-2 2 2 0 01-2 2z"
							  fill="#ff007b"></path>
					</svg>
					<span>{{ queryItem.budget }}</span>
				</li>
				<li>
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 18.783 24">
						<path d="M16.988,17.433a7.278,7.278,0,0,1-9.193,0A7.3,7.3,0,0,0,3,24.286H21.783a7.3,7.3,0,0,0-4.795-6.853Z"
							  transform="translate(-3 -0.286)" fill="#ff007b"/>
						<path d="M16.478,1H8.131A3.13,3.13,0,0,0,7.087,7.082v5.4a5.217,5.217,0,0,0,10.435,0v-5.4A3.13,3.13,0,0,0,16.478,1ZM15.435,12.478a3.13,3.13,0,1,1-6.261,0V10.391h6.261Z"
							  transform="translate(-2.913 -1)" fill="#ff007b"/>
					</svg>
					<span>{{ queryItem.chefName }}</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="ristorante-hero--simple__figure col-8">
		<div class="ristorante-hero--simple__image-wrapper">
			<picture
					data-link="{!! get_permalink() !!}">
				<!--[if IE 9]>
				<video style="display: none;"><![endif]-->
				<source
						data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,486)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,972)) !!} 2x"
						media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img class="lazyload"
					 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,486)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,972)) !!} 2x"
					 alt="{!! get_the_title() !!}"/>
			</picture>
		</div>
	</div>
</article>

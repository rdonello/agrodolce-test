<!-- partial-card-video-list.twig -->

<div class="partial-card-video-list">
	@asset('css/components/partials/partial-card-video-list.min.css')
	<div class="card-video-list__figure">
		<div class="card-video-list__image-wrapper">
			<picture data-link="{!! get_permalink() !!}">
				<svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewbox="0 0 72 72">
					<g transform="translate(-1101 -388)">
						<circle cx="36" cy="36" r="36" transform="translate(1101 388)" fill="#ff007b"/>
						<path d="M21.6,10.016,7.567,2.049A.379.379,0,0,0,7,2.379V18.313a.379.379,0,0,0,.567.33L21.6,10.676a.379.379,0,0,0,0-.66Z"
							  transform="translate(1123.887 413.726)" fill="#fff"/>
					</g>
				</svg>

				@if (isset($ad_loop) && $ad_loop === 0)
					<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x"
						 src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}"
						 alt="{!! tbm_get_the_post_thumbnail_alt()!!}"/>
				@else
					<img class="lazyload"
						 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x"
						 alt="{!! tbm_get_the_post_thumbnail_alt()!!}"/>
				@endif
			</picture>
		</div>
	</div>
	<div class="card-video-list__content">
		{!! agrodolce_tbm_get_label("card-video-list__story", "name") !!}
		<a class="card-video-list__title" href="{!! get_permalink() !!}">
			<h3>{!! get_the_title() !!}</h3>
		</a>
		<p class="card-video-list__abstract">@php the_excerpt() @endphp</p>
	</div>
</div>

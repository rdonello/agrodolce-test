<!-- partial-innerrelated-video.twig -->
@dd($sc)
		<aside class="partial-innerrelated-video">
				@asset('css/components/partials/partial-innerrelated-video.min.css')
			<article class="innerrelated-video__wrapper">
				<div class="innerrelated-video__image-wrapper">
					<picture data-link="{!! $sc->permalink !!}">
						<svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewbox="0 0 72 72">
							<g transform="translate(-1101 -388)"><circle cx="36" cy="36" r="36" transform="translate(1101 388)" fill="#ff007b"/><path d="M21.6,10.016,7.567,2.049A.379.379,0,0,0,7,2.379V18.313a.379.379,0,0,0,.567.33L21.6,10.676a.379.379,0,0,0,0-.66Z" transform="translate(1123.887 413.726)" fill="#fff"/></g>
						</svg>

						<img class="lazyload" data-src="{!! tbm_wp_get_attachment_image_url($sc->image_id,array(430,327)) !!}" alt="{!! $sc->title !!}"/>
					</picture>
				</div>

				<a class="innerrelated-video__category" href="{!! $sc->permalink !!}">
					<span>{!! $sc->tax !!}</span>
				</a>
				<a class="innerrelated-video__title" href="{!! $sc->permalink !!}">
					<h3>{!! $sc->title !!}</h3>
				</a>
			</article>
		</aside>

<!-- partial-card-video-big.twig -->

<article class="partial-card-video-big">
	@asset('css/components/partials/partial-card-video-big.min.css')
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewBox="0 0 72 72"><g transform="translate(-1101 -388)"><circle cx="36" cy="36" r="36" transform="translate(1101 388)" fill="#ff007b"/><path d="M21.6,10.016,7.567,2.049A.379.379,0,0,0,7,2.379V18.313a.379.379,0,0,0,.567.33L21.6,10.676a.379.379,0,0,0,0-.66Z" transform="translate(1123.887 413.726)" fill="#fff"/></g></svg>

			<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x" alt="{!! get_the_title() !!}"/>
		</picture>
	</div>


			{!! agrodolce_tbm_get_label("card-video__story", "name") !!}


	<div class="card__title">
		<a href="{!! get_permalink() !!}"><h3>{!! get_the_title() !!}</h3></a>
	</div>
</article>

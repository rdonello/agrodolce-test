<!--search-form.twig partial -->

<div class="slide-search search-form-js" role="search">
	@asset('css/components/partials/partial-search-form.min.css')
	<div class="slide-search__lens">
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 20 20"><g transform="translate(0.005 -952.326)"><g transform="translate(-0.005 952.326)"><path class="svg--theme--primary" d="M-.7-9A8.316,8.316,0,0,0-9-.717,8.316,8.316,0,0,0-.7,7.565,8.2,8.2,0,0,0,4.012,6.079L8.5,10.562a1.447,1.447,0,0,0,2.075,0,1.483,1.483,0,0,0,0-2.071L6.111,4.009A8.3,8.3,0,0,0,7.6-.717,8.316,8.316,0,0,0-.7-9Zm0,2.923A5.35,5.35,0,0,1,4.671-.717,5.35,5.35,0,0,1-.7,4.642,5.35,5.35,0,0,1-6.07-.717,5.35,5.35,0,0,1-.7-6.077Z" fill="#fff" transform="translate(9 9)"></path></g></g></svg>
	</div>
	<div class="slide-search__container">
		<div class="slide-search__input">
			<form method="get" action="/">
				<div class="slide-search__input__field box-shadow">
					<div class="search-bar__field__search-ico">
						<button type="submit" class="search-lens">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
								 y="0px" width="24px" height="24px" viewbox="0 0 16 16">
								<g transform="translate(0, 0)">
									<path class="svg--theme--primary"
										  d="M12.7,11.3c0.9-1.2,1.4-2.6,1.4-4.2C14.1,3.2,11,0,7.1,0S0,3.2,0,7.1c0,3.9,3.2,7.1,7.1,7.1 c1.6,0,3.1-0.5,4.2-1.4l3,3c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3c0.4-0.4,0.4-1,0-1.4L12.7,11.3z M7.1,12.1 C4.3,12.1,2,9.9,2,7.1S4.3,2,7.1,2s5.1,2.3,5.1,5.1S9.9,12.1,7.1,12.1z"
										  fill="#fff"></path>
								</g>
							</svg>
						</button>
					</div>
					<input class="search-bar__field__input" name="s" type="text" name="main_search" id="main_search" placeholder="cerca..." />
					<input type="hidden" name="innersearch_type" />
					<div class="search-bar__select">
						<span>in&nbsp;&nbsp;</span>
						<select class="dropdown chosen-select" id="search_type" name="search_type">
							<option value="all">Agrodolce</option>
							<option value="ricetta">Ricette</option>
							<option value="ristorante">Ristoranti</option>
							<option value="ristorante">Locali</option>
							<option value="post">Articoli</option>
							<option value="news">Notizie</option>
							<option value="podcast">Podcast</option>
							<option value="video">Video</option>
							<option value="galleria">Gallery</option>
						</select>
					</div>
					<div class="search-bar__field__results">
					</div>
				</div>
			</form>

			<div class="button button--collapse"></div>
		</div>
	</div>

</div>

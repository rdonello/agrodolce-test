<!-- partial-card-ricetta-big.twig -->

<article class="partial-card-ricetta-big">
		@asset('css/components/partials/partial-card-ricetta-big.min.css')
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(471,236)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(942,472)) !!} 2x" alt="{!! get_the_title() !!}"/>
		</picture>
	</div>

	<div class="card__content" style="--background: url('{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(942,472)) !!}')">
		<a class="card__label" href="{!! get_post_type_archive_link('ricetta') !!}">Ricette</a>
				{!! agrodolce_tbm_get_label("card-ricetta__story", "name") !!}
		<div class="card__title">
			<a href="{!! get_permalink() !!}">
				<h3>{!! get_the_title() !!}</h3>
			</a>
		</div>
		<div class="card__details">
			<ul>
				<li>
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 20 24">
						<g transform="translate(-2)"><path d="M13,4.051V2h3a1,1,0,0,0,0-2H8A1,1,0,0,0,8,2h3V4.051A10.013,10.013,0,0,0,2,14v9a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V14a10.013,10.013,0,0,0-9-9.949ZM12,6a8,8,0,1,1-8,8,8,8,0,0,1,8-8Z" fill="#ff007b"/><rect width="7" height="2" transform="translate(11 13)" fill="#ff007b"/></g>
					</svg>
					<span>{{ get_field( "tempo_preparazione" ) }} min</span>
				</li>
				<li>
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 28 24">
						<g transform="translate(-0.6 -2)"><rect width="4" height="24" rx="1" transform="translate(0.6 2)" fill="#ff007b"/><rect width="4" height="18" rx="1" transform="translate(6.6 8)" fill="#ff007b"/><rect width="4" height="18" rx="1" transform="translate(18.6 8)" fill="#ff007b"/><rect width="4" height="12" rx="1" transform="translate(12.6 14)" fill="#ff007b"/><rect width="4" height="12" rx="1" transform="translate(24.6 14)" fill="#ff007b"/></g>
					</svg>
					<span>{{ get_field( "calorie" ) }} cal x 100g</span>
				</li>
				<li>
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 24 24"><path d="M13,21H5v2a.945.945,0,0,0,1,1H18a.945.945,0,0,0,1-1V21Z" fill="#ff007b"/><path d="M18,4h-.3A5.99,5.99,0,0,0,12,0,6.087,6.087,0,0,0,6.3,4H6A5.991,5.991,0,0,0,5,15.9V19h6V15h2v4h6V15.9A5.991,5.991,0,0,0,18,4Z" fill="#ff007b"/></svg>
					<span>Difficolt&agrave; {{ get_field( "difficolta" ) }}</span>
				</li>
			</ul>
		</div>
	</div>
</article>

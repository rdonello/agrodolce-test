<!-- partial-innerproduct-affiliation--big.twig -->

<div id="{{ queryProduct.id }}" class="partial-innerproduct-affiliation--big">
    	{% if loop.index <= 1 and partialinnerproductaffiliationbig is not defined or loop.index is not defined and partialinnerproductaffiliationbig is not defined %}

		@asset('css/components/partials/partial-innerproduct-affiliation--big.min.css')

	{% endif %}
    <div class="innerproduct-affiliation--big__product">

        {% if queryProduct.badge is not empty %}
            <div class="product__badge">
                <span>{{ queryProduct.badge }}</span>
            </div>
        {% endif %}

        <div class="product__info">
            <div class="container">
                <div class="span-5">
                    <picture>
                        <img class="lazyload" data-srcset="{{ queryProduct.srcImg }}, {{ queryProduct.srcImgx2 }} 2x" alt="{{ queryProduct.name }}"/>
                    </picture>
                </div>
                <div class="span-7">
                    <!-- link to the best price -->
                    <a href="#" target="_blank"><h2>{{ queryProduct.name }}</h2></a>

                    <p class="comment">{{ queryProduct.comment }}</p>
                    <p class="abstract">{{ queryProduct.abstract }}</p>

                    <!-- Affiliation cta -->
                    <div class="product__cta">
                        {% for queryCta in queryProduct.affiliation %}
                            <a class="cta cta--amazonprime" href="{{ queryCta.link }}" target="_blank">{{ queryCta.price }} € su {{ queryCta.name }}</a>

                            {% if queryCta.originalPrice is defined and queryCta.originalPrice != queryCta.price %}
                                <div class="product__offert">
                                    <span class="offert">{{ queryCta.originalPrice }} €</span>
                                    <span class="discount">risparmi 80 € (12%)</span>
                                </div>
                            {% endif %}
                        {% endfor %}
                    </div>
                </div>
            </div>
        </div>

        <div class="product__list">
            <div class="container">
                <div class="col-6">
                    <p><strong>Pro</strong></p>
                    <ul class="list--pro">
                        {% for queryPro in queryProduct.pro %}
                            <li>{{ queryPro }}</li>
                        {% endfor %}
                    </ul>
                </div>
                <div class="col-6">
                    <p><strong>Contro</strong></p>
                    <ul class="list--cons">
                        {% for queryCons in queryProduct.cons %}
                            <li>{{ queryCons }}</li>
                        {% endfor %}
                    </ul>
                </div>
            </div>
        </div>
    </div>

    {{-- Innerproduct Affiliation description --}}
    {{ queryProduct.description|replace({
        "%innerRelatedPost%": innerRelatedPost,
        "%innerRelatedVideo%": innerRelatedVideo,
        "%innerRelatedLink%": innerRelatedLink,
        "%innerRelatedPlayerVideoInside%": innerRelatedPlayerVideoInside,
        "%innerRelatedPlayerVideoAside%": innerRelatedPlayerVideoAside,
        "%innerRelatedSliderGallery%": innerRelatedSliderGallery,
        "%innerRelatedImage%": innerRelatedImage
    }) }}
</div>

<!-- partial-card-gallery-list.twig -->
@php $thumbs = tbm_get_gallery_thumbs(get_the_ID(),1,3); @endphp
<div class="partial-card-gallery-list">
		@asset('css/components/partials/partial-card-gallery-list.min.css')
	<div class="gallery-list__figure">
		<div class="gallery-list__image-wrapper">
			<picture data-link="{!! get_permalink() !!}">
				<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(149,228)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(298,456)) !!} 2x" alt="{!! get_the_title() !!}"/>
			</picture>
		</div>
		<div class="gallery-list__image-container">
			<div class="gallery-list__image-wrapper">
				<picture data-link="{!! get_permalink() !!}">
					<img class="lazyload" data-srcset="{!! tbm_wp_get_attachment_image_url ($thumbs[0],array(149,113)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(298,226)) !!} 2x" alt="{!! get_the_title() !!}"/>
				</picture>
			</div>
			<div class="gallery-list__image-wrapper">
				<span data-link="{!! get_permalink() !!}" class="gallery-list__photo-number">+{!! tbm_get_gallery_photo_number(get_the_ID()) !!}</span>
				<picture data-link="{!! get_permalink() !!}">
					<img class="lazyload" data-srcset="{!! tbm_wp_get_attachment_image_url ($thumbs[1],array(149,113)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(298,226)) !!} 2x" alt="{!! get_the_title() !!}"/>
				</picture>
			</div>
		</div>
	</div>
	<div class="gallery-list__content">
				{!! agrodolce_tbm_get_label("gallery-list__story", "name") !!}
		<a class="gallery-list__title" href="{!! get_permalink() !!}">
			<h3>{!! get_the_title() !!}</h3>
			<p class="gallery-list__abstract">@php the_excerpt() @endphp</p>
		</a>
	</div>
</div>

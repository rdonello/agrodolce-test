<!-- partial-innerrelated-playervideo--inside.twig -->
<div class="partial-innerrelated-playervideo--inside">
	@asset('css/components/partials/partial-innerrelated-playervideo--inside.min.css')
	<div class="playervideo-wrapper">
        @if ( function_exists( 'html_video_generate_brid_video_player' ) && get_field( 'video_cnvid_id', 'option' ) )
            {!!  html_video_generate_brid_video_player( $sc->id, '', true ) !!}
        @else
            <video src="{!! get_field( 'tbm_videoid', $sc->id )['url'] !!}" controls controlsList="nodownload"></video>
        @endif
    </div>
</div>

<!-- partial-card-video-hero.twig -->

<article class="partial-card-video-hero">
		@asset('css/components/partials/partial-card-video-hero.min.css')
    <div class="video-hero__content span-5">

                {!! agrodolce_tbm_get_label("video-hero__story", "name") !!}


        <a class="video-hero__title" href="{!! get_permalink() !!}">
            <h2>{!! get_the_title() !!}</h2>
        </a>

        <p class="video-hero__abstract abstract">@php the_excerpt() @endphp</p>
    </div>
    <div class="video-hero__figure span-7">
        <div class="video-hero__image-wrapper">
            <picture data-link="{!! get_permalink() !!}">
                <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewBox="0 0 72 72"><g transform="translate(-1101 -388)"><circle cx="36" cy="36" r="36" transform="translate(1101 388)" fill="#ff007b"/><path d="M21.6,10.016,7.567,2.049A.379.379,0,0,0,7,2.379V18.313a.379.379,0,0,0,.567.33L21.6,10.676a.379.379,0,0,0,0-.66Z" transform="translate(1123.887 413.726)" fill="#fff"/></g></svg>

				<!--[if IE 9]><video style="display: none;"><![endif]-->
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x" media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(725,551)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1450,1102)) !!} 2x"
					 src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(725,551)) !!}"
					 alt="{!! tbm_get_the_post_thumbnail_alt() !!}"/>
    		</picture>
        </div>
    </div>
</article>

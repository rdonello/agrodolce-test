<!-- partial-card-ristorante-hero.twig -->
@php $data=get_ristorante_data_object(get_the_ID()); @endphp
<article class="partial-card-ristorante-hero">
	@asset('css/components/partials/partial-card-ristorante-hero.min.css')
	<div class="ristorante-hero__figure span-8">
		<div class="ristorante-hero__image-wrapper">
			<picture data-link="{!! get_permalink() !!}">
				<!--[if IE 9]>
				<video style="display: none;"><![endif]-->
				<source
						srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(800,450)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1600,900)) !!} 2x"
						media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(800,450)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1600,900)) !!} 2x"
					 src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(800,450)) !!}"
					 alt="{!! tbm_get_the_post_thumbnail_alt(get_the_ID()) !!}"/>
			</picture>
		</div>
	</div>
	<div class="ristorante-hero__content span-6">
		{!! agrodolce_tbm_get_label("ristorante-hero__label", "name",1,get_the_ID(),'category') !!}

		{!! agrodolce_tbm_get_label("ristorante-hero__story", "name",1,get_the_ID(),'citta') !!}

		<a class="ristorante-hero__title" href="{!! get_permalink() !!}">
			<h2>{!! get_the_title() !!}</h2>
		</a>

		<div class="ristorante-hero__details">

			{{-- Address --}}
			@if($data['address'] && $data['city'])
				<p class="ristorante-hero__address">{!! $data['address'] !!}
					@if($data['city_link'])
						, <a href="{!! $data['city_link'] !!}">{!! $data['city'] !!}</a>
					@endif
				</p>
			@elseif($data['address_complete'])
				<p class="ristorante-hero__address">{!! $data['address_complete'] !!}</p>
			@endif


			<ul>
				{{-- Budget --}}
				@if($data['budget'])
					<li>
						<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 24 24">
							<path d="M23 4H3a1 1 0 010-2h15v1h2V1a1 1 0 00-1-1H3a3 3 0 00-3 3v17a4 4 0 004 4h19a1 1 0 001-1V5a1 1 0 00-1-1zm-5 12a2 2 0 112-2 2 2 0 01-2 2z"
								  fill="#ff007b"></path>
						</svg>
						<span>{!! $data['budget'] !!}</span>
					</li>
				@endif

				{{-- Chef --}}
				@if($data['chef'])
					<li>
						<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 24 24">
							<path d="M13,21H5v2a.945.945,0,0,0,1,1H18a.945.945,0,0,0,1-1V21Z" fill="#ff007b"/>
							<path d="M18,4h-.3A5.99,5.99,0,0,0,12,0,6.087,6.087,0,0,0,6.3,4H6A5.991,5.991,0,0,0,5,15.9V19h6V15h2v4h6V15.9A5.991,5.991,0,0,0,18,4Z"
								  fill="#ff007b"/>
						</svg>

						<span>{!! $data['chef'] !!}</span>
					</li>
				@endif
			</ul>
		</div>
	</div>
</article>

<!-- partial-card-post-hero.twig -->

<article class="partial-card-post-hero">
	@asset('css/components/partials/partial-card-post-hero.min.css')
	<div class="post-hero__content span-4">
		{!! agrodolce_tbm_get_label("post-hero__story", "name") !!}
		<a class="post-hero__title" href="{!! get_permalink() !!}">
			<h2>{!! get_the_title() !!}</h2>
		</a>
		<span class="author__name">di {!! tbm_get_the_author() !!}</span>
		<div class="post-hero__special-abstract">
			<div class="icon-quotes">
				<svg xmlns="http://www.w3.org/2000/svg" width="32" height="24" viewbox="0 0 32 24">
					<g transform="translate(0 -4)">
						<path d="M0,4V28L12,16V4Z" fill="#19b7b1"/>
						<path d="M20,4V28L32,16V4Z" fill="#19b7b1"/>
					</g>
				</svg>
			</div>
			<p>@php the_excerpt() @endphp</p>
		</div>

		<p class="post-hero__abstract abstract">@php the_excerpt() @endphp</p>
	</div>
	<div class="post-hero__figure span-8">
		{!! tbm_agrodolce_get_rubrica() !!}
		<div class="post-hero__image-wrapper">
			<picture
					data-link="{!! get_permalink() !!}">
				<!--[if IE 9]>
				<video style="display: none;"><![endif]-->
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,486)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,972)) !!} 2x"
						media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,486)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,972)) !!} 2x"
					 src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,486)) !!}"
					 alt="{!! tbm_get_the_post_thumbnail_alt(get_the_ID()) !!}"/>
			</picture>
		</div>
	</div>
</article>

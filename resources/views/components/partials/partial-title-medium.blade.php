<!-- partial-title-medium.twig -->

<div class="partial-title-medium">
{% if loop.index <= 1 and partialtitlemedium is not defined or loop.index is not defined and partialtitlemedium is not defined %}

		@asset('css/components/partials/partial-title-medium.min.css')

	{% endif %}
    <div class="title-medium__content">
        <h2 class="title-medium__title">{{ queryItem.name }}</h2>
        <span class="bottom_line"></span>
    </div>
</div>

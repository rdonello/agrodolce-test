<!-- partial-pagination.twig -->
@php
	// Get pagination option from controller and parse with defaults ($navigator_data are defaults)
	$pagination_data = wp_parse_args( $pagination, $navigator_data );
@endphp
<nav class="partial-pagination">
	@asset('css/components/partials/partial-pagination.min.css')
	<div class="pagination pagination--standard">
		@php
			$array = explode('<li>',paginate_links($pagination_data));
			array_splice($array,-2,1,array());
			echo implode('<li>',$array);
		@endphp
	</div>
</nav>

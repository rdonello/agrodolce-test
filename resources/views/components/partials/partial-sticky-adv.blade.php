<!-- partial-sticky-adv.twig -->
<div class="partial-sticky-adv">
	@asset('css/components/partials/partial-sticky-adv.min.css')
	<div class="sticky-element">
		<div class="sticky-adv">
			@if($banner == 'post_header')
				{!! tbm_get_the_banner( 'POST_HEADER','','',false ) !!}
			@endif

			{{--  Static adv - post_footer --}}
			@if($banner == 'post_footer')
				{!! tbm_get_the_banner( 'POST_FOOTER','','',false,false ) !!}
			@endif

			{{--  Static adv - Mobile Top --}}
			@if($banner == 'mobile_top')
				{!! tbm_get_the_banner( 'BOX_MOBILE_INSIDE_TOP','','',false,false ) !!}
			@endif

			{{--  Static adv - Mobile Bottom --}}
			@if($banner == 'mobile_bottom')
				{{-- tbm_get_the_banner( 'BOX_MOBILE_INSIDE_BOTTOM','','',false,false ) --}}
			@endif

			{{--  Static adv - Desktop Top --}}
			@if($banner == 'desktop_top')
				{!! tbm_get_the_banner( 'BOX_DESKTOP_SIDE_TOP','','',false,false ) !!}
			@endif

			{{--  Static adv - Mobile Bottom --}}
			@if($banner == 'desktop_bottom')
				{!! tbm_get_the_banner( 'BOX_DESKTOP_SIDE_BOTTOM','','',false,false ) !!}
			@endif

			{{--  Static adv - Mobile Bottom --}}
			@if(!$banner)
				{!! tbm_get_the_banner( 'BOX_DESKTOP_SIDE_TOP','','',false,false ) !!}
			@endif
		</div>
	</div>
</div>

<!-- partial-list-stories.twig -->
<div class="partial-list-stories editorial">
	@asset('css/components/partials/partial-list-stories.min.css')
	<ul>
		@foreach($featured_specials as $special)
			<li><a href="{!!  $special['permalink'] !!}"> {!! $special['title'] !!}</a></li>
		@endforeach
	</ul>
</div>

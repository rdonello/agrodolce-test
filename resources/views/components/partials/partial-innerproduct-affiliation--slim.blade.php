<!-- partial-innerproduct-affiliation--slim.twig -->

<div id="{{ queryProduct.id }}" class="partial-innerproduct-affiliation--slim">
	{% if loop.index <= 1 and partialinnerproductaffiliationslim is not defined or loop.index is not defined and partialinnerproductaffiliationslim is not defined %}

		@asset('css/components/partials/partial-innerproduct-affiliation--slim.min.css')

	{% endif %}
	<div class="innerproduct-affiliation--slim__product">
		<div class="product__info">
			<div class="container">
				<div
					class="span-6">
					<!-- link to the best price -->
					<a href="#" target="_blank">
						<h2>{{ queryProduct.name }}</h2>
					</a>
				</div>
				<div
					class="span-6">
					<!-- Affiliation cta -->
					<div class="product__cta">
						{% for queryCta in queryProduct.affiliation %}
							<a class="cta cta--amazonprime" href="{{ queryCta.link }}" target="_blank">{{ queryCta.price }}
								€ su
								{{ queryCta.name }}</a>

							{% if queryCta.originalPrice is defined and queryCta.originalPrice != queryCta.price %}
								<div class="product__offert">
									<span class="offert">{{ queryCta.originalPrice }}
										€</span>
									<span class="discount">risparmi 80 € (12%)</span>
								</div>
							{% endif %}
						{% endfor %}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

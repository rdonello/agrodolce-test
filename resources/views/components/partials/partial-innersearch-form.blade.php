<!-- partial-innersearch-form.twig -->

<div class="partial-innersearch innersearch__field innersearch-form-js" role="search">
	@asset('css/components/partials/partial-innersearch-form.min.css')
	<div class="slide-innersearch__container">
		<div class="slide-innersearch__input">
			<form method="get" action="/">
				<div class="slide-innersearch__input__field box-shadow">
					<div class="innersearch-lens">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="34px" height="34px" viewBox="0 0 16 16"> <g transform="translate(0, 0)"> <path class="svg--theme--primary" d="M12.7,11.3c0.9-1.2,1.4-2.6,1.4-4.2C14.1,3.2,11,0,7.1,0S0,3.2,0,7.1c0,3.9,3.2,7.1,7.1,7.1 c1.6,0,3.1-0.5,4.2-1.4l3,3c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3c0.4-0.4,0.4-1,0-1.4L12.7,11.3z M7.1,12.1 C4.3,12.1,2,9.9,2,7.1S4.3,2,7.1,2s5.1,2.3,5.1,5.1S9.9,12.1,7.1,12.1z" fill="#ff007b"></path> </g></svg>
					</div>
					<input class="innersearch-bar__field__input" type="text" name="s" id="innersearch"
						   placeholder="{!! isset( $search_placeholder )? $search_placeholder : '' !!}"/>
					<input class="innersearch-bar__field__input" type="hidden" name="innersearch_type"
						   id="innersearch_type" value="{!! $innersearch_type !!}" />
					<button type="submit" class="innersearch__field__submit btn">
						<input type="submit" value="{!! isset($search_text) ? $search_text : 'Cerca' !!}">
					</button>
					<div class="innersearch-bar__field__results"></div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- partial-pagination.twig -->
<div class="partial-alpha-pagination">
@asset('css/components/partials/partial-alpha-pagination.min.css')
	<ul class="letters-list">
	@foreach($alpha_pagination as $letter => $v)
		<li >
			@if($v['has_posts'])
				<a href="{{ $v['link'] }}" {!! $v['class'] !!} >{{ $letter }}</a>
			@else
				<span>{{ $letter }}</span>
			@endif
		</li>
	@endforeach
	</ul>
</div>

<!-- partial-innerrelated-ricetta.twig -->
	<div class="partial-innerrelated-ricetta">
			@asset('css/components/partials/partial-innerrelated-ricetta.min.css')
		<h2>Preparazione</h2>
		<ol>
			{% for steps in queryItem.relatedRicetta %}
				<picture data-link="{{ steps.link }}">
					<img class="lazyload" data-src="{{ steps.srcImg640x427 }}" alt="{{ steps.title }}"/>
				</picture>
				<li class="steps__item">{{ steps.content }}</li>
			{% endfor %}
		</ol>
	</div>


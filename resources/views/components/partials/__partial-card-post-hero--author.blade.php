<!-- partial-card-post-hero--author.twig -->

<article class="partial-card-post-hero--author">
	{% if loop.index <= 1 and partialcardpostheroauthor is not defined or loop.index is not defined and partialcardpostheroauthor is not defined %}

		@asset('css/components/partials/partial-card-post-hero--author.min.css')

	{% endif %}
	<div class="post-hero--author__content span-4">

				{!! agrodolce_tbm_get_label("post-hero--author__story","name") !!}


		<a class="post-hero--author__title" href="{!! get_permalink() !!}">
			<h2>{!! get_the_title() !!}</h2>
		</a>
		<div class="post-hero--author__datetime">
			<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 20 20">
				<g transform="translate(20) rotate(90)"><path d="M10,0A10,10,0,1,0,20,10,10,10,0,0,0,10,0Zm.714,10a.714.714,0,0,1-.714.714H4.286a.714.714,0,1,1,0-1.429h5v-5a.714.714,0,1,1,1.429,0Z" fill="#ff007b"/></g>
			</svg>
			<p>{{ queryItem.publishedAt }}</p>
		</div>
	</div>
	<div class="post-hero--author__figure span-8">
		<div class="post-hero--author__image-wrapper">
			<picture
				data-link="{!! get_permalink() !!}">
				<!--[if IE 9]><video style="display: none;"><![endif]-->
				<source
				data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,486)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,972)) !!} 2x" media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(640,486)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1280,972)) !!} 2x" alt="{!! get_the_title() !!}"/>
			</picture>
		</div>
	</div>
</article>

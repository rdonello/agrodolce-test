<!-- partial-card-post-big.twig -->

<article class="partial-card-post-small">
	@asset('css/components/partials/partial-card-post-small.min.css')
	<div class="card__thumbnail--small">
		{!! tbm_agrodolce_get_rubrica() !!}
		<picture data-link="{!! get_permalink() !!}">
			<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x" alt="{!! get_the_title() !!}"/>
		</picture>
	</div>
	{!! agrodolce_tbm_get_label("card-post__story", "name") !!}
	<div class="card__title">
		<a href="{!! get_permalink() !!}"><h3>{!! get_the_title() !!}</h3></a>
	</div>
</article>

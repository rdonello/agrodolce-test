<!-- partial-card-gallery-hero.twig -->
@php $thumbs = tbm_get_gallery_thumbs(get_the_ID(),1,3);@endphp
<article class="partial-card-gallery-hero">
	@asset('css/components/partials/partial-card-gallery-hero.min.css')
	<div class="gallery-hero__content span-5">

				{!! agrodolce_tbm_get_label("gallery-hero__story", "name") !!}


		<a class="gallery-hero__title" href="{!! get_permalink() !!}">
			<h2>{!! get_the_title() !!}</h2>
		</a>

		<p class="gallery-hero__abstract abstract">@php the_excerpt() @endphp</p>
	</div>
	<div class="gallery-hero__figure span-7">
		<div class="gallery-hero__image-wrapper">
			<picture data-link="{!! get_permalink() !!}">
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(725,551)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1450,1102)) !!} 2x" media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(725,551)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1450,1102)) !!} 2x" alt="{!! tbm_get_the_post_thumbnail_alt(get_the_ID()) !!}"/>
			</picture>
		</div>
		<div class="gallery-hero__image-container">
			<div class="gallery-hero__image-wrapper">
				<picture data-link="{!! get_permalink() !!}">
					<img src="{!! tbm_wp_get_attachment_image_url ($thumbs[0],array(430,327)) !!}" alt=""/>
				</picture>
			</div>
			<div class="gallery-hero__image-wrapper">
				<span data-link="{!! get_permalink() !!}" class="gallery-hero__photo-number">+{!! tbm_get_gallery_photo_number(get_the_ID()) !!}</span>
				<picture data-link="{!! get_permalink() !!}">
					<img src="{!! tbm_wp_get_attachment_image_url ($thumbs[1],array(430,327)) !!}" alt=""/>
				</picture>
			</div>
		</div>
	</div>
</article>

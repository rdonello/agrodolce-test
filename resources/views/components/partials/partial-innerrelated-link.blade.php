<!-- partial-innerrelated-link.twig -->

<div class="partial-innerrelated-link">
{% if loop.index <= 1 and partialinnerrelatedlink is not defined or loop.index is not defined and partialinnerrelatedlink is not defined %}

		@asset('css/components/partials/partial-innerrelated-link.min.css')

	{% endif %}
    <svg class="icon icon--standard icon--innerlink" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" stroke-width="2" style="width: 24px; height:24px;"><g stroke-width="2" transform="translate(0, 0)"><line data-cap="butt" fill="none" stroke="#151515" stroke-width="2" stroke-miterlimit="10" x1="1" y1="12" x2="17" y2="12" stroke-linejoin="miter" stroke-linecap="butt"></line> <polyline fill="none" stroke="#151515" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points="11,6 17,12 11,18 " stroke-linejoin="miter"></polyline></g></svg>

    <a href="#">Regionali, l’Emilia resta rossa: Bonaccini confermato. </a>
</div>

<!-- partial-google-map.twig partial -->
<div class="partial-google-map">

	@asset('css/components/partials/partial-google-map.min.css')

	<style>
		#map {
			height: 488px;
			width: 725px;
			max-width: 100%;
		}

		@media screen and(max-width: 1280px) {
			#map {
				width: 100%;
			}
		}

		@media screen and(max-width: 767px) {
			.container--google-map #map {
				position: absolute;
			}

			.container--google-map #map {
				width: 100%;
			}
		}
	</style>

	<div id="map"></div>
	<script>
		var geocoder;
		var map;
		var address = "{{ $address }}";
		geocoder = new google.maps.Geocoder();

		function initMap() {
			var map = new google.maps.Map(document.getElementById('map'), {zoom: 16});
			geocoder = new google.maps.Geocoder();
			codeAddress(geocoder, map);
		}

		function codeAddress(geocoder, map) {
			geocoder.geocode({
				'address': address
			}, function (results, status) {
				if (status === 'OK') {
					map.setCenter(results[0].geometry.location);
					var marker = new google.maps.Marker({map: map, position: results[0].geometry.location});
				} else {
					alert('Geocode non ha trovato l\'indirizzo per questo motivo: ' + status);
				}
			});
		}
	</script>

	<script async defer src="https://maps.googleapis.com/maps/api/js?key={!! getenv( 'GMAPS_API' )  !!}&callback=initMap"></script>
</div>

@if( $sc && isset($sc->permalink) && isset($sc->title))
	<div class="partial-amp-innerrelated-post">
		<a href="{!! $sc->permalink !!}"><h3>{!! $sc->title !!}</h3></a>
	</div>
@endif

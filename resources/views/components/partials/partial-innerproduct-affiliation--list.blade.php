<!-- partial-innerproduct-affiliation--list.twig -->

<div id="{{ queryProduct.id }}" class="partial-innerproduct-affiliation--list">
	{% if loop.index <= 1 and partialinnerproductaffiliationlist is not defined or loop.index is not defined and partialinnerproductaffiliationlist is not defined %}

		@asset('css/components/partials/partial-innerproduct-affiliation--list.min.css')

	{% endif %}
	<div class="innerproduct-affiliation--list__product">
		<div class="product__info">
			<div class="container">
				<div class="span-4">
					<picture>
						<img class="lazyload" data-srcset="{{ queryProduct.srcImg }}, {{ queryProduct.srcImgx2 }} 2x" alt="{{ queryProduct.name }}"/>
					</picture>
				</div>
				<div
					class="span-7">

					<!-- link to the best price -->
					<a href="#" target="_blank">
						<h2>{{ queryProduct.name }}</h2>
					</a>

					<p class="comment">{{ queryProduct.comment }}</p>

					<!-- Affiliation cta -->
					<div class="product__cta">
						{% for queryCta in queryProduct.affiliation %}
							<a class="cta cta--amazonprime" href="{{ queryCta.link }}" target="_blank">{{ queryCta.price }}
								€ su
								{{ queryCta.name }}</a>

							{% if queryCta.originalPrice is defined and queryCta.originalPrice != queryCta.price %}
								<div class="product__offert">
									<span class="offert">{{ queryCta.originalPrice }}
										€</span>
									<span class="discount">risparmi 80 € (12%)</span>
								</div>
							{% endif %}
						{% endfor %}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

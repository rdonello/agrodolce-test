<!-- partial-innerrelated-cocktail.twig -->

	<div class="partial-innerrelated-cocktail">
		{% if loop.index <= 1 and partialinnerrelatedcocktail is not defined or loop.index is not defined and partialinnerrelatedcocktail is not defined %}
			@asset('css/components/partials/partial-innerrelated-cocktail.min.css')

		{% endif %}
		<h2>Preparazione</h2>
		<ol>
			{% for steps in queryItem.relatedRicetta %}
				<picture data-link="{{ steps.link }}">
					<img class="lazyload" data-src="{{ steps.srcImg640x427 }}" alt="{{ steps.title }}"/>
				</picture>
				<li class="steps__item">{{ steps.content }}</li>
			{% endfor %}
		</ol>
	</div>


<!-- partial-innerrelated-post.twig -->
		<aside class="partial-innerrelated-post">
				@asset('css/components/partials/partial-innerrelated-post.min.css')
			<article class="innerrelated-post__wrapper">
				<div class="innerrelated-post__image-wrapper">
					<picture data-link="{!! $sc->permalink !!}">
						<img class="lazyload" data-src="{!! tbm_wp_get_attachment_image_url($sc->image_id,array(430,327)) !!}" alt="{!! $sc->title !!}"/>
					</picture>
				</div>

				<a class="innerrelated-post__category" href="{!! $sc->permalink !!}">
					<span>Categoria</span>
				</a>
				<a class="innerrelated-post__title" href="{!! $sc->permalink !!}">
					<h3>{!! $sc->title !!}</h3>
				</a>
			</article>
		</aside>

<!-- forms.twig macros -->
{% macro input(id, name, value, type, placeholder, dataValidation, required, css) %}
	{% if css|default(false) %}
		@asset('css/components/partials/forms.min.css')
	{% endif %}
	<input id="{{ id }}" type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|e }}" autocomplete="off" placeholder="{{ placeholder }}" data-validation="{{ dataValidation }}" required="{{ required }}"/>
{% endmacro %}

{% macro inputField(id, name, value, type, placeholder, dataValidation, required, label, css) %}
	{% if css|default(false) %}
		@asset('css/components/partials/forms.min.css')
	{% endif %}
	<div class="input-field">
		<label for="{{ id }}">{{ label }}</label>
		<input id="{{ id }}" type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|e }}" autocomplete="off" placeholder="{{ placeholder }}" data-validation="{{ dataValidation }}" required="{{ required }}"/>
	</div>
{% endmacro %}

{% macro textAreaField(id, name, value, placeholder, dataValidation, required, label, css) %}
	{% if css|default(false) %}
		@asset('css/components/partials/forms.min.css')
	{% endif %}
	<div class="input-field input-field--textarea">
		<label for="{{ id }}">{{ label }}</label>
		<textarea id="{{ id }}" name="{{ name }}" value="{{ value }}" placeholder="{{ placeholder }}" data-validation="{{ dataValidation }}" required="{{ required }}"></textarea>
	</div>
{% endmacro %}

{% macro selectField(id, label, name, placeholder, options, dataValidation, required, css, class) %}
	{% if css|default(false) %}
		@asset('css/components/partials/forms.min.css')
	{% endif %}
	<div class="input-field input-field--select {{ class }}">
		<label for="{{ id }}">{{ label }}</label>
		<select id="{{ id }}" name="{{ name }}" data-placeholder="{{ placeholder }}" data-validation="{{ dataValidation }}" required="{{ required }}" class="dropdown">
			{% if placeholder != "" %}
				<option selected disabled>{{ placeholder }}</option>
			{% endif %}
			{% for option in options %}
				<option value="{{ option.value }}">{{ option.label }}</option>
			{% endfor %}
		</select>
	</div>
{% endmacro %}

{% macro radiocouple(label1, label2, id, id1, id2, groupName, description, dataValidation, required, css) %}
	{% if css|default(false) %}
		@asset('css/components/partials/forms.min.css')
	{% endif %}
	<div class="newsletter__check">
		<div class="rad">
			<fieldset id="{{ id }}">
				<div>
					<input id="{{ id1 }}" type="radio" value="1" name="{{ groupName }}" data-validation="{{ dataValidation }}" required="{{ required }}" class="btn-switch__radio btn-switch__radio_yes"/>
					<input id="{{ id2 }}" type="radio" checked value="0" name="{{ groupName }}" data-validation="{{ dataValidation }}" required="{{ required }}" class="btn-switch__radio btn-switch__radio_no"/>
					<label for="{{ id1 }}" class="btn-switch__label btn-switch__label_yes">
						<span class="btn-switch__txt">{{ label1 }}</span>
					</label>
					<label for="{{ id2 }}" class="btn-switch__label btn-switch__label_no">
						<span class="btn-switch__txt">{{ label2 }}</span>
					</label>
				</fieldset>
				<label for="{{ id }}">{{ description }}</label>
			</div>
		</div>
	{% endmacro %}

	{% macro newsletterText() %}
		{% if css|default(false) %}
			@asset('css/components/partials/forms.min.css')
		{% endif %}
		<p>Compilando il presente form acconsento a ricevere le informazioni relative ai servizi di cui alla presente pagina ai sensi dell'<a href="#"><strong>informativa sulla privacy</strong></a>.</p>
	{% endmacro %}


	{% macro checkbox(label, value, checked, css) %}
		{% if css|default(false) %}
			@asset('css/components/partials/forms.min.css')
		{% endif %}

		<div class="input-field input-field--checkbox">
			<input id="{{ value }}" type="checkbox" class="input-field__checkbox" value="{{ value }}"></button>
		<label for="{{ value }}">{{ label }}</label>
	</div>
{% endmacro %}

{% macro newsletter(id, name, value, type, placeholder, dataValidation, required, extended, css) %}
	{% if css|default(false) %}
		@asset('css/components/partials/forms.min.css')
	{% endif %}
	<div class="newsletter__field">
		<input id="{{ id }}" type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|e }}" autocomplete="off" placeholder="{{ placeholder }}" data-validation="{{ dataValidation }}" required="{{ required }}"/>
		<button type="submit" class="cta cta--icon-left cta--enter newsletter__field__submit">
			<svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewbox="0 0 23 23"><path d="M11.979,17.642a.864.864,0,0,1-.479.1.864.864,0,0,1-.479-.1L0,11.7V22.529a.905.905,0,0,0,.958.958H22.042A.905.905,0,0,0,23,22.529V11.7Z" transform="translate(0 -0.487)" fill="#390b3a"/><path d="M5.788,12.075h0V1.917h11.5V12.075l4.888-2.683L19.2,7.188V.958A.905.905,0,0,0,18.246,0H4.829a.905.905,0,0,0-.958.958V7.283L.9,9.488Z" transform="translate(-0.037)" fill="#390b3a"/><rect width="7" height="2" transform="translate(8 5)" fill="#390b3a"/><rect width="7" height="2" transform="translate(8 9)" fill="#390b3a"/></svg>Iscriviti alla newsletter</button>
	</div>
	{% if extended|default(false) %}
		<div class="newsletter__block">
			<div class="newsletter__check">
				<div class="rad">
					<fieldset id="radP1a-fieldset">
						<div>
							<input id="radP1aYes" type="radio" value="1" name="trattamento_privacy" data-validation="privacy_radio" required="required"/>
							<label for="radP1aYes">
								<span>Si</span>
							</label>
						</div>
						<div>
							<input id="radP1aNo" type="radio" value="0" name="trattamento_privacy" data-validation="privacy_radio" required="required" class="btn-switch__label btn-switch__label_no"/>
							<label for="radP1aNo">
								<span>No</span>
							</label>
						</div>
					</fieldset>
					<label for="radP1a-fieldset">Ho letto e acconsento l'<a href="http://www.greenstyle.it/privacy-policy" rel="nofollow" target="_blank">informativa sulla privacy</a>
					</label>
				</div>
			</div>
		</div>

		<div class="newsletter__block">
			<div class="newsletter__check">
				<div class="rad">
					<fieldset id="radP2a-fieldset">
						<div>
							<input id="radP2aYes" type="radio" value="1" name="n_inviadem" data-validation=""/>
							<label for="radP2aYes">
								<span>Si</span>
							</label>
						</div>
						<div>
							<input id="radP2aNo" type="radio" value="0" name="n_inviadem" data-validation=""/>
							<label for="radP2aNo">
								<span>No</span>
							</label>
						</div>
					</fieldset>
					<label for="radP2a-fieldset">Acconsento al trattamento dei dati per
																																										                    attività di marketing</label>
				</div>
			</div>
		</div>
	{% endif %}
{% endmacro %}

{% macro search(name, value, type, placeholder, css) %}
	{% if css|default(false) %}
		@asset('css/components/partials/forms.min.css')
	{% endif %}
	<div class="search__field">
		<input type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|e }}" autocomplete="off" placeholder="{{ placeholder }}"/>
		<button type="submit" class="search__field__submit">
			<a href="#">
				<svg xmlns="http://www.w3.org/2000/svg" width="22.414" height="22.414" viewbox="0 0 22.414 22.414">
					<g transform="translate(1.414 1)"><line y1="6.344" x2="6.344" transform="translate(0 13.656)" fill="none" stroke="#fff" stroke-linecap="square" stroke-miterlimit="10" stroke-width="2"/><circle cx="8" cy="8" r="8" transform="translate(4)" fill="none" stroke="#fff" stroke-linecap="square" stroke-miterlimit="10" stroke-width="2"/></g>
				</svg>
			</a>
		</button>
	</div>
{% endmacro %}

{% macro pagination(name, value, type, min, max, placeholder, css) %}
	{% if css|default(false) %}
		@asset('css/components/partials/forms.min.css')
	{% endif %}
	<div class="pagination__field">
		<input type="{{ type|default('number') }}" name="{{ name }}" value="{{ value|e }}" min="{{ min }}" max="{{ max }}" autocomplete="off" placeholder="{{ placeholder }}"/>
		<button type="submit" class="pagination__field__submit">vai</button>
	</div>
{% endmacro %}

{% macro innersearch(name, value, type, placeholder, css) %}
	{% if css|default(false) %}
		@asset('css/components/partials/forms.min.css')
	{% endif %}
	<div class="innersearch search__field">
		<svg xmlns="http://www.w3.org/2000/svg" width="36.74" height="37.74" viewbox="0 0 36.74 37.74">
			<g transform="translate(2.828 1.944)"><line y1="10.141" x2="10.141" transform="translate(0 22.827)" fill="none" stroke="#ff007b" stroke-linecap="square" stroke-miterlimit="10" stroke-width="4"/><circle cx="13" cy="13" r="13" transform="translate(5.911 0.056)" fill="none" stroke="#ff007b" stroke-linecap="square" stroke-miterlimit="10" stroke-width="4"/></g>
		</svg>
		<input type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|e }}" autocomplete="off" placeholder="{{ placeholder }}"/>
		<button type="submit" class="search__field__submit btn">
			<a href="#">Cerca</a>
		</button>
	</div>
{% endmacro %}

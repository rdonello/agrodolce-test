<!-- partial-innercard-static-content.twig -->

<div class="partial-innercard-static-content">
		@asset('css/components/partials/partial-innercard-static-content.min.css')
	<div class="single-card__nav">
		<p>Pagina
			<span class="current">{!! $paged !!}</span>
			di
			<strong>{!! $total_cards !!}</strong>
		</p>
		{{-- Social share --}}
		@include('components.partials.partial-social')

		<a href="" class="card-button-next " title="Card precedente">
			<svg xmlns="http://www.w3.org/2000/svg" width="23.856" height="17.507" viewbox="0 0 23.856 17.507">
				<g transform="translate(1 1.414)"><path d="M0,0H20.9" transform="translate(0 7.522)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="2"/><path d="M0,14.678,7.339,7.339,0,0" transform="translate(14.517)" fill="rgba(0,0,0,0)" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/></g>
			</svg>
			<span>Pagina 4</span>
		</a>
		<a href="{!! $get_nav['prev'] !!}" class="card-button-prev " title="Card precedente">
			<svg xmlns="http://www.w3.org/2000/svg" width="23.86" height="17.506" viewbox="0 0 23.86 17.506">
				<g transform="translate(1 1.414)"><path d="M20.9,0H0" transform="translate(0.956 7.522)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="2"/><path d="M7.339,14.678,0,7.339,7.339,0" transform="translate(0 0)" fill="rgba(0,0,0,0)" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/></g>
			</svg>
			<span>Pagina {!! $paged-1 !!}</span>
		</a>
	</div>

	{{-- card content se previsto --}}
	<div class="editorial">
		{!! $paragraph_array[$card]['html_card_paragrafo'] !!}
	</div>
	@if ($paged < $total_cards)
	<div class="static-content__cta">
		<a class="btn" href="{!! $get_nav['next'] !!}">
			<span>Continua</span>
			<svg xmlns="http://www.w3.org/2000/svg" width="23" height="16.091" viewbox="0 0 23 16.091">
				<g transform="translate(22.706 18.884) rotate(180)"><line x1="22" transform="translate(0.206 10.655)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/><path d="M7.839,3.5.5,10.839l7.339,7.339" transform="translate(-0.154 0)" fill="rgba(0,0,0,0)" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/></g>
			</svg>
		</a>
	</div>
	@endif
</div>

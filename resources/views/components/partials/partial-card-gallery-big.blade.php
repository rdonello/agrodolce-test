<!-- partial-card-gallery-big.twig -->

<article class="partial-card-gallery-big">
	@asset('css/components/partials/partial-card-gallery-big.min.css')
	<div class="gallery-big__figure">
		<div class="gallery-big__image-wrapper">
			<span data-link="#" class="gallery-big__photo-number">+15</span>
			<picture data-link="{!! get_permalink() !!}">
				<source
				data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(725,551)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1450,1102)) !!} 2x" media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(725,551)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1450,1102)) !!} 2x" alt="{!! get_the_title() !!}"/>
			</picture>
		</div>
	</div>


			{!! agrodolce_tbm_get_label("card-gallery__story", "name") !!}


	<div class="card__title">
		<a href="{!! get_permalink() !!}">
			<h3>{!! get_the_title() !!}</h3>
		</a>
	</div>
</article>

<!-- partial-card-cocktail-hero.twig -->

<article class="partial-card-cocktail-hero">
	@asset('css/components/partials/partial-card-cocktail-hero.min.css')
	<div class="cocktail-hero__figure span-8">
		<div class="cocktail-hero__image-wrapper">
			<picture
					data-link="{!! get_permalink() !!}">
				<!--[if IE 9]>
				<video style="display: none;"><![endif]-->
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(359,180)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(718,360)) !!} 2x"
						media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(767,385)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1534,770)) !!} 2x"
					 src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(767,385)) !!}"
					 alt="{!! tbm_get_the_post_thumbnail_alt(get_the_ID()) !!}"/>
			</picture>
		</div>
	</div>
	<div class="cocktail-hero__content span-6">
		<a class="cocktail-hero__label" href="!! get_post_type_archive_link('ricetta') !!}">Cocktail</a>

		{!! agrodolce_tbm_get_label("cocktail-hero__story", "name") !!}


		<a class="cocktail-hero__title" href="{!! get_permalink() !!}">
			<h2>{!! the_title() !!}</h2>
		</a>

		<p class="cocktail-hero__abstract abstract">{!! the_excerpt() !!}</p>
		<div class="cocktail-hero__details">
			<ul>
				<li>
					<svg xmlns:svg="http://www.w3.org/2000/svg" width="20" height="20" id="svg2472"
						 viewbox="0 0 362.48 363.27">
						<g id="g2482">
							<path id="path2484" fill="#ff007b"
								  d="m0 0h362.48l-157.52 194.57v124.61h87.2c29.25 0 29.5 44.09 0 44.09h-217.5c-29.504 0-29.504-44.09-0.004-44.09h85.714v-125.57l-160.37-193.61z"/>
						</g>
					</svg>
					<span>{!! get_field('bicchiere') !!}</span>
				</li>
				<li>
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 24 24">
						<path d="M13,21H5v2a.945.945,0,0,0,1,1H18a.945.945,0,0,0,1-1V21Z" fill="#ff007b"/>
						<path d="M18,4h-.3A5.99,5.99,0,0,0,12,0,6.087,6.087,0,0,0,6.3,4H6A5.991,5.991,0,0,0,5,15.9V19h6V15h2v4h6V15.9A5.991,5.991,0,0,0,18,4Z"
							  fill="#ff007b"/>
					</svg>
					<span>{!! get_field('difficolta') !!}</span>
				</li>
			</ul>
		</div>
	</div>
</article>

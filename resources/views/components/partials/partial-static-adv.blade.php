<!-- partial-static-adv.twig -->
<div class="partial-static-adv">
	@asset('css/components/partials/partial-static-adv.min.css')
	<div class="static-element">
		<div class="static-adv">
			{{--  Static adv - post_header --}}
			@if($banner == 'post_header')
				{!! tbm_get_the_banner( 'POST_HEADER','','',false ) !!}
			@endif

			{{--  Static adv - post_footer --}}
			@if($banner == 'post_footer')
				{!! tbm_get_the_banner( 'POST_FOOTER','','',false,false ) !!}
			@endif

			{{--  Static adv - Mobile Top --}}
			@if($banner == 'mobile_top')
				{!! tbm_get_the_banner( 'BOX_MOBILE_INSIDE_TOP','','',false,false ) !!}
			@endif

			{{--  Static adv - Mobile Bottom --}}
			@if($banner == 'mobile_bottom')
				{{-- tbm_get_the_banner( 'BOX_MOBILE_INSIDE_BOTTOM','','',false,false ) --}}
			@endif

			{{--  Static adv - Desktop Top --}}
			@if($banner == 'desktop_top')
				{!! tbm_get_the_banner( 'BOX_DESKTOP_SIDE_TOP','','',false,false ) !!}
			@endif

			{{--  Static adv - Mobile Bottom --}}
			@if($banner == 'desktop_bottom')
				{!! tbm_get_the_banner( 'BOX_DESKTOP_SIDE_BOTTOM','','',false,false ) !!}
			@endif

		</div>
	</div>
</div>

<!-- partial-card-post-list.twig -->
<div class="partial-card-post-list">
		@asset('css/components/partials/partial-card-post-list.min.css')
	<div class="card-post-list__figure">
		{!! tbm_agrodolce_get_rubrica() !!}
		<div class="card-post-list__image-wrapper">
			<picture data-link="{!! get_permalink() !!}">
				@if (isset($ad_loop) && $ad_loop === 0)
					<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x"
						 src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}"
						 alt="{!! tbm_get_the_post_thumbnail_alt()!!}"/>
				@else
					<img class="lazyload"
						 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x"
						 alt="{!! tbm_get_the_post_thumbnail_alt()!!}"/>
				@endif
			</picture>
		</div>
	</div>
	<div class="card-post-list__content">
		{!! agrodolce_tbm_get_label("card-post-list__story", "name") !!}
		<a class="card-post-list__title" href="{!! get_permalink() !!}">
			<h3>{!! get_the_title() !!}</h3>
		</a>
		<span class="author__name">di {!! tbm_get_the_author() !!}</span>
		<p class="card-post-list__abstract">@php the_excerpt() @endphp</p>
	</div>
</div>

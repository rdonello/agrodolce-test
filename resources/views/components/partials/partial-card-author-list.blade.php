<!-- partial-card-author-list.twig -->

<div class="partial-card-author-list">
	{% if loop.index <= 1 and partialcardauthorlist is not defined or loop.index is not defined and partialcardauthorlist is not defined %}

		@asset('css/components/partials/partial-card-author-list.min.css')

	{% endif %}
	<div class="card-author-list__figure">
		<div class="card-author-list__image-wrapper">
			<picture data-link="{!! get_permalink() !!}">
				<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x" alt="{!! the_title() !!}"/>
			</picture>
		</div>
	</div>
	<div class="card-author-list__content">
		<a class="card-author-list__title" href="{!! get_permalink() !!}">
			<h3>{{ queryItem.name }}</h3>
		</a>
		<p class="card-author-list__abstract">{!! the_excerpt() !!}</p>
	</div>
</div>

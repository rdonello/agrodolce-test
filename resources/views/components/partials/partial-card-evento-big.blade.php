<!-- partial-card-evento-big.twig -->

<article class="partial-card-evento-big">
		@asset('css/components/partials/partial-card-evento-big.min.css')
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x" alt="{!! get_the_title() !!}"/>
		</picture>
	</div>


			{!! agrodolce_tbm_get_label("card-evento__story", "name") !!}


	<div class="card__title">
		<a href="{!! get_permalink() !!}">
			<h3>{!! get_the_title() !!}</h3>
		</a>
	</div>
	<div class="card__date">
		<svg xmlns="http://www.w3.org/2000/svg" width="20" height="31" viewbox="0 0 32.069 31">
			<g transform="translate(-2 -1)"><path d="M32.74,30.912l-8.552,8.552a.534.534,0,0,1-.756,0l-4.276-4.276a.534.534,0,1,1,.756-.756l3.9,3.9,8.174-8.174a.534.534,0,1,1,.756.756Z" transform="translate(-7.914 -13.5)" fill="#ff0f8f"/><path d="M15.534,8.483A.534.534,0,0,1,15,7.948V1.534a.534.534,0,0,1,1.069,0V7.948A.534.534,0,0,1,15.534,8.483Z" transform="translate(-6.052)" fill="#ff0f8f"/><path d="M32.466,7H28.19v3.741a1.6,1.6,0,0,1-3.207,0V7h-13.9v3.741a1.6,1.6,0,1,1-3.207,0V7H3.6A1.6,1.6,0,0,0,2,8.6V33.19a1.6,1.6,0,0,0,1.6,1.6H32.466a1.6,1.6,0,0,0,1.6-1.6V8.6a1.6,1.6,0,0,0-1.6-1.6ZM33,33.19a.534.534,0,0,1-.534.534H3.6a.534.534,0,0,1-.534-.534V15.552H33Z" transform="translate(0 -2.793)" fill="#ff0f8f"/><path d="M47.534,8.483A.534.534,0,0,1,47,7.948V1.534a.534.534,0,0,1,1.069,0V7.948A.534.534,0,0,1,47.534,8.483Z" transform="translate(-20.948)" fill="#ff0f8f"/></g>
		</svg>
		<span> {{queryItem.days}} {{queryItem.month}}</span>
	</div>
</article>

<!-- partial-card-podcast-hero.twig -->

<article class="partial-card-podcast-hero">
		@asset('css/components/partials/partial-card-podcast-hero.min.css')
	<div class="podcast-hero__content span-7">

				{!! agrodolce_tbm_get_label("podcast-hero__story", "name") !!}


		<a class="podcast-hero__title" href="{!! get_permalink() !!}">
			<h2>{!! get_the_title() !!}</h2>
		</a>

		<p class="podcast-hero__abstract abstract">@php the_excerpt() @endphp</p>
	</div>
	<div class="podcast-hero__figure span-5">
		<div class="podcast-hero__image-wrapper">
			<picture data-link="{!! get_permalink() !!}">
				<svg xmlns="http://www.w3.org/2000/svg" width="86" height="86" viewbox="0 0 86 86">
					<g class="icon-hover-fill" transform="translate(-491 -10731)"><circle cx="43" cy="43" r="43" transform="translate(491 10731)" fill="#ff007b"/><g transform="translate(509.447 10750)"><path d="M12.394,30.135a11.87,11.87,0,0,0-.868,23.408c.289.059.578.095.868.132Z" transform="translate(0 -7.256)" fill="#fff"/><path d="M20.375,30.236A12.055,12.055,0,0,0,18,30V53.747h.012a11.827,11.827,0,0,0,2.362-.239.742.742,0,0,0,.594-.727V30.965A.742.742,0,0,0,20.375,30.236Z" transform="translate(-4.122 -7.221)" fill="#fff"/><path d="M44.968,30a11.83,11.83,0,0,0-2.375.239.742.742,0,0,0-.594.727V52.782a.742.742,0,0,0,.594.728,12.062,12.062,0,0,0,2.375.237Z" transform="translate(-10.311 -7.221)" fill="#fff"/><path d="M44.073,19.068V24.72a13.268,13.268,0,0,1,1.484,1.558v-7.21A17.087,17.087,0,0,0,28.489,2H21.068A17.087,17.087,0,0,0,4,19.068v7.2a13.21,13.21,0,0,1,1.484-1.544V19.068A15.6,15.6,0,0,1,21.068,3.484h7.421A15.6,15.6,0,0,1,44.073,19.068Z" transform="translate(-0.511)" fill="#fff"/><path d="M56.4,35.352a11.8,11.8,0,0,0-7.536-5.079c-.289-.059-.578-.095-.868-.132V53.687a11.874,11.874,0,0,0,8.4-18.335Z" transform="translate(-11.859 -7.258)" fill="#fff"/></g>
					</g>
				</svg>
				<!--[if IE 9]><podcast style="display: none;"><![endif]-->
				<source srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x" media="(max-width: 736px)"/>
				<!--[if IE 9]></podcast><![endif]-->
				<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(725,551)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1450,1102)) !!} 2x"
					 src="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(725,551)) !!}"
					 alt="{!! tbm_get_the_post_thumbnail_alt(get_the_ID()) !!}"/>
			</picture>
		</div>
	</div>
</article>

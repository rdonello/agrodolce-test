<!-- partial-innerrelated-podcast.twig -->
<aside class="partial-innerrelated-podcast">
	@asset('css/components/partials/partial-innerrelated-podcast.min.css')
	<article class="innerrelated-podcast__wrapper">
		<div class="innerrelated-podcast__image-wrapper">
			<picture data-link="{!! $sc->permalink !!}">
				<svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewbox="0 0 72 72" style="">
					<g class="icon-hover-fill" transform="translate(-533 -488)">
						<path d="M-559-316a35.773,35.773,0,0,1-14.013-2.829,35.882,35.882,0,0,1-11.443-7.715,35.882,35.882,0,0,1-7.715-11.443A35.774,35.774,0,0,1-595-352a35.775,35.775,0,0,1,2.829-14.013,35.883,35.883,0,0,1,7.715-11.443,35.881,35.881,0,0,1,11.443-7.715A35.775,35.775,0,0,1-559-388a35.774,35.774,0,0,1,14.013,2.829,35.88,35.88,0,0,1,11.443,7.715,35.882,35.882,0,0,1,7.715,11.443A35.775,35.775,0,0,1-523-352a35.775,35.775,0,0,1-2.829,14.013,35.882,35.882,0,0,1-7.715,11.443,35.881,35.881,0,0,1-11.443,7.715A35.772,35.772,0,0,1-559-316Z"
							  transform="translate(1128 876)" fill="#ff0030"/>
						<g transform="translate(554 479)">
							<g transform="translate(0 31)">
								<path d="M15,31A14.906,14.906,0,0,0,0,45.734v6.933a2.622,2.622,0,0,0,2.636,2.6H3.52V46.6H2.636a2.639,2.639,0,0,0-.879.159V45.734A13.15,13.15,0,0,1,15,32.678a13.15,13.15,0,0,1,13.24,13.057v1.026a2.639,2.639,0,0,0-.879-.159H26.48v8.666h.879a2.622,2.622,0,0,0,2.64-2.6V45.734A14.906,14.906,0,0,0,15,31Z"
									  transform="translate(0 -31)" fill="#fff"/>
							</g>
							<g transform="translate(5.138 42.683)">
								<path d="M93.146,271H92.1a2.1,2.1,0,0,0-2.1,2.1v10.488a2.1,2.1,0,0,0,2.1,2.1h1.049a1.049,1.049,0,0,0,1.049-1.049v-12.59A1.049,1.049,0,0,0,93.146,271Z"
									  transform="translate(-90 -271)" fill="#fff"/>
							</g>
							<g transform="translate(20.665 42.683)">
								<path d="M364.1,271h-1.049A1.049,1.049,0,0,0,362,272.049v12.586a1.049,1.049,0,0,0,1.049,1.049H364.1a2.1,2.1,0,0,0,2.1-2.1V273.1a2.1,2.1,0,0,0-2.1-2.1Z"
									  transform="translate(-362 -271)" fill="#fff"/>
							</g>
						</g>
					</g>
				</svg>

				<img class="lazyload" data-src="{!! tbm_wp_get_attachment_image_url($sc->image_id,array(300,200)) !!}" alt="{!! $sc->title !!}"/>
			</picture>
		</div>

		<a class="innerrelated-podcast__category" href="{!! $sc->permalink !!}">
			<span>{!! $sc->tax !!}</span>
		</a>
		<a class="innerrelated-podcast__title" href="{!! $sc->permalink !!}">
			<h3>{!! $sc->title !!}</h3>
		</a>
	</article>
</aside>

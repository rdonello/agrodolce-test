<!-- partial-innerrelated-products.twig -->
<div class="partial-innerrelated-products lazyload" data-script="js/components/partials/partial-innerrelated-products.js">
		@asset('css/components/partials/partial-innerrelated-products.min.css')
	<h2>Occorrente per la ricetta</h2>
	<div class="swiper-container">
		<div class="swiper-wrapper">
			@foreach($products as $v)
				<div class="swiper-slide product-slide">
					<div class="product-info">
						@if(!empty($v['featured_url']))
						<picture>
							<img srcset="{{ $v['featured_url'] }}" alt="{{ $v['name'] }}"/>
						</picture>
						@endif
						<h4>{{ $v['name'] }}</h4>
						<span class="product-price">{{ $v['price']}} &euro;</span>
						<a href=""{{ $v['link'] }} class="product-seller">{{ $v['seller'] }}</a>
						@if(!empty($v['link']))
						<a href="{{ $v['link'] }}" class="btn">acquista&nbsp;
							<svg xmlns="http://www.w3.org/2000/svg" width="18.751" height="18.968" viewbox="0 0 18.751 18.968">
								<g transform="translate(17.98 9.466) rotate(135)"><line x1="18.433" transform="translate(0 5.995)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/><path d="M6.149,0,0,6.149,6.149,12.3" transform="translate(0.117)" fill="rgba(0,0,0,0)" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/></g>
							</svg>
						</a>
						@endif
					</div>
				</div>
			@endforeach
		</div>

		<!-- Add Arrows -->
		<div class="swiper-button-next">
			<svg class="icon icon--standard icon--next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12" height="21.035" viewbox="0 0 12 21.035">
				<defs>
					<filter id="a" x="0" y="0" width="12" height="21.035" filterunits="userSpaceOnUse"><feOffset input="SourceAlpha"/><feGaussianBlur result="b"/><feFlood flood-opacity="0.161"/><feComposite operator="in" in2="b"/><feComposite in="SourceGraphic"/></filter>
				</defs>
				<g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#a)"><path d="M9,28.03l8.558-8.512L9,11.006,11.016,9l9.566,9.515a1.412,1.412,0,0,1,0,2.005l-9.566,9.515Z" transform="translate(-9 -9)" fill="#000"/></g>
			</svg>
		</div>
		<div class="swiper-button-prev">
			<svg class="icon icon--standard icon--prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12" height="21.035" viewbox="0 0 12 21.035">
				<defs>
					<filter id="a" x="0" y="0" width="12" height="21.035" filterunits="userSpaceOnUse"><feOffset input="SourceAlpha"/><feGaussianBlur result="b"/><feFlood flood-opacity="0.161"/><feComposite operator="in" in2="b"/><feComposite in="SourceGraphic"/></filter>
				</defs>
				<g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#a)"><path d="M21,28.029l-8.557-8.512L21,11.006,18.984,9,9.418,18.515a1.412,1.412,0,0,0,0,2.006l9.566,9.515Z" transform="translate(-9 -9)" fill="#000"/></g>
			</svg>
		</div>
	</div>

</div>

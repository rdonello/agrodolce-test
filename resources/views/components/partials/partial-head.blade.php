<!-- partial-head.twig -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, maximum-scale=5, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
{!! $tbm_critical !!}
<link rel="apple-touch-icon" sizes="180x180" href="@asset('images/apple-touch-icon.png')">
<link rel="icon" type="image/png" sizes="32x32" href="@asset('images/favicon-32x32.png')">
<link rel="icon" type="image/png" sizes="192x192" href="@asset('images/android-chrome-192x192.png')">
<link rel="icon" type="image/png" sizes="16x16" href="@asset('images/favicon-16x16.png')">
<link rel="shortcut icon" href="@asset('images/favicon.ico')">
<meta name="msapplication-TileColor" content="#2d0008">
<meta name="theme-color" content="#2d0008">

@php
    echo \TbmCommon\Templates\Header::getPrefetch();

    if ( function_exists ('tbm_get_the_banner') ) {
        tbm_get_the_banner( 'TRIBOOADV', ' ', ' ', true, true );
    }

    if ( function_exists( 'tbm_head' ) ) {
        tbm_head();
    }

    wp_head();

    if ( function_exists ('tbm_get_the_banner') ) {
        tbm_get_the_banner( 'HEAD', ' ', ' ', true, true );
    }
@endphp


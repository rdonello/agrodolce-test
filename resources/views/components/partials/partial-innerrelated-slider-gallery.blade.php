<!-- partial-innerrelated-slider-gallery.twig -->
@if(count((array)$sc->gallery) > 1)
		<aside class="partial-innerrelated-slider-gallery lazyload" data-script="js/components/partials/partial-innerrelated-slider-gallery.js">
				@asset('css/components/partials/partial-innerrelated-slider-gallery.min.css')
			<div class="swiper-container">
				<div class="swiper-wrapper">
					@foreach($sc->gallery as $photo)
						@if ($loop->index <= 4)
							<div class="swiper-slide photo-slide">
								<img src="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(640,290)) !!}" alt=""/>
							</div>
						@endif
						@if ($loop->index == 5)
							<div class="swiper-slide photo-slide gallery-slide">
								<a href="{!! $sc->permalink !!}/img/6" class="gallery__photo-number">
									<h3>{!! $sc->title !!}</h3>
									<span>Vedi le altre {!! count($sc->gallery) !!} fotografie ›</span>
								</a>
								<img src="{!! tbm_wp_get_attachment_image_url($photo['ID'],array(640,290)) !!}" alt=""/>
							</div>
						@endif
					@endforeach
				</div>
			</div>

			<div class="swiper-pagination"></div>

			<div class="swiper-button-next">
			</div>
			<div class="swiper-button-prev">
			</div>
		</aside>
@endif

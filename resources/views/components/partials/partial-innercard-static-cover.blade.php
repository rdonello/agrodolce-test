<!-- partial-innercard-static-cover.twig -->

<div class="partial-innercard-static-cover">
		@asset('css/components/partials/partial-innercard-static-cover.min.css')
	{{-- Card navigation --}}
	<div
		class="single-card__hero">
		{{-- Card featured image --}}
		<div
			class="single-card__image">
			{{-- Cover featured image --}}
			<div class="featured-image">
				<picture>
					<!--[if IE 9]><video style="display: none;"><![endif]-->
					<source
					srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x" media="(max-width: 736px)"/>
					<!--[if IE 9]></video><![endif]-->
					<img srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(1150,647)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(2300,1294)) !!} 2x" alt="{!! tbm_get_the_post_thumbnail_alt() !!}"/>

				</picture>
				{{-- Social share --}}
				@include('components.partials.partial-social')

			</div>
			<div
				class="single-card__abstract">
				{{-- Card abstract --}}
				<p class="abstract">@php the_excerpt() @endphp</p>

				<div class="static-cover__cta">
					<a class="btn" href="{!! $get_nav['next'] !!}">
						<span>Continua</span>
						<svg xmlns="http://www.w3.org/2000/svg" width="23" height="16.091" viewbox="0 0 23 16.091">
							<g transform="translate(22.706 18.884) rotate(180)"><line x1="22" transform="translate(0.206 10.655)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/><path d="M7.839,3.5.5,10.839l7.339,7.339" transform="translate(-0.154 0)" fill="rgba(0,0,0,0)" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/></g>
						</svg>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

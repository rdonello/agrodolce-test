<!-- partial-card-evento-hero.twig -->

<article class="partial-card-evento-hero">
		@asset('css/components/partials/partial-card-evento-hero.min.css')
	<div class="evento-hero__figure span-7">
		<div class="evento-hero__image-wrapper">
			<picture
				data-link="{!! get_permalink() !!}">
				<!--[if IE 9]><video style="display: none;"><![endif]-->
				<source
				srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x" media="(max-width: 736px)"/>
				<!--[if IE 9]></video><![endif]-->
				<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x" alt="{!! get_the_title() !!}"/>
			</picture>
		</div>
	</div>
	<div class="evento-hero__content span-5">

				{!! agrodolce_tbm_get_label("evento-hero__story","name") !!}


		<a class="evento-hero__title" href="{!! get_permalink() !!}">
			<h2>{!! get_the_title() !!}</h2>
		</a>
		<p class="evento-hero__abstract abstract">@php the_excerpt() @endphp</p>
		<div class="card-evento-hero__details">
			<ul>
				<li>
					<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewbox="0 0 32.069 31">
						<g transform="translate(-2 -1)"><path d="M32.74,30.912l-8.552,8.552a.534.534,0,0,1-.756,0l-4.276-4.276a.534.534,0,1,1,.756-.756l3.9,3.9,8.174-8.174a.534.534,0,1,1,.756.756Z" transform="translate(-7.914 -13.5)" fill="#ff0f8f"/><path d="M15.534,8.483A.534.534,0,0,1,15,7.948V1.534a.534.534,0,0,1,1.069,0V7.948A.534.534,0,0,1,15.534,8.483Z" transform="translate(-6.052)" fill="#ff0f8f"/><path d="M32.466,7H28.19v3.741a1.6,1.6,0,0,1-3.207,0V7h-13.9v3.741a1.6,1.6,0,1,1-3.207,0V7H3.6A1.6,1.6,0,0,0,2,8.6V33.19a1.6,1.6,0,0,0,1.6,1.6H32.466a1.6,1.6,0,0,0,1.6-1.6V8.6a1.6,1.6,0,0,0-1.6-1.6ZM33,33.19a.534.534,0,0,1-.534.534H3.6a.534.534,0,0,1-.534-.534V15.552H33Z" transform="translate(0 -2.793)" fill="#ff0f8f"/><path d="M47.534,8.483A.534.534,0,0,1,47,7.948V1.534a.534.534,0,0,1,1.069,0V7.948A.534.534,0,0,1,47.534,8.483Z" transform="translate(-20.948)" fill="#ff0f8f"/></g>
					</svg>
					<span class="card__date">
						{{queryItem.days}}
						{{queryItem.month}}</span>
				</li>
				<li>
					<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewbox="0 0 27.81 30.459">
						<g fill="#ff007b">
							<path d="M25.925 13.03a12.27 12.27 0 01-1.802 10.911 24.366 24.366 0 00-2.931-.89c-.512-.534-1.936-1.876-3.147-1.707a5.108 5.108 0 00-2.562 1.548.647.647 0 00-.171.462 12.255 12.255 0 01-.04 1.561 1.939 1.939 0 00-1.781.175 1.992 1.992 0 00-.568 1.785 2.138 2.138 0 00-1.258 1 1.555 1.555 0 00.089 1.085 12.555 12.555 0 01-4.391-1.64 8.08 8.08 0 00.968-1.62 3.251 3.251 0 001.033-1.328 3.468 3.468 0 00.072-1.67c.268-.819.593-2.113.23-2.744a3.2 3.2 0 00-1.737-1.092 3.13 3.13 0 00-1.967-1.244.861.861 0 01-.837-.912 5.754 5.754 0 01.824-2.144A2.9 2.9 0 007.3 13.423a5.2 5.2 0 00.237-1.3c.264-.5 1.007-2.118.366-3.258a2.154 2.154 0 00-1.173-.794l-.712-1.049A12.6 12.6 0 0112.962 4.3a7.254 7.254 0 01.593-1.334A13.752 13.752 0 1027.81 16.704a13.524 13.524 0 00-.981-5.043c-.285.472-.587.931-.904 1.369z"></path>
							<path d="M19.705 0a7.229 7.229 0 00-7.368 7.221c0 4.844 6.6 10.764 6.879 11.014a.737.737 0 00.978 0c.281-.25 6.879-6.17 6.879-11.014A7.229 7.229 0 0019.705 0zm0 9.579a2.21 2.21 0 112.21-2.21 2.21 2.21 0 01-2.21 2.21z"></path>
						</g>
					</svg>
					<span class="card__place">
						{{queryItem.place}}</span>
				</li>
			</ul>
		</div>


	</div>
</article>

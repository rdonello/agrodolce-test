<!-- partial-slider-trend.twig -->
@if($lifestyle_menu || $recipe_menu)
	<div class="partial-slider-trend">
		@asset('css/components/partials/partial-slider-trend.min.css')
		<div class="slider-trend-container">
			@if( $lifestyle_menu )
				{!! wp_nav_menu($lifestyle_menu) !!}
			@elseif($recipe_menu)
				{!! wp_nav_menu($recipe_menu) !!}
			@endif
		</div>
	</div>
@endif

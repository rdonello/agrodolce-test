<div class="partial-social">
	@asset('css/components/partials/partial-social.min.css')
	<label for="reveal-social" class="icon-share">
		<svg xmlns="http://www.w3.org/2000/svg" width="63" height="63" viewbox="0 0 63 63">
			<g transform="translate(0.054 0.006)"><circle cx="31.5" cy="31.5" r="31.5" transform="translate(-0.054 -0.006)" fill="#ff007b"/><g transform="translate(20.052 19.181)"><path d="M17.755,9.139A4.6,4.6,0,1,0,13.3,5.558L8.4,8.623A4.523,4.523,0,0,0,5.57,7.616a4.57,4.57,0,0,0,0,9.139A4.523,4.523,0,0,0,8.4,15.748l4.9,3.065a4.566,4.566,0,1,0,4.455-3.581,4.523,4.523,0,0,0-2.826,1.007l-4.9-3.065a4.334,4.334,0,0,0,0-1.977l4.9-3.065A4.523,4.523,0,0,0,17.755,9.139Z" transform="translate(-1)" fill="#fff"/></g>
			</g>
		</svg>
	</label>
	<input type="checkbox" id="reveal-social" role="button">

	<div class="icon-social">
@if(tbm_yoast_social_accounts('fb'))
		<a href="https://www.facebook.com/dialog/share?app_id={{ $tbm_fb_app_id }}&display=page&href={!! $share_link !!}&redirect_uri={!! $share_link !!}" class="icon-facebook">
			<svg xmlns="http://www.w3.org/2000/svg" width="63" height="63" viewbox="0 0 63 63">
				<g transform="translate(17380 12371)">
					<g transform="translate(-17379.945 -12370.993)"><circle cx="31.5" cy="31.5" r="31.5" transform="translate(-0.054 -0.006)" fill="#ff007b"/></g>
					<g transform="translate(-17377 -12276.001)"><path d="M19.84,0H1.159A1.159,1.159,0,0,0,0,1.16V19.841A1.159,1.159,0,0,0,1.159,21H11.217V12.863H8.48V9.7h2.737V7.362a3.818,3.818,0,0,1,4.075-4.189,22.7,22.7,0,0,1,2.445.125V6.132H16.058c-1.312,0-1.575.626-1.575,1.544V9.7h3.14l-.406,3.164H14.489V21H19.84A1.16,1.16,0,0,0,21,19.841V1.16A1.159,1.159,0,0,0,19.84,0Z" transform="translate(18 -74)" fill="#fff"/></g>
				</g>
			</svg>
		</a>
@endif
@if(tbm_yoast_social_accounts('tw'))
		<a href="https://twitter.com/share?url={!! $share_link !!}&text={!! rawurlencode( get_the_title() ) !!}" class="icon-twitter">
			<svg xmlns="http://www.w3.org/2000/svg" width="63" height="63" viewbox="0 0 63 63">
				<g transform="translate(17380 12290)">
					<g transform="translate(-17419.945 -12298.993)"><circle cx="31.5" cy="31.5" r="31.5" transform="translate(39.946 8.994)" fill="#ff007b"/></g>
					<g transform="translate(-17238 -12402.5)">
						<g transform="translate(-121 135.001)"><path d="M22,50.116a9.4,9.4,0,0,1-2.6.712,4.485,4.485,0,0,0,1.984-2.493,9.013,9.013,0,0,1-2.86,1.092,4.51,4.51,0,0,0-7.8,3.084,4.643,4.643,0,0,0,.1,1.028,12.765,12.765,0,0,1-9.3-4.715,4.511,4.511,0,0,0,1.386,6.028A4.454,4.454,0,0,1,.88,54.3v.049a4.538,4.538,0,0,0,3.613,4.432,4.5,4.5,0,0,1-1.182.148,3.988,3.988,0,0,1-.854-.077A4.553,4.553,0,0,0,6.672,62a9.063,9.063,0,0,1-5.592,1.925A8.448,8.448,0,0,1,0,63.851a12.7,12.7,0,0,0,6.919,2.024A12.749,12.749,0,0,0,19.756,53.04c0-.2-.007-.392-.016-.583A9,9,0,0,0,22,50.116Z" transform="translate(0 -48.001)" fill="#fff"/></g>
					</g>
				</g>
			</svg>
		</a>
@endif
	</div>
</div>

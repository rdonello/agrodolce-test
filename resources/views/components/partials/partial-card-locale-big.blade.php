<!-- partial-card-ristorante-big.twig -->
@php $data=get_ristorante_data_object(get_the_ID()); @endphp
<article class="partial-card-ristorante-big">
	@asset('css/components/partials/partial-card-ristorante-big.min.css')
	<div class="card__thumbnail--big">
		<picture data-link="{!! get_permalink() !!}">
			<img class="lazyload"
				 data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(430,327)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(860,654)) !!} 2x"
				 alt="{!! get_the_title() !!}"/>
		</picture>
	</div>

	<div class="card__content">
		{!! agrodolce_tbm_get_label("card__label", "link", 1, 0, 'category') !!}

		{!! agrodolce_tbm_get_label("card-ristorante__story", "name") !!}

		<div class="card__title">
			<a href="{!! get_permalink() !!}">
				<h3>{!! get_the_title() !!}</h3>
			</a>
		</div>

		<div class="card__details">
			@if($data['address_complete'])
				<p>{{ $data['address_complete'] }}</p>
			@endif

			<ul>
				@if($data['budget'])
					<li>
						<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 24 24">
							<path d="M23 4H3a1 1 0 010-2h15v1h2V1a1 1 0 00-1-1H3a3 3 0 00-3 3v17a4 4 0 004 4h19a1 1 0 001-1V5a1 1 0 00-1-1zm-5 12a2 2 0 112-2 2 2 0 01-2 2z"
								  fill="#ff007b"></path>
						</svg>
						<span>{!! $data['budget'] !!}</span>
					</li>
				@endif

				@if($data['chef'])
					<li>
						<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewbox="0 0 24 24">
							<path d="M13,21H5v2a.945.945,0,0,0,1,1H18a.945.945,0,0,0,1-1V21Z" fill="#ff007b"/>
							<path d="M18,4h-.3A5.99,5.99,0,0,0,12,0,6.087,6.087,0,0,0,6.3,4H6A5.991,5.991,0,0,0,5,15.9V19h6V15h2v4h6V15.9A5.991,5.991,0,0,0,18,4Z"
								  fill="#ff007b"/>
						</svg>
						<span>{!! $data['chef'] !!}</span>
					</li>
				@endif
			</ul>
		</div>
	</div>
</article>

@extends('base-essential')
{% block wpbodyclass %}single single-autore
@endsection

@section('content')
	<!-- single-autore.twig -->
	{% set queryItem = authors.items.children[1] %}

	@asset('css/single-autore.min.css')

	<div class="single-autore">
		<div class="single-autore__info">
			<div class="wrapper">
				<div class="container">
					<div class="col-12">
						<div class="single-autore__heading">
							<div class="heading__info">
								<ul>
									<li class="info__category">
										<a href="#">Autori</a>
									</li>
								</ul>
							</div>

							{{-- autore title --}}
							<h1>{{ queryItem.name }}</h1>
						</div>
					</div>
				</div>

				<div class="container">
					<div
						class="col-4">
						{{-- autore featured image --}}
						<div class="featured-image">
							<picture>
								<!--[if IE 9]><video style="display: none;"><![endif]-->
								<source
								data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,300)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,600)) !!} 2x" media="(max-width: 736px)"/>
								<!--[if IE 9]></video><![endif]-->
								<img class="lazyload" data-srcset="{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(300,300)) !!}, {!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(600,600)) !!} 2x" alt="{!! get_the_title() !!}"/>
							</picture>
						</div>
					</div>
					<div
						class="col-8">
						{{-- Descrizione autore --}}
						<p class="content">{{ queryItem.content }}</p>
					</div>
				</div>

			</div>
		</div>
		<!-- Ultimo post scritto dall'autore
		<div class="single-autore__top-post">
			<div class="wrapper">
				<div class="container">
					{//%
							set query = {
								col1: {
									type: posts,
									component: 'partial-card-post-hero--author',
									componentNumber: '1',
								}
							}
						%}
					{//% include 'components/sections/section-1-columns.twig' %}
				</div>
			</div>
		</div> -->
		<div class="wrapper">
			<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
				<div class="col-8">
					@include('components.sections.section-1-columns',array('blocks' => array(['template' => 'partial-card-post-list','posts' => $get_posts,'post_number' => 8])))


					{{-- Pagination --}}
					@include('components.partials.partial-pagination')
				</div>
				<aside class="col-4">
					@include('components.partials.partial-sticky-adv')
				</aside>
			</div>
		</div>
	</div>

@endsection

@extends('base')
@section('content')
	<!-- archive.twig -->
	@asset('css/archive.min.css')
	<div class="archive">
		<div class="archive__heading">
			<h1>{!! get_the_archive_title() !!}</h1>
			@if(get_the_archive_description())
				<p class="abstract">{!! get_the_archive_description() !!}</p>
			@endif
		</div>
		<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
			<div class="col-8">
				<section class="section-1-columns">
					<div class="col-12">
						@if ( $tbm_videoricetta_query->have_posts() )
							@while ($tbm_videoricetta_query->have_posts()) @php $tbm_videoricetta_query->the_post() @endphp
							@includeFirst(['components.partials.partial-card-'.ad_get_post_type().'-list','components.partials.partial-card-post-list'])
							@endwhile
						@endif
					</div>
				</section>
			</div>
			<aside class="col-4">
				@include('components.partials.partial-sticky-adv')
			</aside>
		</div>

		<div class="container">
			@include('components.partials.partial-pagination')
		</div>
	</div>

@endsection

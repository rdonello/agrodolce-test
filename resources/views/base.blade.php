<!doctype html>
<html lang="it" class="no-js">
<head>
	{{-- Head --}}
	@include('components.partials.partial-head')
</head>
<!-- class="leo-skin leo-skin-mobile leo-skin-mobile-overlayered leo-viewmax-mobile" -->
<body @php body_class(); @endphp >
<!-- base.twig -->
<div class="base swiper chosen-js sticky-parent sticky-kit main-js">

	{{-- Header --}}
	@include('components.sections.header')

	<div class="wrapper">

		{{-- Static adv - Masthead --}}
		{!! tbm_get_the_banner( 'HEADOFPAGE','','',false,false ) !!}
		{!! tbm_get_the_banner( 'OOP','','',false,false ) !!}

		<div class="container">


			{{-- Block content --}}
			@yield('content')

		</div>
	</div>

	@include('components.sections.footer')
</div>
@php wp_footer() @endphp
</body>
</html>

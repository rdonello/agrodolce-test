{{-- /* Template Name: Ingredienti */ --}}
@extends('base')
@section('content')
	<!-- archive.twig -->
	@asset('css/archive.min.css')
	<div class="archive">
		<div class="archive__heading">
			<h1>{!! get_the_title() !!}</h1>
			@include('components.partials.partial-alpha-pagination')
			@if( get_the_excerpt() )
				<p class="abstract">{!! get_the_excerpt() !!}</p>
			@endif
		</div>
		<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
			<div class="col-8">
				<section class="section-1-columns">
					<div class="col-12">
						@foreach($last_terms as $term)
							{{--  partial-card-post-list.twig --}}
							<div class="partial-card-post-list">
								@asset('css/components/partials/partial-card-post-list.min.css')
								<div class="card-post-list__figure">
									<div class="card-post-list__image-wrapper">
										<picture data-link="{!! $term['url'] !!}">
											<img class="lazyload"
												 data-srcset="{!! $term['image'] !!}, {!! $term['imagex2'] !!} 2x"
												 alt=""/>
										</picture>
									</div>
								</div>
								<div class="card-post-list__content">
									<a class="card-post-list__title" href="{!! $term['url'] !!}">
										<h3>{!! $term['name'] !!}</h3>
									</a>
									<p class="card-post-list__abstract">{!! $term['description'] !!}</p>
								</div>
							</div>
						@endforeach
					</div>
				</section>
			</div>
			<aside class="col-4">
				@include('components.partials.partial-sticky-adv')
			</aside>
		</div>

		<div class="container">
			@include('components.partials.partial-alpha-pagination')
		</div>
	</div>

@endsection

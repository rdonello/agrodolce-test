@if(is_paged())
	@include('archive')
	@php exit() @endphp
@else
	@extends('base')
@section('content')
	@asset('css/front-page.min.css')

	<!-- front-page.twig -->
	<div class="front-page">

	{{-- Static adv - Mobile top --}}
	@include('components.partials.partial-static-adv')

	<!-- Hero -->
		<section class="front-page__section special">
			<div class="container">
				@include('components.sections.section-1-columns', array('blocks' => array(['template' => 'partial-card-post_type-hero','posts' => $hp_pp])))
			</div>
		</section>

		<!-- Ultime Notizie -->
		<section class="front-page__section ultime-notizie">
			<div class="container">
				<section class="section-title--block">
					<div class="partial-title-medium">
						@asset('css/components/partials/partial-title-medium.min.css')
						<div class="title-medium__content">
							<h2 class="title-medium__title">Ultimi articoli</h2>
							<span class="bottom_line"></span>
							<a class="more" href="{!! get_page_link( get_page_by_path( 'lifestyle' ) ) !!}">Leggi
								tutto</a>
						</div>
					</div>
				</section>
			</div>
			<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
				<div class="col-8">
					<div class="container container-cycle-col-2">
						@while ($hp_spa->have_posts()) @php $hp_spa->the_post() @endphp
						@include('components.partials.partial-card-'.ad_get_post_type().'-big', ['lazyload' => false])
						@endwhile
						@php wp_reset_postdata(); @endphp
					</div>
				</div>
				<div class="col-4">
					{{-- @include('components.partials.partial-promobox') --}}
					@include('components.partials.partial-sticky-adv')
				</div>
			</div>
		</section>

		<!-- Articoli in evidenza -->
		<section class="front-page__section articoli-evidenza">
			<div class="container">
				<section class="section-title--block">
					<div class="partial-title-medium">
						@asset('css/components/partials/partial-title-medium.min.css')
						<div class="title-medium__content">
							<h2 class="title-medium__title">Articoli in evidenza</h2>
							<span class="bottom_line"></span>
						</div>
					</div>
				</section>
			</div>
			<div class="container">
				@include('components.sections.section-1-columns', array('blocks' => array(['template' => 'partial-card-post_type-hero', 'posts' => $hp_ae_pp, 'lazyload' => false])))
			</div>
			<div class="container container-cycle-col-2">
				@while ($hp_ae->have_posts()) @php $hp_ae->the_post() @endphp
				@include('components.partials.partial-card-'.ad_get_post_type().'-big', ['lazyload' => false])
				@endwhile
				@php wp_reset_postdata(); @endphp
			</div>
		</section>

		<!-- Ultime Ricette -->
		<section class="front-page__section ricette">
			<div class="container">
				<section class="section-title--block">
					<div class="partial-title-medium">
						@asset('css/components/partials/partial-title-medium.min.css')
						<div class="title-medium__content">
							<h2 class="title-medium__title">Ultime ricette</h2>
							<span class="bottom_line"></span>
							<a class="more" href="{!! get_post_type_archive_link('ricetta') !!}">Leggi tutto</a>
						</div>
					</div>
				</section>
			</div>
			<div class="container">
				@include('components.sections.section-1-columns', array('blocks' => array(['template' => 'partial-card-post_type-hero','posts' => $hp_recipe_pp,'lazyload' => false])))
			</div>
			<div class="container container-cycle-col-2">
				@if(method_exists($hp_recipe_sp, 'have_posts'))
					@while($hp_recipe_sp->have_posts()) @php $hp_recipe_sp->the_post() @endphp
					@include('components.partials.partial-card-ricetta-big',['lazyload' => false])
					@endwhile
					@php wp_reset_postdata(); @endphp
				@endif
			</div>
		</section>

		<!-- Speciali -->
		<section class="front-page__section speciali">
			<div class="container">
				<section class="section-title--block">
					<div class="partial-title-medium">
						@asset('css/components/partials/partial-title-medium.min.css')
						<div class="title-medium__content">
							<h2 class="title-medium__title">Gli Speciali di Agrodolce</h2>
							<span class="bottom_line"></span>
							<a class="more" href="{!! get_page_link( get_page_by_path( 'speciali' ) ) !!}">Leggi
								tutto</a>
						</div>
					</div>
				</section>
			</div>
			<div class="container">
				@include('components.partials.partial-list-stories')
			</div>
		</section>

		<!-- Guide di Agrodolce -->
		<section class="front-page__section guide">
			<div class="container">
				<section class="section-title--block">
					<div class="partial-title-medium">
						@asset('css/components/partials/partial-title-medium.min.css')
						<div class="title-medium__content">
							<h2 class="title-medium__title">La Guida di Agrodolce</h2>
							<span class="bottom_line"></span>
							<a class="more" href="{!! get_page_link( get_page_by_path( 'guida-agrodolce' ) ) !!}">Leggi
								tutto</a>
						</div>
					</div>
				</section>
			</div>
			<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
				<div class="col-8">
					<div class="container container-cycle-col-2">
						@while ($hp_locali->have_posts()) @php $hp_locali->the_post() @endphp
						@include('components.partials.partial-card-ristorante-small', ['lazyload' => false])
						@endwhile
						@php wp_reset_postdata(); @endphp
					</div>
				</div>
				<div class="col-4">
					@include('components.partials.partial-sticky-adv')
				</div>
			</div>
		</section>

		<!-- Podcast -->
		<section class="front-page__section podcast">
			<div class="container">
				<section class="section-title--block">
					<div class="partial-title-medium">
						@asset('css/components/partials/partial-title-medium.min.css')
						<div class="title-medium__content">
							<h2 class="title-medium__title">Podcast</h2>
							<span class="bottom_line"></span>
							<a class="more" href="{!! get_post_type_archive_link('podcast') !!}">Leggi
								tutto</a>
						</div>
					</div>
				</section>
			</div>
			<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
				<div class="col-8">
					@include('components.sections.section-1-columns', array('blocks' => array(['template' => 'partial-card-podcast-list','posts' => $hp_podcasts, 'lazyload' => false])))
				</div>
				<div class="col-4">
					@include('components.partials.partial-sticky-adv')
				</div>
			</div>
		</section>
		<!-- Altri articoli -->
		<section class="front-page__section">
			<div class="container">
				<section class="section-title--block">
					<div class="partial-title-medium">
						@asset('css/components/partials/partial-title-medium.min.css')
						<div class="title-medium__content">
							<h2 class="title-medium__title">Ultime notizie</h2>
							<span class="bottom_line"></span>
							<a class="more" href="{!! get_post_type_archive_link('news') !!}">Leggi
								tutto</a>
						</div>
					</div>
				</section>
			</div>
			<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
				<div class="col-8">
					@include('components.sections.section-1-columns', array('blocks' => array(['template' => 'partial-card-post_type-list','posts' => $hp_other_articles, 'lazyload' => false])))
				</div>
				<div class="col-4">
					@include('components.partials.partial-sticky-adv')
				</div>
			</div>
		</section>
	</div>
@endsection
@endif

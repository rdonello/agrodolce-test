<pre>
@php
//echo $paged;
@endphp
</pre>
@extends('base-essential')
@section('content')
	<!-- single-card.twig -->
	@asset('css/single-card.min.css')

	<div class="single-card">
		<article class="single-card__article">
			<div class="wrapper">
				<div class="container">
					<div class="col-12">
						<div
							class="single-post__heading">
							{{-- Card category and trend --}}
							<div class="heading__info">
								<ul>
									@if($related_categories)
										@foreach($related_categories as $category)
											<li class="info__category">
												<a href="{!! $category['link'] !!}">{!! $category['name'] !!}</a>
											</li>
										@endforeach
									@endif
								</ul>
							</div>

							{{-- Card title --}}
							<h1>{!! get_the_title() !!}</h1>

							{{-- Card author and datetime --}}
							<div class="heading__detail">
								<span class="author__name">di {!! tbm_get_the_author() !!}</span>
								<span > • {!! tbm_get_pub_date() !!}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			@if($paged < 2)
			<div class="container">
				<div class="col-12">
					{{-- Static innercard cover - just first page --}}
					@include('components.partials.partial-innercard-static-cover')
				</div>
			</div>
			@endif
			@if($paged > 1)
			<div class="wrapper">
				<div class="container">
					<div class="col-12">
						{{-- Static innercard content --}}
						@include('components.partials.partial-innercard-static-content')
					</div>
				</div>
			</div>
			@endif
		</article>
	</div>

@endsection

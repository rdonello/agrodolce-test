<!doctype html>
<html lang="it" class="no-js">
<head>
	{{-- Head --}}
	@include('components.partials.partial-head')
	{{-- HEAD --}}
</head>
<body>
	<!-- base.twig -->
	<div class="base base-modal sticky-kit">
		<div class="wrapper">
			<div class="container">



				{{-- Block content --}}
				@section('content')@endsection

			</div>
		</div>
		@include('components.sections.footer')
	</div>

	@php wp_footer() @endphp
</body>
</html>

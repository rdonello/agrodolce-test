@if($paged > 1)
@include('archive')
@php exit() @endphp
@else
	{{-- HEAD --}}
	@extends('base-essential')
@section('content')
	<!-- single-ingrediente.twig -->

	@asset('css/single-ingrediente.min.css')

	<div class="single-ingrediente">
		<article class="single-ingrediente__article">

			<div class="single-ingrediente__info">
				<div class="wrapper">

					<div class="container">
						<div class="single-ingrediente__heading">

							<div class="heading__info">
								{!! $get_breadcrumb !!}
							</div>

							<h1>{{ get_the_archive_title() }}</h1>
						</div>
					</div>
					@if($term_image_id)
						<div class="container">
							<div class="single-ingrediente__abstract">
								<div
										class="col-8">
									{{-- Descrizione ingrediente --}}
									<p class="abstract">{!! get_the_archive_description() !!}</p>
								</div>
								<div
										class="col-4">
									{{-- Ingrediente featured image --}}
									<div class="featured-image">
										<picture>
											<!--[if IE 9]>
											<video style="display: none;"><![endif]-->
											<source srcset="{!! tbm_wp_get_attachment_image_url($term_image_id,array(300,300)) !!}, {!! tbm_wp_get_attachment_image_url($term_image_id,array(600,600)) !!} 2x"
													media="(max-width: 736px)"/>
											<!--[if IE 9]></video><![endif]-->
											<img srcset="{!! tbm_wp_get_attachment_image_url($term_image_id,array(300,300)) !!}, {!! tbm_wp_get_attachment_image_url($term_image_id,array(600,600)) !!} 2x"
												 src="{!! tbm_wp_get_attachment_image_url($term_image_id,array(300,300)) !!}, {!! tbm_wp_get_attachment_image_url($term_image_id,array(600,600)) !!} 2x"
												 alt="{!! tbm_get_the_post_thumbnail_alt($term_image_id) !!}"/>
										</picture>
										{{-- Social share --}}

									</div>
								</div>
							</div>
						</div>
					@else
						<div class="container">
							<div class="single-ingrediente__abstract">
								<div class="col-12">
									<p class="abstract">{!! get_the_archive_description() !!}</p>
								</div>
							</div>
						</div>
					@endif
				</div>
			</div>

			<div class="wrapper">
				<div class="container sticky-parent sticky-offset-element" data-sticky-offset-top="0">
					<div class="col-8">
						<div
								class="editorial">
							{{-- Post content --}}
							{!! $term_content !!}
						</div>
						<section class="section-1-columns">
							<div class="col-12">
								@if ( have_posts() )
									@while (have_posts()) @php the_post() @endphp
									@php global $wp_query; $ad_loop = is_object( $wp_query ) && property_exists( $wp_query, 'current_post' ) ? $wp_query->current_post : ''; @endphp
									@includeFirst(['components.partials.partial-card-'.ad_get_post_type().'-list','components.partials.partial-card-post-list'], ['ad_loop' => $ad_loop] )
									@endwhile
								@endif
							</div>
						</section>
					</div>
					<aside class="col-4">
						@include('components.partials.partial-sticky-adv')
					</aside>
				</div>
			</div>
		</article>

		<div class="container">
			@include('components.partials.partial-pagination')
		</div>
	</div>

@endsection
@endif

module.exports = function (grunt) {
	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

	// Imposta la url di riscrittura degli asset
	var url = '';

	imgArray = {
		"srcImgFeaturedBig": "490,490",
		"srcImgFeaturedBigx2": "980,980",
		"srcImgCardHero": "490,326",
		"srcImgCardHerox2": "980,654",
		"srcImgCardSemiHero": "640,250",
		"srcImgCardSemiHerox2": "1280,500",
		"srcImgCardBig": "300,200",
		"srcImgCardBigx2": "600,400",
		"srcImgCardList": "148,200",
		"srcImgCardSmall": "72,148",
		"srcImgPostFeaturedBig": "640,320",
		"srcImgPostFeaturedBigx2": "1280,640",
		"srcImgFeaturedBig1": "490,326",
		"srcImgFeaturedBig1x2": "980,652",
		"srcImgFeaturedBig2": "162,162",
		"srcImgFeaturedBig3": "162,162",
		"srcImgFeaturedBig4": "162,162",
		"srcImgCardSemiHero1": "514,250",
		"srcImgCardSemiHero1x2": "1028,500",
		"srcImgCardBig1": "300,200",
		"srcImgCardBig1x2": "600,400",
		"srcImgCardBig2": "149,149",
		"srcImgCardBig3": "149,149",
		"srcImgCardList1": "148,125",
		"srcImgCardList2": "73,73",
		"srcImgCardList3": "73,73",
		"srcImgCardSmall1": "73,73",
		"srcImgCardSmall2": "73,73",
		"srcImgSlider": "300,80",
		"srcImgTrendFeaturedBig": "300,480",
		"srcImgTrendFeaturedBigx2": "600,960",
		"srcImgHero": "148,148",
		"srcImgHerox2": "296,296",
		"srcImgBig": "96,96",
		"srcImgSmall": "48,48"
	};

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		notify_hooks: {
			options: {
				enabled: true,
				success: true,
				duration: 3
			}
		},
		replace: {
			test: {
				options: {
					patterns: [
						{
							match: /\{%[^%]+set query(?:[^%]+)type: ([^,]+)(?:[^%]+)categoryName: '([^']+)'(?:[^%]+)component: '([^']+)'[^%]+%\}(?:[^']+)@include\('components.sections.section-title--block'\)/igs,
							replacement: function (e, f, g, h) {
								return "@include('components.sections.section-title--block',array('blocks' => array(['template' => '" + f + "','categoryName' => '" + g + "','component' => '" + h + "'])))";
							}
						}
					]
				},
				files: [
					{
						expand: true,
						cwd: 'resources/views/',
						src: ['**/front-page.blade.php'],
						dest: 'resources/views/'
					}
				]
			},
			html: {
				options: {
					patterns: [
						{
							match: /\/app\/themes\/agrodolce-2021\/dist\//ig,
							replacement: url + '/app/themes/agrodolce-2021/dist/'
						}
					]
				},
				files: [
					{
						expand: true,
						flatten: true,
						src: ['templates/*.html'],
						dest: 'templates/'
					}
				]
			},
			css: {
				options: {
					patterns: [
						{
							match: /\/app\/themes\/new_brands\/dist\//ig,
							replacement: url + '/app/themes/agrodolce-2021/dist/'
						}
					]
				},
				files: [
					{
						expand: true,
						flatten: true,
						src: ['dist/css/*.css'],
						dest: 'dist/css/'
					}
				]
			},
			js: {
				options: {
					patterns: [
						{
							match: /\/app\/themes\/new_brands\/dist\//ig,
							replacement: url + '/app/themes/agrodolce-2021/dist/'
						}
					]
				},
				files: [
					{
						expand: true,
						flatten: true,
						src: ['dist/js/*.js'],
						dest: 'dist/js/'
					}
				]
			},
			twig: {
				options: {
					patterns: [
						{
							match: /\{%[^%]+set query(?:[^%]+)type: ([^,]+)(?:[^%]+)categoryName: '([^']+)'(?:[^%]+)component: '([^']+)'[^%]+%\}(?:[^']+)@include\('components.sections.section-title--block'\)/igs,
							replacement: function (e, f, g, h) {
								return "@include('components.sections.section-title--block',array('blocks' => array(['template' => '" + f + "','categoryName' => '" + g + "','component' => '" + h + "'])))";
							}
						},
						{
							match: /\{%[^%]+set query(?:[^%]+)type: ([^,]+)(?:[^%]+)component: '([^']+)'(?:[^%]+)componentNumber: '([^']+)'[^%]+type: ([^,]+)(?:[^%]+)component: '([^']+)'(?:[^%]+)componentNumber: '([^']+)'[^%]+type: ([^,]+)(?:[^%]+)component: '([^']+)'(?:[^%]+)componentNumber: '([^']+)'[^%]+%\}(?:[^']+)@include\('components.sections.section-3-columns'\)/igs,
							replacement: function (e, f, g, h, i, l, m, n, o, p) {
								return "@include('components.sections.section-3-columns',array('blocks' => array(['template' => '" + g + "','posts' => $get_" + f + ",'post_number' => " + h + "],['template' => '" + l + "','posts' => $get_" + i + ",'post_number' => " + m + "],['template' => '" + o + "','posts' => $get_" + n + ",'post_number' => " + p + "])))";
							}
						},
						{
							match: /\{%[^%]+set query(?:[^%]+)type: ([^,]+)(?:[^%]+)component: '([^']+)'(?:[^%]+)componentNumber: '([^']+)'[^%]+type: ([^,]+)(?:[^%]+)component: '([^']+)'(?:[^%]+)componentNumber: '([^']+)'[^%]+%\}(?:[^']+)@include\('components.sections.section-2-columns'\)/igs,
							replacement: function (e, f, g, h, i, l, m) {
								return "@include('components.sections.section-2-columns',array('blocks' => array(['template' => '" + g + "','posts' => $get_" + f + ",'post_number' => " + h + "],['template' => '" + l + "','posts' => $get_" + i + ",'post_number' => " + m + "])))";
							}
						},
						{
							match: /\{%[^%]+set query(?:[^%]+)type: ([^,]+)(?:[^%]+)component: '([^']+)'(?:[^%]+)componentNumber: '([^']+)'[^%]+%\}(?:[^']+)@include\('components.sections.section-1-columns'\)/igs,
							replacement: function (e, f, g, h) {
								return "@include('components.sections.section-1-columns',array('blocks' => array(['template' => '" + g + "','posts' => $get_" + f + ",'post_number' => " + h + "])))";
							}
						},
						{
							match: /\{% for relatedTrend in queryItem\.relatedTrends %\}(?:[\s]*)\{% if loop.index == 1 %\}([\s\S]*)\{% endif %\}(?:[\s]*)\{% endfor %\}/ig,
							replacement: function (e, f) {
								return f;
							}
						},
						{
							match: /\{% for relatedPost in queryItem\.relatedPosts %\}(?:[\s\S]+)\{% endfor %\}/ig,
							replacement: function (e, f) {
								return '{!! agrodolce_tbm_get_post_related() !!}';
							}
						},

						{
							match: /{% block head %}[^%]+{% endblock %}/ig,
							replacement: '{{-- HEAD --}}'
						},
						{
							match: /{# ([^#}]+) #}/ig,
							replacement: '{{-- $1 --}}'
						},
						{
							match: /{% block content %}/,
							replacement: '@section(\'content\')'
						},
						{
							match: /{% endblock %}/,
							replacement: '@endsection'
						},
						{
							match: /{% include (?:'|")([^'"]+)(?:'|") %}/ig,
							replacement: function (e, f) {
								f = f.replace('../partials/', 'components/partials');
								f = f.replace(/\//g, '.');
								f = f.replace('.twig', '');
								return "@include('" + f + "')";
							}
						},
						{
							match: /{% extends (?:'|")([^'"]+)(?:'|") %}/ig,
							replacement: function (e, f) {
								f = f.replace(/\//g, '.');
								f = f.replace('.twig', '');
								return "@extends('" + f + "')";
							}
						},
						{
							match: /<link rel="stylesheet" href="@@url\/css\/([^@]+)@css_extension.css" ?\/>/ig,
							replacement: function (e, f) {
								return '@asset(\'css/' + f + '.min.css\')';
							}
						},
						{
							match: /<link rel="stylesheet" href="@@url\/css\/components\/partials\/([^@]+)@css_extension.css" ?\/>/ig,
							replacement: function (e, f) {
								return '@asset(\'css/' + f + '.min.css\')';
							}
						},
						{
							match: /<link rel="stylesheet" href="@@url\/css\/components\/sections\/([^@]+)@css_extension.css" ?\/>/ig,
							replacement: function (e, f) {
								return '@asset(\'css/' + f + '.min.css\')';
							}
						},
						{
							match: /<a class="([^"]+)" href="\{\{ relatedTrend\.link \}\}">\{\{ relatedTrend\.name \}\}<\/a>/ig,
							replacement: function (e, f) {
								return '\{!! agrodolce_tbm_get_label("' + f + '") !!\}';
							}
						},
						{
							match: /{% for queryItem in query.col1.type.items.children %}/,
							replacement: '@foreach ($blocks as $block)'
						},

						{
							match: /{{ queryItem\.(srcImg[^\s]+) }}/ig,
							replacement: function (e, f) {
								if (f !== undefined) {
									f = f.replace("srcImg", "");
									f = f.replace("x", ",");
									return '{!! tbm_get_the_post_thumbnail_url(get_the_ID(),array(' + f + ')) !!}';
								}
							}
						},
						{
							match: /{{ ?queryItem\.link ?}}/ig,
							replacement: '{!! get_permalink() !!}'
						},
						{
							match: /{{ ?queryItem\.title ?}}/ig,
							replacement: '{!! the_title() !!}'
						},
						{
							match: /{{ ?queryItem\.abstract ?}}/ig,
							replacement: '{!! the_excerpt() !!}'
						},
						{
							match: /{{ ?queryItem.photoNumber ?}}/ig,
							replacement: '{!! tbm_get_gallery_photo_number(get_the_ID()) !!}'
						}
					]
				},
				files: [
					{
						expand: true,
						cwd: 'resources/views/',
						// src: ['**/partial-card-post-hero--fuji.blade.php'],
						src: ['**/*.blade.php'],
						dest: 'resources/views/'
					}
				]
			}
		},
		bump: {
			options: {
				files: ['package.json', 'resources/style.css'],
				updateConfigs: [],
				commit: true,
				commitMessage: 'Release v%VERSION%',
				commitFiles: ['package.json', 'resources/style.css'],
				createTag: true,
				tagName: '  %VERSION%',
				tagMessage: 'Version %VERSION%',
				push: true,
				pushTo: 'origin',
				gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
				globalReplace: false,
				prereleaseName: false,
				regExp: false
			}
		},
		copy: {
			assets: {
				files: [
					{expand: true, cwd: 'node_modules/rogiostrap.agrodolce/dist/', src: ['**'], dest: 'dist/'},
					{expand: true, cwd: 'node_modules/source.amp/dist/', src: ['**'], dest: 'dist/'}
				]
			},
			templates: {
				files: [
					{
						expand: true,
						cwd: 'node_modules/rogiostrap.agrodolce/templates/',
						src: ['**'],
						dest: 'templates/'
					}
				]
			},
			twig: {
				files: [
					{
						expand: true,
						cwd: 'templates/twig/',
						src: ['**'],
						dest: 'resources/views/',
						rename: function (dest, src) {
							return dest + src.replace('.twig', '.blade.php');
						},
						filter: function (filepath) {
							if(process.platform === 'linux') {
								path = filepath.replace('templates/twig', 'resources/views');
							} else {
								path = filepath.replace('templates\\twig', 'resources\\views');
							}
							path = path.replace('.twig', '.blade.php');
							return (!grunt.file.exists(path));
						}
					}
				]
			}
		},
		concat: {
			options: {
				separator: ';\n',
			},
			dist: {
				src: ['dist/js/customModulesLoader.js', 'dist/js/scriptloader.js'],
				dest: 'dist/js/tbm.min.js',
			}
		},
		exec: {
			update_source: 'yarn upgrade rogiostrap.agrodolce source.amp --production && yarn install',

			commit: {
				cmd: function () {
					var out = [];

					if (grunt.file.exists('node_modules/rogiostrap.agrodolce/package.json')) {
						out.push("Rogiostrap to version " + grunt.file.readJSON('node_modules/rogiostrap.agrodolce/package.json').version);
					}
					if (grunt.file.exists('node_modules/source.amp/package.json')) {
						out.push("AMP source to version " + grunt.file.readJSON('node_modules/source.amp/package.json').version);
					}

					return 'git add dist && git commit -m "Updated ' + out.join(' and ') + '" dist/';

				}
			}
		}
	});
	grunt.registerTask('default', 'Copia gli asset nella directory del tema, riscrive i path e copia i template', [
			'copy',
			'replace:html',
			'replace:js',
			'replace:twig'
		]
	);
	grunt.registerTask('production', 'Copia gli asset nella directory del tema e riscrive i path', [
		'copy:assets',
		'replace:js',
		'concat'
	]);
	grunt.registerTask('update_source', 'Update Frontend', ['exec:update_source']);
	grunt.registerTask('commit_source', 'Commit Frontend', ['exec:commit']);
};

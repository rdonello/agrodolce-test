<?php

/**
 * @param string[] $taxonomy Array of taxonomies to query. Default array( 'category' )
 *
 * @return array
 *
 * Get the terms to fill the category selection in News HP
 */
function agrodolce_acf_taxonomy_select( $taxonomy = array( 'category' ) ) {
	$out = array();

	$terms = get_terms( array(
		'taxonomy' => $taxonomy,
	) );

	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		foreach ( $terms as $term ) {
			$taxonomy                                      = get_taxonomy( $term->taxonomy )->label;
			$out[ $term->term_id . '|' . $term->taxonomy ] = $term->name . ' (' . $taxonomy . ')';
		}
	}

	return $out;

}

/**
 * Create Pages
 */
if ( function_exists( 'acf_add_options_page' ) ) {

	$args = array(
		'page_title' => 'TBM Config',
		'menu_slug'  => 'tbm_config',
		'icon_url'   => 'dashicons-list-view',
		'capability' => 'activate_plugins',
	);

	acf_add_options_page( $args );

	// add homepage sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione HP',
		'menu_title'  => 'Configurazione HP',
		'parent_slug' => 'tbm_config'
	) );

	// add news homepage sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione HP News',
		'menu_title'  => 'Configurazione HP News',
		'parent_slug' => 'tbm_config'
	) );

	// add lifestyle homepage sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione HP Lifestyle',
		'menu_title'  => 'Configurazione HP Lifestyle',
		'parent_slug' => 'tbm_config'
	) );

	// add ricette homepage sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione HP Ricette',
		'menu_title'  => 'Configurazione HP Ricette',
		'parent_slug' => 'tbm_config'
	) );

	// add città homepage sub page
	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione HP Città',
		'menu_title'  => 'Configurazione HP Città',
		'parent_slug' => 'tbm_config'
	) );

}

/**
 * Homepage fields
 */
add_action( 'admin_init', function () {
	if ( ! function_exists( 'acf_add_local_field_group' ) ) {
		return '';
	}

	// Post array
	$post_type_array = array(
		'post'                 => 'Articoli',
		'news'                 => 'News',
		'ricetta'              => 'Ricetta',
		HTML_GALLERY_POST_TYPE => HTML_GALLERY_POST_TYPE_NAME,
		HTML_VIDEO_POST_TYPE   => HTML_VIDEO_POST_TYPE_NAME
	);

	// Field to select specials
	$specials = array(
		array(
			'key'               => 'specials_00',
			'label'             => 'Speciali',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'placement'         => 'top',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'pp_specials_00',
			'label'             => 'Speciali',
			'name'              => 'pp_specials_hp',
			'instructions'      => 'Seleziona gli speciali da pubblicare in HP.',
			'required'          => 0,
			'conditional_logic' => array(),
			'type'              => 'taxonomy',
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'taxonomy'          => SITE_SPECIAL_TAXONOMY,
			'field_type'        => 'multi_select',
			'allow_null'        => 1,
			'add_term'          => 0,
			'save_terms'        => 0,
			'load_terms'        => 0,
			'return_format'     => 'id',
			'multiple'          => 0,
		)
	);

	// Final field list
	$fields_hp = array_merge(
		tbm_acf_create_hp_fields( 'pp', 'Primo piano', 1, $post_type_array ),
		tbm_acf_create_hp_fields( 'spa', 'Secondo piano', 4, $post_type_array ),
		tbm_acf_create_hp_fields( 'ae_pp', 'Articoli in evidenza primo piano', 1, $post_type_array ),
		tbm_acf_create_hp_fields( 'ae', 'Articoli in evidenza', 4, $post_type_array ),
		tbm_acf_create_hp_fields( 'recipe_pp', 'Ultime ricette primo piano', 1, [ 'ricetta' => 'ricetta' ] ),
		tbm_acf_create_hp_fields( 'recipes_sp', 'Ultime ricette secondo piano', 4, [ 'ricetta' => 'ricetta' ] ),
		tbm_acf_get_hp_fields( $specials )
	);

	/**
	 * ACF for Homepage Configuration
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5b451f5f87394',
		'title'                 => 'Homepage: selezione contenuti',
		'fields'                => $fields_hp,
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'acf-options-configurazione-hp',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );

} );

/**
 * NEWS Homepage fields
 */
add_action( 'admin_init', function () {
	if ( ! function_exists( 'acf_add_local_field_group' ) ) {
		return '';
	}

	// Get categories
	$terms = agrodolce_acf_taxonomy_select();

	// Selezione categorie in Homepage News
	$blocks_lifestyle = array(
		array(
			'key'               => 'field_5e57942ert625',
			'label'             => 'Ordine categorie HP News',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'           => 'field_5e57942afx2e4',
			'label'         => 'Ordine delle categorie in HP News',
			'name'          => 'tbm_categories_news_hp',
			'type'          => 'select',
			'instructions'  => 'Seleziona le categorie o i trend da attivare in questa zona',
			'required'      => 0,
			'wrapper'       => array(
				'width' => '100',
				'class' => '',
				'id'    => '',
			),
			'choices'       => (array) $terms,
			'default_value' => array(),
			'allow_null'    => 1,
			'multiple'      => 1,
			'ui'            => 1,
			'ajax'          => 0,
			'return_format' => 'value',
			'placeholder'   => '',
		),
	);

	// Final field list
	$fields_news_hp = array_merge(
		tbm_acf_create_hp_fields( 'news_pp', 'Primo piano', 1, array( 'news' => 'News' ) ),
		tbm_acf_create_hp_fields( 'news_sp', 'Secondo piano', 6, array( 'news' => 'News' ) ),
		tbm_acf_get_hp_fields( $blocks_lifestyle )
	);

	/**
	 * ACF for Homepage Configuration
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5b451f5f83dt',
		'title'                 => 'Homepage News: selezione contenuti.',
		'fields'                => $fields_news_hp,
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'acf-options-configurazione-hp-news',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );

} );

/**
 * Lifestyle Homepage fields
 */
add_action( 'admin_init', function () {
	if ( ! function_exists( 'acf_add_local_field_group' ) ) {
		return '';
	}

	// Get categories
	$terms = agrodolce_acf_taxonomy_select();

	//Selezione speciali in Homepage Lifestyle
	$specials = array(
		array(
			'key'               => 'specials_01',
			'label'             => 'Speciali in lista',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'placement'         => 'top',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'pp_specials_01',
			'label'             => 'Speciali',
			'name'              => 'pp_specials_lifestyle',
			'instructions'      => 'Seleziona gli speciali da pubblicare in lista',
			'required'          => 0,
			'conditional_logic' => array(),
			'type'              => 'taxonomy',
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'taxonomy'          => SITE_SPECIAL_TAXONOMY,
			'field_type'        => 'multi_select',
			'allow_null'        => 1,
			'add_term'          => 0,
			'save_terms'        => 0,
			'load_terms'        => 0,
			'return_format'     => 'id',
			'multiple'          => 0,
		)
	);

	/**
	 * Selezione categorie in Homepage News
	 */
	$blocks_lifestyle = array(
		array(
			'key'               => 'field_5e57942ert325',
			'label'             => 'Ordine categorie HP Lifestyle',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'           => 'field_5e57942afx3r7',
			'label'         => 'Ordine delle categorie nella HP Lifestyle',
			'name'          => 'tbm_categories_lifestyle_hp',
			'type'          => 'select',
			'instructions'  => 'Seleziona le categorie o i trend da attivare in questa zona',
			'required'      => 0,
			'wrapper'       => array(
				'width' => '100',
				'class' => '',
				'id'    => '',
			),
			'choices'       => (array) $terms,
			'default_value' => array(),
			'allow_null'    => 1,
			'multiple'      => 1,
			'ui'            => 1,
			'ajax'          => 0,
			'return_format' => 'value',
			'placeholder'   => '',
		),
	);

	$fields_lifestyle_hp = array_merge(
		tbm_acf_create_hp_fields( 'lifestyle_pp', 'Primo piano', 1 ),
		tbm_acf_create_hp_fields( 'lifestyle_sp', 'Secondo piano', 6 ),
		tbm_acf_get_hp_fields( $blocks_lifestyle ),
		tbm_acf_get_hp_fields( $specials )
	);

	/**
	 * ACF for Homepage Configuration
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5b451f5f83xw',
		'title'                 => 'Homepage Lifestyle: selezione contenuti',
		'fields'                => $fields_lifestyle_hp,
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'acf-options-configurazione-hp-lifestyle',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );

} );

/**
 * Ricetta Homepage fields
 */
add_action( 'admin_init', function () {
	if ( ! function_exists( 'acf_add_local_field_group' ) ) {
		return '';
	}

	// Get categories
	$terms = agrodolce_acf_taxonomy_select( 'categoria_ricetta' );

	/**
	 * Select menus
	 */
	$menus = array(
		array(
			'key'               => 'menus_01',
			'label'             => 'Menu in lista',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'placement'         => 'top',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'pp_menus_01',
			'label'             => 'Speciali',
			'name'              => 'pp_menus_ricette',
			'instructions'      => 'Seleziona i menu da pubblicare in formato lista',
			'type'              => 'post_object',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'post_type'         => array(
				0 => 'menu',
			),
			'taxonomy'          => '',
			'allow_null'        => 1,
			'multiple'          => 1,
			'return_format'     => 'object',
			'ui'                => 1,
		)
	);

	/**
	 * Selezione categorie in Homepage News
	 */
	$blocks = array(
		array(
			'key'               => 'field_5e57942ert439',
			'label'             => 'Ordine categorie ricette HP Ricette',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'           => 'field_5e57942afxhwy',
			'label'         => 'Ordine delle categorie nella HP Ricette',
			'name'          => 'tbm_categories_ricette_hp',
			'type'          => 'select',
			'instructions'  => 'Seleziona le categorie o i trend da attivare in questa zona',
			'required'      => 0,
			'wrapper'       => array(
				'width' => '100',
				'class' => '',
				'id'    => '',
			),
			'choices'       => (array) $terms,
			'default_value' => array(),
			'allow_null'    => 1,
			'multiple'      => 1,
			'ui'            => 1,
			'ajax'          => 0,
			'return_format' => 'value',
			'placeholder'   => '',
		),
	);

	$fields_ricette_hp = array_merge(
		tbm_acf_create_hp_fields( 'ricette_pp', 'Primo piano', 1, array( 'ricetta' ) ),
		tbm_acf_create_hp_fields( 'ricette_sp', 'Secondo piano', 6, array( 'ricetta' ) ),
		tbm_acf_get_hp_fields( $blocks ),
		tbm_acf_get_hp_fields( $menus )
	);

	/**
	 * ACF for Homepage Configuration
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5b451f5f81si',
		'title'                 => 'Homepage Lifestyle: selezione contenuti',
		'fields'                => $fields_ricette_hp,
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'acf-options-configurazione-hp-ricette',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );

} );

/**
 * Citta Homepage fields
 */
add_action( 'admin_init', function () {
	if ( ! function_exists( 'acf_add_local_field_group' ) ) {
		return '';
	}

	// Get categories
	$terms = agrodolce_acf_taxonomy_select( 'citta' );

	/**
	 * Selezione categorie in Homepage News
	 */
	$blocks = array(
		array(
			'key'               => 'field_5e57942ert569',
			'label'             => 'Blocchi città',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           =>
				array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
			'placement'         => 'left',
			'endpoint'          => 0,
		),
		array(
			'key'           => 'field_5e57942af3xcd',
			'label'         => 'Ordine dei blocchi città nella HP Città',
			'name'          => 'tbm_categories_citta_hp',
			'type'          => 'select',
			'instructions'  => 'Seleziona le categorie o i trend da attivare in questa zona',
			'required'      => 0,
			'wrapper'       => array(
				'width' => '100',
				'class' => '',
				'id'    => '',
			),
			'choices'       => (array) $terms,
			'default_value' => array(),
			'allow_null'    => 1,
			'multiple'      => 1,
			'ui'            => 1,
			'ajax'          => 0,
			'return_format' => 'value',
			'placeholder'   => '',
		),
	);

	//Città
	$cities = array(
		array(
			'key'               => 'cities_01',
			'label'             => 'Città in lista',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'placement'         => 'top',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'pp_cities_01',
			'label'             => 'Città',
			'name'              => 'pp_cities_ricette',
			'instructions'      => 'Seleziona le città da pubblicare in formato lista',
			'required'          => 0,
			'conditional_logic' => array(),
			'type'              => 'taxonomy',
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'taxonomy'          => 'citta',
			'field_type'        => 'multi_select',
			'allow_null'        => 1,
			'add_term'          => 0,
			'save_terms'        => 0,
			'load_terms'        => 0,
			'return_format'     => 'id',
			'multiple'          => 0,
		)
	);

	$fields_citta_hp = array_merge(
		tbm_acf_create_hp_fields( 'citta_pp', 'Primo piano', 1, array( 'locale' ) ),
		tbm_acf_create_hp_fields( 'citta_sp', 'Secondo piano', 6, array( 'locale' ) ),
		tbm_acf_get_hp_fields( $blocks ),
		tbm_acf_get_hp_fields( $cities )
	);

	/**
	 * ACF for Homepage Configuration
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5b451f5f54lk',
		'title'                 => 'Homepage Città: selezione contenuti',
		'fields'                => $fields_citta_hp,
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'acf-options-configurazione-hp-citta',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );

} );

<?php
/**
 *  Documentazione su https://posttypes.jjgrainger.co.uk/
 */

use PostTypes\PostType;
use PostTypes\Taxonomy;

/**
 * DEFINITION
 */
if ( ! defined( 'HTML_GALLERY_POST_TYPE' ) ) {
	define( 'HTML_GALLERY_POST_TYPE', 'gallery' );
}
if ( ! defined( 'HTML_GALLERY_POST_TYPE_NAME' ) ) {
	define( 'HTML_GALLERY_POST_TYPE_NAME', 'gallery' );
}
if ( ! defined( 'HTML_VIDEO_POST_TYPE' ) ) {
	define( 'HTML_VIDEO_POST_TYPE', 'video' );
}
if ( ! defined( 'HTML_VIDEO_POST_TYPE_NAME' ) ) {
	define( 'HTML_VIDEO_POST_TYPE_NAME', 'video' );
}
if ( ! defined( 'HTML_CARD_POST_TYPE' ) ) {
	define( 'HTML_CARD_POST_TYPE', 'card' );
}
if ( ! defined( 'HTML_EVENT_POST_TYPE' ) ) {
	define( 'HTML_EVENT_POST_TYPE', 'event' );
}
if ( ! defined( 'HTML_PODCAST_POST_TYPE' ) ) {
	define( 'HTML_PODCAST_POST_TYPE', 'podcast' );
}
if ( ! defined( 'HTML_PRODUCT_POST_TYPE' ) ) {
	define( 'HTML_PRODUCT_POST_TYPE', 'product' );
}

/* NEWS */
$tbm_labels  = [
	'add_new' => 'Aggiungi'
];
$tbm_options = [
	'has_archive'  => true,
	'supports'     => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' ),
	'show_in_rest' => false,
	'taxonomies'   => array( 'category' ),
	'public'       => true,
	'menu_icon'    => 'dashicons-format-aside',
	'menu_position' => 5
];
$tbm_names   = [
	'name'     => 'news',
	'singular' => 'News',
	'plural'   => 'News',
	'slug'     => 'news'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->register();

/* RICETTA */
$tbm_labels  = [
	'add_new' => 'Aggiungi'
];
$tbm_options = [
	'has_archive'  => true,
	'supports'     => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' ),
	'show_in_rest' => false,
	'public'       => true,
	'taxonomies'   => [ 'post_tag' ],
	'menu_icon'    => 'dashicons-editor-ol',
	'menu_position' => 5
];
$tbm_names   = [
	'name'     => 'ricetta',
	'singular' => 'Ricetta',
	'plural'   => 'Ricette',
	'slug'     => 'ricette'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->columns()->hide( array(
	'tags',
	'taxonomy-sponsor',
	'taxonomy-citta',
	'taxonomy-filters',
	'taxonomy-rubrica',
	'taxonomy-tagalert',
	'comments'
) );
$tbm->register();

/* MENU */
$tbm_menu_labels  = [
	'add_new' => 'Aggiungi'
];
$tbm_menu_options = [
	'has_archive'  => true,
	'supports'     => array( 'title', 'author', 'editor' ),
	'show_in_rest' => false,
	'public'       => true,
	'rewrite'      => array( 'with_front' => false ),
	'menu_icon'    => 'dashicons-welcome-widgets-menus',
	'menu_position' => 5
];
$tbm_menu_names   = [
	'name'     => 'menu',
	'singular' => 'Menu',
	'plural'   => 'Menu',
	'slug'     => 'menu'
];
$tbm_menu         = new PostType( $tbm_menu_names, $tbm_menu_options, $tbm_menu_labels );
$tbm_menu->register();

/* COCKTAIL */
$tbm_labels  = [
	'add_new' => 'Aggiungi'
];
$tbm_options = [
	'has_archive'  => true,
	'supports'     => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' ),
	'show_in_rest' => false,
	'public'       => true,
	'taxonomies'   => [ 'post_tag' ],
	'menu_icon'    => 'dashicons-welcome-view-site',
	'menu_position' => 5
];
$tbm_names   = [
	'name'     => 'cocktail',
	'singular' => 'Cocktail',
	'plural'   => 'Cocktail',
	'slug'     => 'cocktail'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->register();

/* LOCALE */
$tbm_labels  = [
	'add_new' => 'Aggiungi'
];
$tbm_options = [
	'has_archive'  => true,
	'supports'     => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' ),
	'show_in_rest' => false,
	'public'       => true,
	'menu_icon'    => 'dashicons-store',
	'menu_position' => 5
];
$tbm_names   = [
	'name'     => 'locale',
	'singular' => 'Locale',
	'plural'   => 'Locali',
	'slug'     => 'locale'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->register();

/* PODCAST */
$tbm_labels  = [
	'add_new' => 'Aggiungi'
];
$tbm_options = [
	'has_archive'  => true,
	'supports'     => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' ),
	'show_in_rest' => false,
	'public'       => true,
	'menu_icon'    => 'dashicons-format-audio',
	'menu_position' => 5
];
$tbm_names   = [
	'name'     => 'podcast',
	'singular' => 'Podcast',
	'plural'   => 'Podcasts',
	'slug'     => 'podcast'
];
$tbm         = new PostType( $tbm_names, $tbm_options, $tbm_labels );
$tbm->register();

/**
 * REGISTRAZIONE TASSONOMIE
 */

/**
 * Original
 */
$special_cpts = [
	'post',
	'news',
	HTML_GALLERY_POST_TYPE, //gallery
	HTML_VIDEO_POST_TYPE, //video
	HTML_PODCAST_POST_TYPE, //podcast
];

/* * Taxonomy citta */
$tax0_labels  = [ 'add_new_item' => 'Aggiungi Città' ];
$tax0_names   = [ 'name' => 'citta', 'singular' => 'Città', 'plural' => 'Città', 'slug' => 'citta' ];
$tax0_options = [ 'rewrite' => array( 'slug' => 'citta', 'with_front' => false ), 'show_in_rest' => false ];
$tax0         = new Taxonomy( $tax0_names, $tax0_options, $tax0_labels );
$tax0->posttype( 'locale' );
$tax0->posttype( 'post' );
$tax0->register();

/* * Taxonomy ingredienti */
$tax1_labels  = [ 'add_new_item' => 'Aggiungi Ingredienti' ];
$tax1_names   = [
	'name'     => 'ingredienti',
	'singular' => 'Ingredienti',
	'plural'   => 'Ingredienti',
	'slug'     => 'ingredienti'
];
$tax1_options = [ 'rewrite' => array( 'slug' => 'ingredienti', 'with_front' => false ), 'show_in_rest' => false ];
$tax1         = new Taxonomy( $tax1_names, $tax1_options, $tax1_labels );
$tax1->posttype( 'ricetta' );
$tax1->posttype( 'cocktail' );
$tax1->register();

/* * Taxonomy speciale */
$tax2_labels  = [ 'add_new_item' => 'Aggiungi Speciale' ];
$tax2_names   = [ 'name' => 'speciale', 'singular' => 'Speciale', 'plural' => 'Speciale', 'slug' => 'speciale' ];
$tax2_options = [
	'rewrite'      => array( 'slug' => 'speciali', 'with_front' => false ),
	'show_in_rest' => false,
	'hierarchical' => false
];
$tax2         = new Taxonomy( $tax2_names, $tax2_options, $tax2_labels );
foreach ( $special_cpts as $cpt ) {
	$tax2->posttype( $cpt );
}
$tax2->posttype( [ 'ricetta', 'locale', 'cocktail' ] );
$tax2->register();

/* * Taxonomy categoria_ricetta */
$tax3_labels  = [ 'add_new_item' => 'Aggiungi Categoria ricetta' ];
$tax3_names   = [
	'name'     => 'categoria_ricetta',
	'singular' => 'Categoria ricetta',
	'plural'   => 'Categoria ricetta',
	'slug'     => 'categoria_ricetta'
];
$tax3_options = [ 'rewrite' => array( 'slug' => 'categoria_ricetta', 'with_front' => false ), 'show_in_rest' => false ];
$tax3         = new Taxonomy( $tax3_names, $tax3_options, $tax3_labels );
$tax3->posttype( 'ricetta' );
$tax3->register();

/* * Taxonomy filters */
$tax5_labels  = [ 'add_new_item' => 'Aggiungi Filtri' ];
$tax5_names   = [ 'name' => 'filters', 'singular' => 'Filtri', 'plural' => 'Filtri', 'slug' => 'filters' ];
$tax5_options = [ 'rewrite' => array( 'slug' => 'filters', 'with_front' => false ), 'show_in_rest' => false ];
$tax5         = new Taxonomy( $tax5_names, $tax5_options, $tax5_labels );
foreach ( $special_cpts as $cpt ) {
	$tax5->posttype( $cpt );
}
$tax5->posttype( 'ricetta' );
$tax5->posttype( 'locale' );
$tax5->posttype( 'cocktail' );
$tax5->register();

/* * Taxonomy rubrica */
$tax6_labels  = [ 'add_new_item' => 'Aggiungi Rubrica' ];
$tax6_names   = [ 'name' => 'rubrica', 'singular' => 'Rubrica', 'plural' => 'Rubriche', 'slug' => 'rubrica' ];
$tax6_options = [ 'rewrite' => array( 'slug' => 'rubriche', 'with_front' => false ), 'show_in_rest' => false ];
$tax6         = new Taxonomy( $tax6_names, $tax6_options, $tax6_labels );
foreach ( $special_cpts as $cpt ) {
	$tax6->posttype( $cpt );
}
$tax6->posttype( [ 'ricetta', 'locale', 'cocktail' ] );
$tax6->register();

/* * Taxonomy categoria_cocktail */
$tax7_labels  = [ 'add_new_item' => 'Aggiungi Categoria cocktail' ];
$tax7_names   = [
	'name'     => 'categoria_cocktail',
	'singular' => 'Categoria cocktail',
	'plural'   => 'Categoria cocktail',
	'slug'     => 'categoria_cocktail'
];
$tax7_options = [
	'rewrite'      => array( 'slug' => 'categoria_cocktail', 'with_front' => false ),
	'show_in_rest' => false
];
$tax7         = new Taxonomy( $tax7_names, $tax7_options, $tax7_labels );
$tax7->posttype( 'cocktail' );
$tax7->posttype( HTML_GALLERY_POST_TYPE );
$tax7->register();

/* * Taxonomy tagalert */
$tax8_labels  = [ 'add_new_item' => 'Aggiungi Tagalert' ];
$tax8_names   = [
	'name'     => 'tagalert',
	'singular' => 'Tagalert',
	'plural'   => 'Tagalert',
	'slug'     => 'tagalert'
];
$tax8_options = [
	'rewrite'      => array( 'slug' => 'tagalert', 'with_front' => false ),
	'show_in_rest' => false
];
$tax8         = new Taxonomy( $tax8_names, $tax8_options, $tax8_labels );
$tax8->posttype( array(
	"ricetta",
	"news",
	"cocktail",
	"post",
	"locale"
) );
$tax8->register();

/* * Taxonomy categoria_imenu */
$tax9_labels  = [ 'add_new_item' => 'Aggiungi Categoria Menu' ];
$tax9_names   = [
	'name'     => 'categoria_menu',
	'singular' => 'Categoria menu',
	'plural'   => 'Categoria menu',
	'slug'     => 'categoria_menu'
];
$tax9_options = [ 'rewrite' => array( 'slug' => 'categoria_menu', 'with_front' => false ), 'show_in_rest' => false ];
$tax9         = new Taxonomy( $tax9_names, $tax9_options, $tax9_labels );
$tax9->posttype( 'menu' );
$tax9->register();


/**
 * Disable extra columns in admin post list sccreen
 */
$ad = new PostType( 'post' );

$ad->columns()->hide( array(
	'tags',
	'taxonomy-sponsor',
	'taxonomy-citta',
	'taxonomy-categoria_ricetta',
	'taxonomy-filters',
	'taxonomy-rubrica',
	'taxonomy-tagalert',
	'comments'
) );
$ad->register();

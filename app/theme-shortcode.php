<?php
/**
 * Slider
 */
add_shortcode( 'slider', function ( $attr ) {
	$post = get_post();

	static $instance = 0;
	$instance ++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) ) {
			$attr['orderby'] = 'post__in';
		}
		$attr['include'] = $attr['ids'];
	}
	//Transform ids strin into array of IDS
	$arr_ids = explode( ',', $attr ['ids'] );
	foreach ( $arr_ids as $id ) {
		$gallery[]['ID'] = $id;
	}

	if ( is_amp_endpoint() ) {
		$size        = 'large';
		$gallery_div = "<amp-carousel width='400' height='300' layout='responsive' type='slides'>";
		$output      = $gallery_div;
		foreach ( $gallery as $k => $id ) {
			$thumb_src = wp_get_attachment_image_src( $id['ID'], $size );
			$output    .= "<amp-img src='{$thumb_src[0]}' width='{$thumb_src[1]}' height='{$thumb_src[2]}'></amp-img>";
		}

		$output .= "</amp-carousel>\n";
	} else {
		$sc          = new stdClass();
		$sc->gallery = isset( $gallery ) ? $gallery : array();
		$data['sc']  = $sc;

		ob_start();
		echo App\template( 'embedder/templates/_gallery_slider', $data );
		$output = ob_get_contents();
		ob_end_clean();
		wp_reset_postdata();
	}

	return $output;
//TODO Check code below
	/**
	 * Filter the default gallery shortcode output.
	 *
	 * If the filtered output isn't empty, it will be used instead of generating
	 * the default gallery template.
	 *
	 * @param string $output The gallery output. Default empty.
	 * @param array $attr Attributes of the gallery shortcode.
	 *
	 * @since 2.5.0
	 *
	 * @see slider_shortcode()
	 *
	 *
	 * $output = apply_filters( 'post_gallery', '', $attr );
	 * if ( $output != '' ) {
	 * return $output;
	 * }
	 * // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	 * if ( isset( $attr['orderby'] ) ) {
	 * $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
	 * if ( ! $attr['orderby'] ) {
	 * unset( $attr['orderby'] );
	 * }
	 * }
	 *
	 * $html5 = current_theme_supports( 'html5', 'gallery' );
	 * extract( shortcode_atts( array(
	 * 'order'      => 'ASC',
	 * 'orderby'    => 'menu_order ID',
	 * 'id'         => $post ? $post->ID : 0,
	 * 'itemtag'    => $html5 ? 'figure' : 'dl',
	 * 'icontag'    => $html5 ? 'div' : 'dt',
	 * 'captiontag' => $html5 ? 'figcaption' : 'dd',
	 * 'columns'    => 3,
	 * 'size'       => 'large',
	 * 'include'    => '',
	 * 'exclude'    => '',
	 * 'link'       => 'file'
	 * ), $attr, 'gallery' ) );
	 *
	 * $id = intval( $id );
	 * if ( 'RAND' == $order ) {
	 * $orderby = 'none';
	 * }
	 *
	 * if ( ! empty( $include ) ) {
	 * $_attachments = get_posts( array(
	 * 'include'        => $include,
	 * 'post_status'    => 'inherit',
	 * 'post_type'      => 'attachment',
	 * 'post_mime_type' => 'image',
	 * 'order'          => $order,
	 * 'orderby'        => $orderby
	 * ) );
	 *
	 * $attachments = array();
	 * foreach ( $_attachments as $key => $val ) {
	 * $attachments[ $val->ID ] = $_attachments[ $key ];
	 * }
	 * } elseif ( ! empty( $exclude ) ) {
	 * $attachments = get_children( array(
	 * 'post_parent'    => $id,
	 * 'exclude'        => $exclude,
	 * 'post_status'    => 'inherit',
	 * 'post_type'      => 'attachment',
	 * 'post_mime_type' => 'image',
	 * 'order'          => $order,
	 * 'orderby'        => $orderby
	 * ) );
	 * } else {
	 * $attachments = get_children( array(
	 * 'post_parent'    => $id,
	 * 'post_status'    => 'inherit',
	 * 'post_type'      => 'attachment',
	 * 'post_mime_type' => 'image',
	 * 'order'          => $order,
	 * 'orderby'        => $orderby
	 * ) );
	 * }
	 *
	 * if ( empty( $attachments ) ) {
	 * return '';
	 * }
	 *
	 * if ( is_feed() ) {
	 * $output = "\n";
	 * foreach ( $attachments as $att_id => $attachment ) {
	 * $output .= wp_get_attachment_link( $att_id, $size, true ) . "\n";
	 * }
	 *
	 * return $output;
	 * }
	 *
	 * $itemtag    = tag_escape( $itemtag );
	 * $captiontag = tag_escape( $captiontag );
	 * $icontag    = tag_escape( $icontag );
	 * $valid_tags = wp_kses_allowed_html( 'post' );
	 * if ( ! isset( $valid_tags[ $itemtag ] ) ) {
	 * $itemtag = 'dl';
	 * }
	 * if ( ! isset( $valid_tags[ $captiontag ] ) ) {
	 * $captiontag = 'dd';
	 * }
	 * if ( ! isset( $valid_tags[ $icontag ] ) ) {
	 * $icontag = 'dt';
	 * }
	 *
	 * $columns   = intval( $columns );
	 * $itemwidth = $columns > 0 ? floor( 100 / $columns ) : 100;
	 * $float     = is_rtl() ? 'right' : 'left';
	 *
	 * $selector = "gallery-{$instance}";
	 *
	 * $gallery_style = $gallery_div = '';
	 *
	 * $size_class = sanitize_html_class( $size );
	 *
	 * if ( is_amp_endpoint() ) {
	 * $gallery_div = "<amp-carousel width='400' height='300' layout='responsive' type='slides'>";
	 * $output      = $gallery_div;
	 * foreach ( $attachments as $id => $attachment ) {
	 * $thumb_src = wp_get_attachment_image_src( $id, $size );
	 * $output    .= "<amp-img src='{$thumb_src[0]}' width='{$thumb_src[1]}' height='{$thumb_src[2]}'></amp-img>";
	 * }
	 *
	 * $output .= "</amp-carousel>\n";
	 * } else {
	 * $gallery_div = "<div id='$selector' class='owl-slider-post owl-carousel owl-theme'>";
	 * $output      = apply_filters( 'gallery_style', $gallery_style . $gallery_div );
	 *
	 * $i = 0;
	 * foreach ( $attachments as $id => $attachment ) {
	 *
	 * $thumb_src = wp_get_attachment_image_src( $id, $size );
	 * $image_output = wp_get_attachment_image( $id, $size, false, array(
	 * 'class'    => 'lazyOwl',
	 * 'data-src' => $thumb_src[0],
	 * 'src'      => 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=='
	 * ) );
	 *
	 * $image_meta = wp_get_attachment_metadata( $id );
	 *
	 * $orientation = '';
	 * if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
	 * $orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
	 * }
	 *
	 * $output .= "<{$itemtag} class='item'>";
	 * $output .= "
	 * <{$icontag} class='gallery-icon {$orientation}'>
	 * $image_output
	 * </{$icontag}>";
	 * if ( $captiontag && trim( $attachment->post_excerpt ) ) {
	 * $output .= "
	 * <{$captiontag} class='wp-caption-text gallery-caption'>
	 * " . wptexturize( $attachment->post_excerpt ) . "
	 * </{$captiontag}>";
	 * }
	 * $output .= "</{$itemtag}>";
	 * }
	 *
	 * $output .= "</div>\n";
	 * }
	 *
	 * return $output;
	 */
} );

/**
 * Phrase
 */
add_shortcode( 'PHRASE', function ( $atts ) {
	extract( shortcode_atts( array(
		'value' => 'something'
	), $atts ) );

	return "<span class=\"box-evidence\">" . $value . "</span>";

} );

/**
 * Update
 */
add_shortcode( 'UPDATE', function ( $atts, $content = '' ) {
	extract( shortcode_atts( array(
		'value' => 'something'
	), $atts ) );

	$out = $content ?: $value;

	return "<p class='box-update'>" . $out . "</p>";

} );

/**
 * Special list
 */
add_shortcode( 'special-list', function ( $atts ) {
	$content_list = get_field( 'content-list' );

	return $content_list;
} );

/**
 * Galleria
 */
add_shortcode( "galleria", "__return_false" );

//TODO This short code has to be redesigned. Comments have been left to keep in mund what param it handled
function panel_func( $atts ) {
	$voto            = ! empty( $atts['voto'] ) ? $atts['voto'] : 0;
	$votopercentuale = $voto * 10;
	$giudizio        = ! empty( $atts['giudizio'] ) ? $atts['giudizio'] : '';
	$degustatore     = ! empty( $atts['degustatore'] ) ? $atts['degustatore'] : '';
	$commento        = ! empty( $atts['commento'] ) ? $atts['commento'] : '';
	$panelhtml       = '<div class="panel-box ' . $atts['icon'] . '">';
	//$panelhtml       .= '<div class="img-details">';
	//$panelhtml       .= '<img class="no-social" src="' . get_bloginfo( "stylesheet_directory" ) . '/images/panel/' . $atts['icon'] . '.png">';
	//$panelhtml       .= '</div>';
	$panelhtml .= '<div class="txt-details">';
	//$panelhtml       .= '<div class="box-vote">';
	//$panelhtml       .= '<div class="img-vote"> <div class="img-voted" style="width:' . $votopercentuale . '%;"></div> </div><!--img-vote-->';
	//$panelhtml       .= '<span>' . $voto . '</span>';
	//$panelhtml       .= '</div><!--box-vote-->';
	$panelhtml .= '<div class="description">';
	$panelhtml .= '<p class="autor-comment">' . $giudizio . '</p>';
	$panelhtml .= '<p class="autor">' . $degustatore . ' (Voto ' . $voto . ')</p>';
	$panelhtml .= '<p>' . $commento . '</p>';
	$panelhtml .= '</div><!--description-->';
	$panelhtml .= '</div><!--txt-details-->';
	$panelhtml .= '</div><!--panel-box-->';

	return $panelhtml;
}

add_shortcode( 'panel', 'panel_func' );

/**
 * Contact form
 */
add_shortcode( "contact-form-7", "__return_false" );

/**
 * Shortcode videohtml (used, ie, in ricette)
 */
add_shortcode( 'videohtml', function ( $atts ) {

	//Esample: [videohtml codevideo="Vpuaimhralc=" autoplay="yes"]

	if ( empty( $atts['codevideo'] ) ) {
		return '';
	}

	$id = (int) video_id_decrypt( $atts['codevideo'] );

	$args = array(
		'post_type'      => 'video',
		'posts_per_page' => 1,
		'fields'         => 'ids',
		'meta_key'       => 'old_id',
		'meta_value'     => $id,
		'meta_compare'   => '='
	);

	$posts = get_posts( $args );

	if ( ! $posts ) {
		return '';
	}

	$video_id = reset( $posts );

	if ( ! function_exists( 'html_video_generate_brid_video_player' ) ) {
		return false;
	}


	/**
	 * Generate output string embedding template
	 */
	ob_start();
	$sc         = new stdClass();
	$sc->id     = $video_id;
	$data['sc'] = $sc;
	echo App\template( 'embedder/templates/_mp4_inside', $data );
	$output_string = ob_get_contents();
	ob_end_clean();

	return $output_string;

} );

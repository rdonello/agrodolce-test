<?php
/**
 * User: Lino
 * Date: 02/02/2021
 * Time: 15:27
 */

namespace App\Controllers;

/**
 * Custom main menu
 */
class Agrodolce_Above_The_Fold_Menu extends \Walker_Nav_Menu {
	/**
	 * Start the element output.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. May be used for padding.
	 * @param array $args Additional strings.
	 *
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$class   = array();
		$class[] = 'swiper-slide';

		// Attributes of the a href tag
		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
		$attributes .= ' class="' . implode( ' ', $class ) . ' "';
		$output     .= '<a ' . $attributes . '>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</a>' . PHP_EOL;
	}

}

/**
 * Lifestyle menu
 */
class Agrodolce_Menu_Secondary extends \Walker_Nav_Menu {

	// Displays start of a level. E.g '<ul>'
	// @see Walker::start_lvl()
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "<ul class=\"main-menu__submenu\">";
	}

	// Displays end of a level. E.g '</ul>'
	// @see Walker::end_lvl()
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "</ul>";
	}

	// Displays start of an element. E.g '<li> Item Name'
	// @see Walker::start_el()
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$current     = '';
		$current_cat = '';

		if ( is_tax() || is_search() ) {
			$current = 'no-current';
		}

		if ( is_front_page() && $item->title == 'Home' ) {
			$current = 'active';
		}

		if ( $item->type === 'post_type_archive' ) {
			if ( is_singular( $item->object ) || is_post_type_archive( $item->object ) ) {
				$current = 'active';
			}
		}

		if ( $item->object === 'category' && ! ( is_front_page() || is_post_type_archive() ) ) {

			// If is post maybe we have more than 1 category
			if ( is_single() ) {
				$categories = get_the_category();

				if ( ! empty( $categories ) ) {
					$current_cat    = $categories[0]->term_id;
					$current_parent = $categories[0]->parent;
				}

				if ( $current_cat && $item->object_id == $current_cat ) {
					$current = 'active';
				}
				/* Disable set current on parent category
								if ( $current_cat && $item->object_id == $current_parent ) {
									$current = 'active';
								}
				*/
			}

			// If is category we have just one category
			if ( is_archive() ) {

				$categories = get_queried_object();

				if ( ! empty( $categories ) ) {
					$current_cat    = $categories->term_id;
					$current_parent = $categories->parent;
				}

				if ( $current_cat && $item->object_id == $current_cat ) {
					$current = 'active';
				}
				/* Disabled set current on parent category
								if ( $current_cat && $item->object_id == $current_parent ) {
									$current = 'active';
								}
				*/

			}

		}

		// Depth-dependent classes.
		$depth_classes     = array(
			( $depth == 0 ? 'main-menu__item data_parent' : 'sub-menu-item' ),
			( $depth >= 2 ? 'sub-sub-menu-item' : '' ),
			( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
			'menu-item-depth-' . $depth
		);
		$depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

		// Passed classes.
		$classes     = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

		// Link attributes.
		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
		//$attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
		$attributes .= ' class="' . $current . '"';

		$pre  = "<div class=\"trend-slide\">";
		$post = "";

		// Build HTML output and pass through the proper filter.
		$item_output = sprintf( '%1$s%2$s<a%3$s>%4$s%5$s%6$s</a>%7$s%8$s',
			$args->before,
			$pre,
			$attributes,
			$args->link_before,
			apply_filters( 'the_title', $item->title, $item->ID ),
			$args->link_after,
			$post,
			$args->after
		);
		$output      .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

	}

	// Displays end of an element. E.g '</li>'
	// @see Walker::end_el()
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		if ( $depth == 0 ) {
			$output .= "</div>";
		} else {
			$output .= "</div>\n";
		}

	}
}

<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 17/11/2018
 * Time: 17:59
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleCard extends Controller {

	use Partials\RelatedTaxonomy;
	use Partials\CommonSingle;

	public $page;
	public $paragraphs;

	public function paragraphs() {
		$paragraphs       = get_field( "html_card_paragrafo_repeater" );
		$this->paragraphs = $paragraphs;
	}

	public function paragraph_array() {
		return $this->paragraphs;
	}

	public function paged() {
		$page       = ( get_query_var( 'doc' ) ) ?: 1;
		$this->page = $page;

		return $page;
	}

	public function card() {
		if($this->page > count($this->paragraphs) + 1){
			$this->page = count($this->paragraphs) + 1;
		}
		return $this->page - 2;
	}

	public function total_cards() {
		return count( $this->paragraphs ) + 1;
	}

	public function get_nav() {

		global $post;

		if ( is_preview() ) {
			$next = get_preview_post_link( $post, array( 'doc' => $this->page + 1 ) );
			$prev = get_preview_post_link( $post, array( 'doc' => $this->page - 1 ) );
		} else {
			$next = get_permalink( $post ) . 'doc/' . ( $this->page + 1 );
			$prev = get_permalink( $post ) . 'doc/' . ( $this->page - 1 );
		}

		return array( 'prev' => $prev, 'next' => $next );
	}

	public function modal_close_link() {

		return get_post_type_archive_link( HTML_CARD_POST_TYPE );
	}

}

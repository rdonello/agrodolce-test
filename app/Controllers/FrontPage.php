<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller {

	use Partials\Pagination;

	/**
	 * Save here the posts to not__in in categories loop
	 *
	 * @var array
	 */
	private $notin = array();

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( array(
			'/dist/css/critical/homepage--critical.min.css',
			'/dist/css/custom-critical.min.css'
		), '/dist/css/custom.min.css' );

		return $css;
	}

	/**
	 * Primo Piano
	 *
	 * @return \WP_Query
	 */
	public function hp_pp() {
		if ( is_paged() ) {
			return '';
		}

		return tbm_print_hp_fields( 'pp', 1 );
	}

	/**
	 *  Secondo Piano Alto
	 *
	 * @return \WP_Query
	 */
	public function hp_spa() {
		if ( is_paged() ) {
			return '';
		}

		return tbm_print_hp_fields( 'spa', 4 );
	}

	/**
	 *  Articoli in evidenza primo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_ae_pp() {
		if ( is_paged() ) {
			return '';
		}

		return tbm_print_hp_fields( 'ae_pp', 1 );
	}

	/**
	 * Le guide (locali) di Agrodolce
	 * @return \WP_Query
	 */
	public function hp_locali() {
		if ( is_paged() ) {
			return '';
		}
		$not_in = get_query_var( 'notin', array() );

		// Defaults args
		$default_args = [
			'posts_per_page' => 4,
			'post_type'      => 'locale',
			'post__not_in'   => $not_in,
		];

		// Execute the query
		$the_query = new \WP_Query( $default_args );

		if ( $the_query->have_posts() ) {
			// Save the returned ids in variable
			$not_in = @array_merge( $not_in, wp_list_pluck( $the_query->posts, 'ID' ) );
			set_query_var( 'notin', $not_in );
		}

		return $the_query;

	}

	/**
	 *  Articoli in evidenza
	 *
	 * @return \WP_Query
	 */
	public function hp_ae() {
		if ( is_paged() ) {
			return '';
		}

		return tbm_print_hp_fields( 'ae', 4 );
	}

	/**
	 *  Blocco ricette primo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_recipe_pp() {
		if ( is_paged() ) {
			return '';
		}

		return tbm_print_hp_fields( 'recipe_pp', 1, array( 'ricetta' ) );
	}

	/**
	 *  Blocco ricette secondo piano
	 *
	 * @return \WP_Query
	 */
	public function hp_recipe_sp() {
		if ( is_paged() ) {
			return '';
		}

		return tbm_print_hp_fields( 'recipes_sp', 4, array( 'ricetta' ) );
	}

	/**
	 *  Blocco Podcast
	 *
	 * @return \WP_Query
	 */
	public function hp_podcasts() {
		if ( is_paged() ) {
			return '';
		}
		$not_in = get_query_var( 'notin', array() );

		// Defaults args
		$default_args = [
			'posts_per_page' => 3,
			'post_type'      => 'podcast',
			'post__not_in'   => $not_in,
		];

		// Execute the query
		$the_query = new \WP_Query( $default_args );

		if ( $the_query->have_posts() ) {
			// Save the returned ids in variable
			$not_in = @array_merge( $not_in, wp_list_pluck( $the_query->posts, 'ID' ) );
			set_query_var( 'notin', $not_in );
		}

		return $the_query;

	}

	/**
	 *  Blocco Altri articoli
	 *
	 * @return \WP_Query
	 */
	public function hp_other_articles() {
		if ( is_paged() ) {
			return '';
		}
		$not_in = get_query_var( 'notin', array() );

		// Defaults args
		$default_args = [
			'posts_per_page' => 4,
			'post_type'      => [ 'news' ],
			'post__not_in'   => $not_in,
		];

		// Execute the query
		$the_query = new \WP_Query( $default_args );

		if ( $the_query->have_posts() ) {
			// Save the returned ids in variable
			$not_in = @array_merge( $not_in, wp_list_pluck( $the_query->posts, 'ID' ) );
			set_query_var( 'notin', $not_in );
		}

		return $the_query;

	}

	/**
	 * Get latest latest stories
	 *
	 * @return array
	 */
	public function featured_specials() {

		$selected_specials = get_field( 'pp_specials_hp', 'option' );

		if ( ! $selected_specials ) {
			return array();
		}

		$my_args       = array(
			'taxonomy' => SITE_SPECIAL_TAXONOMY,
			'include'  => $selected_specials,
			'orderby'  => 'include'
		);
		$term_query_01 = new \WP_Term_Query( $my_args );

		$select_terms = empty( $term_query_01->terms ) ? array() : $term_query_01->terms;

		foreach ( $select_terms as $key => $term ) {
			$term_id    = $term->term_id;
			$taxonomy   = $term->taxonomy;
			$acf_tax_id = $taxonomy . '_' . $term_id;

			$out[ $key ]['title']       = get_field( 'tbm_storie_title', $acf_tax_id ) ?: $term->name;
			$out[ $key ]['description'] = $term->description;
			$out[ $key ]['permalink']   = get_term_link( $term );
			$out[ $key ]['thumbnail']   = get_field( 'tbm_storie_thumbnail', $acf_tax_id );
		}

		return $out;
	}

	public function pagination() {
		$out = array(
			'custom'   => true,
			'end_size' => 3
		);

		return $out;
	}
}

<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageLifestyle extends Controller {
	use Partials\Pagination;

	/**
	 * Save here the posts to not__in in categories loop
	 *
	 * @var array
	 */
	private $notin = array();

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/homepage-lifestyle--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}

	/**
	 * Primo Piano
	 *
	 * @return \WP_Query
	 */
	public function hp_lifestyle_pp() {
		if ( is_paged() ) {
			return '';
		}

		return tbm_print_hp_fields( 'lifestyle_pp', 1 );
	}

	/**
	 *  Secondo Piano Alto
	 *
	 * @return \WP_Query
	 */
	public function hp_lifestyle_sp() {
		if ( is_paged() ) {
			return '';
		}

		return tbm_print_hp_fields( 'lifestyle_sp', 6 );
	}

	/**
	 * Get latest latest stories
	 *
	 * @return array
	 */
	public function featured_specials() {

		$selected_specials = get_field( 'pp_specials_lifestyle', 'option' );

		if ( ! $selected_specials ) {
			return array();
		}

		$my_args       = array(
			'taxonomy' => SITE_SPECIAL_TAXONOMY,
			'include'  => $selected_specials,
			'orderby'  => 'include'
		);
		$term_query_01 = new \WP_Term_Query( $my_args );

		$select_terms = empty( $term_query_01->terms ) ? array() : $term_query_01->terms;

		foreach ( $select_terms as $key => $term ) {
			$term_id    = $term->term_id;
			$taxonomy   = $term->taxonomy;
			$acf_tax_id = $taxonomy . '_' . $term_id;

			$out[ $key ]['title']       = get_field( 'tbm_storie_title', $acf_tax_id ) ?: $term->name;
			$out[ $key ]['description'] = $term->description;
			$out[ $key ]['permalink']   = get_term_link( $term );
			$out[ $key ]['thumbnail']   = get_field( 'tbm_storie_thumbnail', $acf_tax_id );
		}

		return $out;
	}

	/**
	 * Cycle category boxes in HP
	 *
	 * @return array
	 */
	public function hp_categories() {
		if ( is_paged() ) {
			return '';
		}
		$out = array();

		$blocks = get_field( 'tbm_categories_news_hp', 'option' );

		if ( ! $blocks ) {
			return $out;
		}

		// Defaults args
		$default_args = array(
			'posts_per_page' => 4,
			'post_type'      => array(
				'post',
				HTML_CARD_POST_TYPE,
				HTML_PODCAST_POST_TYPE,
				HTML_GALLERY_POST_TYPE
			)
		);

		foreach ( $blocks as $block ) {

			$block_data = explode( '|', $block );

			$term = get_term( $block_data[0], $block_data[1] );

			if ( ! $term || is_wp_error( $term ) ) {
				continue;
			}

			// Get excluded posts
			$not_in = get_query_var( 'notin', array() );

			$args = array(
				'tax_query'    => array(
					array(
						'taxonomy' => $term->taxonomy,
						'terms'    => $term->term_id,
					),
				),
				'post__not_in' => $not_in
			);

			$block_args = wp_parse_args( $args, $default_args );

			// Execute the query
			$the_query = new \WP_Query( $block_args );

			if ( $the_query->have_posts() ) {
				// Save the returned ids in variable
				$not_in = @array_merge( $not_in, wp_list_pluck( $the_query->posts, 'ID' ) );
				set_query_var( 'notin', $not_in );
			}

			$out[] = array(
				'name'        => $term->name,
				'url'         => get_term_link( $term ),
				'description' => $term->description ? wp_strip_all_tags( $term->description ) : '',
				'query'       => $the_query
			);

		}

		return $out;

	}

	public function pagination() {

		$out = array(
			'custom' => true
		);

		return $out;

	}

}

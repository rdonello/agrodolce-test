<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 17:29
 */

namespace App\Controllers;

use Sober\Controller\Controller;
use TbmCommon\Taxonomies\Sponsor;

class SinglePost extends Controller {

	use Partials\RelatedTaxonomy;
	use Partials\CommonSingle;

	/**
	 * Sponsor
	 *
	 * @return mixed
	 */
	public function sponsor() {
		$sponsor = new Sponsor();
		if ( $sponsor->name !== null || $sponsor->url !== null ) {
			return tbm_get_sponsor( $sponsor );
		}
	}

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {

		// If post has video, generate video critical css
		$post_critical = get_field( "post-video-id", get_the_ID() ) ? '/dist/css/critical/single-post-video--critical.min.css' : '/dist/css/critical/single-post--critical.min.css';

		$css = tbm_critical_css( [
			$post_critical,
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;
	}

	public function related_object() {
		$post_id = get_the_ID();

		// Get the yoast primary term
		return tbm_get_related_chronological( $post_id, SITE_SPECIAL_TAXONOMY );
	}

}

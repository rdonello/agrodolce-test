<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageVideoricette extends Controller {
	use Partials\Pagination;

	protected $custom_pagination = [];

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/homepage-secondary--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}


	public function get_videoricetta() {
		$page           = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$posts_per_page = 20;

		/**
		 * Query total
		 */
		$total = count( get_posts( array(
			'post_type'      => array( 'cocktail', 'ricetta' ),
			'fields'         => 'ids',
			'posts_per_page' => - 1,
			'meta_query'     => array(
				'relation' => 'AND',
				array(
					'key'     => 'post-video-id',
					'compare' => 'EXISTS',
				),
				array(
					'key'     => 'post-video-id',
					'value'   => '',
					'compare' => '!=',
				)
			)
		) ) );


		/**
		 * Build pagination
		 */
		$this->custom_pagination = array(
			'current'          => $page,
			'total' => intval( $total / $posts_per_page ),
			'is_paginable'  => true,
			'custom'        => true
		);

		return new \WP_Query( array(
			'post_type'      => array( 'cocktail', 'ricetta' ),
			'paged'          => $page,
			'posts_per_page' => $posts_per_page,
			'meta_query'     => array(
				'relation' => 'AND',
				array(
					'key'     => 'post-video-id',
					'compare' => 'EXISTS',
				),
				array(
					'key'     => 'post-video-id',
					'value'   => '',
					'compare' => '!=',
				)
			)
		) );

	}


	public function pagination() {
		return $this->custom_pagination;
	}

}

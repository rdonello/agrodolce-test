<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;

class App extends Controller {

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( array(
			'/dist/css/critical/archive--critical.min.css',
			'/dist/css/custom-critical.min.css'
		), '/dist/css/custom.min.css' );

		return $css;

	}

	/**
	 * Return the posts count of the main query
	 *
	 * @return bool
	 */
	public static function tbm_count_posts() {
		global $wp_query;

		return $wp_query->found_posts;
	}

	/**
	 * Minify HTML
	 *
	 * @param string $content
	 *
	 * @return string
	 */
	public static function minify_html( $content = '' ) {

		if ( ! $content ) {
			return '';
		}

		$minified_content = '';

		$minified_content = \Minify_HTML::minify( $content, array(
			'jsMinifier'  => array( 'JSMin\\JSMin', 'minify' ),
			'cssMinifier' => array( 'Minify_CSSmin', 'minify' )
		) );

		return $minified_content;

	}

	/** Returns the Facebook App ID */
	public function tbm_fb_app_id() {
		return get_field( 'tbm_fb_app_id', 'options' );
	}

	/**
	 * Return the link to modal close button
	 *
	 * @return int|mixed
	 */
	public function modal_close_button() {

		if ( get_post_type_archive_link( get_post_type() ) ) {
			return get_post_type_archive_link( get_post_type() );
		}

		return home_url();
	}

	/**
	 * Return the page number
	 *
	 * @return int|mixed
	 */
	public function paged() {
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		return $paged;
	}

	/**
	 * Used in some partials. Avoids notice. Returns empty string
	 *
	 * @return string ''
	 */
	public function banner() {
		return '';
	}

	/**
	 *  Used in some partials. Avoids notice
	 *
	 * @return int 0
	 */
	public function counter() {
		return 0;
	}

	/**
	 * Returns the Site name
	 *
	 * @return string
	 */
	public function siteName() {
		return get_bloginfo( 'name' );
	}

	/**
	 * Returns the theme version
	 *
	 * @return false|string
	 */
	public function theme_version() {
		return wp_get_theme()->get( 'Version' );
	}

	/**
	 * Lifestyle menu
	 *
	 * @return bool|array
	 */
	public function lifestyle_menu() {

		if ( is_singular( 'post' ) || is_page( 'lifestyle' ) || is_category() ) {
			$main_menu_args = [
				'theme_location' => 'menu_lifestyle',
				'container'      => '',
				'menu_class'     => '',
				'echo'           => false,
				'items_wrap'     => '%3$s',
				'walker'         => new Agrodolce_Menu_Secondary
			];

			return $main_menu_args;
		}

		return false;

	}

	/**
	 * Lifestyle menu
	 *
	 * @return bool|array
	 */
	public function recipe_menu() {

		if ( is_singular( array( 'ricetta', 'menu', 'cocktail' ) )
		     || is_post_type_archive( array( 'ricetta', 'menu', 'cocktail' ) )
		     || is_page( array( 'videoricette', 'ingredienti' ) )
		     || is_tax( 'ingredienti' )
		) {

			$main_menu_args = [
				'theme_location' => 'menu_recipe',
				'container'      => '',
				'menu_class'     => '',
				'echo'           => false,
				'items_wrap'     => '%3$s',
				'walker'         => new Agrodolce_Menu_Secondary
			];

			return $main_menu_args;
		}

		return false;

	}

	/**
	 * Main menu
	 *
	 * @return array
	 */
	public function above_the_fold_menu() {
		$trend_menu_args = [
			'theme_location' => 'above_the_fold_menu_navigation',
			'items_wrap'     => '%3$s',
			'container'      => '',
			'menu_class'     => '',
			'echo'           => false,
			'walker'         => new Agrodolce_Above_The_Fold_Menu
		];

		return $trend_menu_args;
	}

	public function tbm_site_logo() {
		$img = get_field( 'tbm_site_amp_logo', 'options' );

		if ( $img ) {
			return tbm_wp_get_attachment_image_url( get_field( 'tbm_site_amp_logo', 'options' ), array( 444, 133 ) );
		}

		return '';

	}

	public function tbm_site_logo_footer() {
		$img = get_field( 'tbm_site_amp_logo', 'options' );

		if ( $img ) {
			return tbm_wp_get_attachment_image_url( get_field( 'tbm_site_amp_logo', 'options' ), array( 97, 0 ) );
		}

		return '';

	}

	public function light_request() {
		if ( ! empty( $_GET['critical'] ) && $_GET['critical'] === '1' ) {
			return true;
		}

		return false;
	}


	/*
	 * Return budget list array where key is term_id
	 */
	public function agrodolce_get_budget_list() {
		$budget_list[1] = 'Entro i 30€';
		$budget_list[2] = 'Entro i 50€';
		$budget_list[3] = 'Oltre i 50€';

		return $budget_list;
	}

	/**
	 * Get Yoast breadcrumb
	 *
	 * @return string
	 */
	public function get_breadcrumb() {
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			return yoast_breadcrumb( '', '', false );
		}

		return '';
	}
}



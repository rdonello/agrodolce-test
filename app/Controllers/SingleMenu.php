<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 17/11/2018
 * Time: 17:59
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleMenu extends Controller {

	use Partials\Pagination;

	private $max_num_pages = 0;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/single-menu--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;
	}

	// Get all ACF fields
	public static function get_args( $page ) {

		$tax_query_args = array();
		$tax_meta_args  = array();

		//special_in
		$special_in = get_field( 'special_in' );
		if ( $special_in ) {
			$tax_query_args[] = array( 'taxonomy' => SITE_SPECIAL_TAXONOMY, 'terms' => $special_in );
		}

		//ingredient_in
		$ingredient_in = get_field( 'ingredient_in' );
		if ( $ingredient_in ) {
			$tax_query_args[] = array( 'taxonomy' => 'ingredienti', 'terms' => $ingredient_in );
		}

		//ingredient_not_in
		$ingredient_not_in = get_field( 'ingredient_not_in' );
		if ( $ingredient_not_in ) {
			$tax_query_args[] = array(
				'taxonomy' => 'ingredienti',
				'terms'    => $ingredient_not_in,
				'operator' => 'NOT IN'
			);
		}

		//recipe_category_in
		$recipe_category_in = get_field( 'recipe_category_in' );
		if ( $recipe_category_in ) {
			$tax_query_args[] = array( 'taxonomy' => 'categoria_ricetta', 'terms' => $recipe_category_in );
		}

		//recipe_category_not_in
		$recipe_category_not_in = get_field( 'recipe_category_not_in' );
		if ( $recipe_category_not_in ) {
			$tax_query_args[] = array(
				'taxonomy' => 'categoria_ricetta',
				'terms'    => $recipe_category_not_in,
				'operator' => 'NOT IN'
			);
		}

		//filter_in
		$filter_in = get_field( 'filter_in' );
		if ( $filter_in ) {
			$tax_query_args[] = array( 'taxonomy' => 'filters', 'terms' => $filter_in );
		}

		//filter_not_in
		$filter_not_in = get_field( 'filter_not_in' );
		if ( $filter_not_in ) {
			$tax_query_args[] = array(
				'taxonomy' => 'filters',
				'terms'    => $filter_not_in,
				'operator' => 'NOT IN'
			);
		}

		//difficolta_in
		$difficolta_in = get_field( 'difficolta_in' );
		if ( $difficolta_in ) {
			$tax_meta_args[] = array( 'key' => 'difficolta', 'value' => $difficolta_in );
		}

		//tempo_preparazione_max
		$tempo_preparazione_max = get_field( 'tempo_preparazione_max' );
		if ( $tempo_preparazione_max ) {
			$tax_meta_args[] = array(
				'key'     => 'tempo_preparazione',
				'value'   => (int) $tempo_preparazione_max,
				'compare' => '<='
			);
		}

		//tempo_cottura_max
		$tempo_cottura_max = get_field( 'tempo_cottura_max' );
		if ( $tempo_cottura_max ) {
			$tax_meta_args[] = array(
				'key'     => 'tempo_cottura',
				'value'   => (int) $tempo_cottura_max,
				'compare' => '<='
			);
		}

		//calorie_max
		$calorie_max = get_field( 'calorie_max' );
		if ( $calorie_max ) {
			$tax_meta_args[] = array(
				'key'     => 'calorie',
				'value'   => (int) $calorie_max,
				'compare' => '<='
			);
		}

		// Final arguments
		$args = array(
			'post_type'      => 'ricetta',
			'posts_per_page' => 20,
			'paged'          => $page,
			'tax_query'      => $tax_query_args,
			'meta_query'     => $tax_meta_args,
		);

		return $args;
	}

	public function menu_posts() {
		// Get current page
		$page = ( get_query_var( 'rp' ) ) ? absint( get_query_var( 'rp' ) ) : 1;

		// Get all args
		$args = $this::get_args( $page );

		// Get posts
		$posts = new \WP_Query( $args );

		$this->max_num_pages = $posts->max_num_pages;

		return $posts;

	}

	public function pagination() {
		$page          = ( get_query_var( 'rp' ) ) ? absint( get_query_var( 'rp' ) ) : 1;
		$max_num_pages = $this->max_num_pages;

		$is_paginable = $max_num_pages > 1 ? true : false;

		$out = array(
			'custom'       => true,
			'current'      => $page,
			'format'       => 'rp/%#%/',
			'base'         => get_permalink() . '%_%',
			'total'        => $max_num_pages,
			'is_paginable' => $is_paginable,
		);

		return $out;
	}

}

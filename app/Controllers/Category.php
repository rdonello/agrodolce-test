<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Category extends Controller {

	use Partials\Pagination;

	public $acf_tax_id;
	public $term_id;
	public $queried_object;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		if ( is_paged() ) {
			$css = tbm_critical_css( [
				'/dist/css/critical/archive--critical.min.css',
				'/dist/css/custom-critical.min.css'
			], '/dist/css/custom.min.css' );
		} else {
			$css = tbm_critical_css( [
				'/dist/css/critical/homepage-secondary--critical.min.css',
				'/dist/css/custom-critical.min.css'
			], '/dist/css/custom.min.css' );
		}

		return $css;

	}

	public function run() {
		$queried_object = get_queried_object();

		if ( $queried_object ) {
			$term_id  = $queried_object->term_id;
			$taxonomy = $queried_object->taxonomy;

			$this->queried_object = $queried_object;
			$this->term_id        = $term_id;
			$this->acf_tax_id     = $taxonomy . '_' . $term_id;
		}
	}


	/**
	 * Get the post top
	 *
	 * @return \WP_Query
	 */
	public function get_post_top() {

		// Check if we have a featured post
		if ( $post_id = get_field( 'tbm_featured_category_post', $this->acf_tax_id ) ) {
			set_query_var( 'notin', (array) $post_id );

			return new \WP_Query( array( 'post__in' => (array) $post_id ) );
		}

		// Exclude posts in $stories_related_post_ids
		$not_in = get_query_var( 'notin', array() );

		// Build the query
		$my_args = array(
			'posts_per_page' => 1,
			'post_type'      => 'post',
			'post__not_in'   => $not_in,
			'cat'            => $this->term_id
		);

		$posts  = new \WP_Query( $my_args );
		$not_in = array_merge( $not_in, wp_list_pluck( $posts->posts, 'ID' ) );
		set_query_var( 'notin', $not_in );

		return $posts;
	}

	/**
	 * Get latest news first column
	 *
	 * @return array|\WP_Query
	 */
	public function get_latest_spa() {
		$not_in  = get_query_var( 'notin', array() );
		$my_args = array(
			'category__in'   => $this->term_id,
			'posts_per_page' => 2,
			'post_type'      => 'post',
			'post__not_in'   => $not_in
		);
		$posts   = new \WP_Query( $my_args );

		$not_in = array_merge( $not_in, wp_list_pluck( $posts->posts, 'ID' ) );
		set_query_var( 'notin', $not_in );

		return $posts;
	}

	/**
	 * Get latest news first column
	 *
	 * @return array|\WP_Query
	 */
	public function get_latest_spb() {
		$not_in  = get_query_var( 'notin', array() );
		$my_args = array(
			'category__in'   => $this->term_id,
			'posts_per_page' => 2,
			'post_type'      => 'post',
			'post__not_in'   => $not_in
		);
		$posts   = new \WP_Query( $my_args );

		$not_in = array_merge( $not_in, wp_list_pluck( $posts->posts, 'ID' ) );
		set_query_var( 'notin', $not_in );

		return $posts;
	}

	/**
	 * Get latest news second column
	 *
	 * @return array|\WP_Query
	 */
	public function get_latest() {
		$not_in  = get_query_var( 'notin', array() );
		$my_args = array(
			'category__in'   => $this->term_id,
			'posts_per_page' => 3,
			'post_type'      => 'post',
			'post__not_in'   => $not_in
		);
		$posts   = new \WP_Query( $my_args );
		$not_in  = array_merge( $not_in, wp_list_pluck( $posts->posts, 'ID' ) );
		set_query_var( 'notin', $not_in );

		return $posts;
	}

	/**
	 * Get the child categories
	 *
	 * @return array
	 */
	protected function get_child_categories() {

		$out = array();

		$child_categories = get_terms(
			array(
				'taxonomy'   => 'category',
				'parent'     => $this->term_id,
				'hide_empty' => false
			)
		);

		if ( ! $child_categories || is_wp_error( $child_categories ) ) {
			return array();
		}

		foreach ( $child_categories as $key => $category ) {
			$acf_tax_id = $category->taxonomy . '_' . $category->term_id;

			$out[ $key ]['type']        = 'child';
			$out[ $key ]['title']       = get_field( 'tbm_category_title', $acf_tax_id ) ?: $category->name;
			$out[ $key ]['description'] = $category->description;
			$out[ $key ]['permalink']   = get_term_link( $category );
		}

		return $out;

	}

	/**
	 * Get the parent category
	 *
	 * @return array
	 */
	protected function get_parent_category() {

		$out = array();

		// If is a parent category, return
		if ( $this->queried_object->parent === 0 ) {
			return array();
		}

		$parent_category = get_terms( array(
			'include'    => $this->queried_object->parent,
			'taxonomy'   => 'category',
			'hide_empty' => false
		) );

		if ( ! $parent_category || is_wp_error( $parent_category ) ) {
			return array();
		}

		foreach ( $parent_category as $key => $category ) {
			$acf_tax_id = 'category_' . $category->term_id;

			$out[ $key ]['type']        = 'parent';
			$out[ $key ]['title']       = get_field( 'tbm_category_title', $acf_tax_id ) ?: $category->name;
			$out[ $key ]['description'] = $category->description;
			$out[ $key ]['permalink']   = get_term_link( $category );
		}

		return $out;
	}

	/**
	 * Get the sister (same level) categories
	 *
	 * @return array
	 */
	protected function get_sister_categories() {

		$out = array();

		// If is a parent category, return
		if ( $this->queried_object->parent === 0 ) {
			return array();
		}

		$sister_categories = get_terms( array(
			'parent'   => $this->queried_object->parent,
			'exclude'  => $this->term_id,
			'taxonomy' => 'category'
		) );

		if ( ! $sister_categories || is_wp_error( $sister_categories ) ) {
			return array();
		}

		foreach ( $sister_categories as $key => $category ) {
			$acf_tax_id = 'category_' . $category->term_id;

			$out[ $key ]['type']        = 'sister';
			$out[ $key ]['title']       = get_field( 'tbm_category_title', $acf_tax_id ) ?: $category->name;
			$out[ $key ]['description'] = $category->description;
			$out[ $key ]['permalink']   = get_term_link( $category );
		}

		return $out;
	}

	/**
	 * Get latest latest selected stories
	 *
	 * @return array
	 */
	protected function get_related_stories() {

		$terms_id = get_field( 'tbm_cat_related_stories', $this->acf_tax_id );

		if ( ! $terms_id ) {
			return array();
		}

		foreach ( $terms_id as $key => $term_id ) {
			$term = get_term( $term_id );

			if ( ! $term || is_wp_error( $term ) ) {
				continue;
			}

			$taxonomy   = $term->taxonomy;
			$acf_tax_id = $taxonomy . '_' . $term_id;

			$out[ $key ]['type']        = 'story';
			$out[ $key ]['title']       = get_field( 'tbm_storie_title', $acf_tax_id ) ?: $term->name;
			$out[ $key ]['description'] = $term->description;
			$out[ $key ]['permalink']   = get_term_link( $term );
		}

		return $out;
	}

	/**
	 * Get all the terms related to this category
	 *
	 * @return array
	 */
	public function featured_specials() {
//		$childs  = $this->get_child_categories();
//		$parent  = $this->get_parent_category();
//		$sisters = $this->get_sister_categories();
		$stories = $this->get_related_stories();

		return $stories;
	}

	/**
	 * Get one gallery post
	 *
	 * @return \WP_Query
	 */
	public function get_archive_posts() {
		$not_in  = get_query_var( 'notin', array() );
		$my_args = array(
			'posts_per_page' => 15,
			'post_type'      => 'post',
			'post__not_in'   => $not_in,
			'cat'            => $this->term_id
		);
		$posts   = new \WP_Query( $my_args );

		return $posts;
	}

	/**
	 * Set pagination defaults
	 *
	 * @return array
	 */
	public function pagination() {

		$out = array(
			'custom' => true,
			'link'   => get_term_link( get_queried_object_id() )
		);

		return $out;

	}

}

<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 17:29
 */

namespace App\Controllers;

use Sober\Controller\Controller;
use TbmCommon\Taxonomies\Sponsor;

class SingleCocktail extends Controller {

	use Partials\RelatedTaxonomy;
	use Partials\CommonSingle;

	/**
	 * Get ACF fields
	 *
	 * @var string[]
	 */
	protected $acf = [
		'tempo_preparazione',
		'bicchiere',
		'difficolta',
		'ricette-preparazione',
		'ricette-ingredienti-persone',
		'ricette-variante',
		'ricette-conclusione'
	];

	/**
	 * Sponsor
	 *
	 * @return mixed
	 */
	public function sponsor() {
		$sponsor = new Sponsor();
		if ( $sponsor->name !== null || $sponsor->url !== null ) {
			return tbm_get_sponsor( $sponsor );
		}
	}

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/single-cocktail--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;
	}

	public function related_cocktails() {

		$recipes = tbm_get_custom_related_posts( null, 6, array(), 'categoria_ricetta', array(
			'cocktail',
		), 'object' );

		return $recipes;

	}

	public function get_ingredients() {

		$post_id = get_the_ID();

		$ingredients = get_field( 'ingredienti-repeater', $post_id );

		// Declare final results array2
		$results = array();

		foreach ( $ingredients as $ingredient ) {

			$term = get_term( (int) $ingredient['ingredienti-nome'], 'ingredienti' );

			if ( ! $term || is_wp_error( $term ) ) {
				return array();
			}

			$out['nome']     = $term->name;
			$out['url']      = get_term_link( $term );
			$out['spec']     = $ingredient['ingredienti-spec'];
			$out['quantita'] = $ingredient['ingredienti-quantita'];
			$out['unita']    = $ingredient['ingredienti-unita'];

			// Push array in main array
			$results[] = $out;

		}

		return $results;
	}

	public function get_categoria_cocktail() {

		$categoria_cocktail = get_the_terms( get_the_ID(), 'categoria_ricetta' );

		if ( ! $categoria_cocktail || is_wp_error( $categoria_cocktail ) ) {
			return array();
		}

		$out['name'] = $categoria_cocktail[0]->name;
		$out['url']  = get_term_link( $categoria_cocktail[0] );

		return $out;

	}

	public function get_products_list() {
		$product_list_id = get_field( 'tbm_related_products', get_the_ID() );

		$products = array();

		if ( empty( $product_list_id ) ) {
			return array();
		}

		foreach ( $product_list_id as $post_id ) {
			$out['seller']       = 'Amazon';
			$out['name']         = get_post_field( 'post_title', $post_id );
			$out['price']        = get_field( 'tbm_amazon_price', $post_id );
			$out['link']         = get_field( 'tbm_amazon_link', $post_id );
			$out['featured_url'] = get_the_post_thumbnail_url( $post_id );

			// Push the products in final array
			$products[] = $out;
		}

		return $products;
	}

}

<?php


namespace App\Controllers;

use Yoast\WP\SEO\Generators\Schema\Abstract_Schema_Piece;
use Yoast\WP\SEO\Surfaces\Helpers_Surface;
use Yoast\WP\SEO\Config\Schema_IDs;
use WPSEO_Schema_Context;
use DOMWrap\Document;

class SchemaRecipe extends Abstract_Schema_Piece {

	/**
	 * A value object with context variables.
	 *
	 * @var WPSEO_Schema_Context
	 */
	public $context;

	/**
	 * @var The helpers surface
	 *
	 * @var Helpers_Surface
	 */
	public $helpers;

	/**
	 * Recipe constructor.
	 *
	 * @param WPSEO_Schema_Context $context Value object with context variables.
	 */
	public function __construct( WPSEO_Schema_Context $context ) {
		$this->context = $context;
	}

	/**
	 * Determines whether or not a piece should be added to the graph.
	 *
	 * @return bool Whether or not a piece should be added.
	 */
	public function is_needed() {
		if ( is_singular( array( 'ricetta', 'cocktail' ) ) && ! empty ( $this->get_name() ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Adds an image node if the post has a featured image.
	 *
	 * @param array $data The Recipe data.
	 *
	 * @return array $data The Recipe data.
	 */
	private function add_image( $data ) {
		if ( $this->context->main_image_url !== null ) {
			$data['image']        = [
				'@id' => $this->context->canonical . Schema_IDs::PRIMARY_IMAGE_HASH,
			];
			$data['thumbnailUrl'] = $this->context->main_image_url;
		}

		return $data;
	}

	/**
	 * Determines the name of the recipe
	 *
	 * @return string
	 */
	private function get_name() {
		$recipeId   = $this->context->post->ID;
		$customName = ! empty( get_field( 'nome_ricetta', $recipeId ) ) ? get_field( 'nome_ricetta', $recipeId ) : $this->context->post->post_title;

		return $customName;
	}

	/**
	 * Adds the category
	 *
	 * @param array $data The Recipe data.
	 *
	 * @return array $data The Recipe data.
	 */
	private function add_category( $data ) {
		$category     = '';
		$mainCategory = get_field( 'main-category-post', $this->context->post->ID );
		$categories   = wp_get_post_terms( $this->context->post->ID, "categoria_ricetta" );
		if ( ! empty( $categories ) ) {
			foreach ( $categories as $cat ) {
				if ( ! empty( $mainCategory ) && $mainCategory === $cat->term_id ) {
					$category = $cat->name;
					break;
				}
				$category = $cat->name;
			}
		}
		if ( ! empty ( $category ) ) {
			$data['recipeCategory'] = $category;
		}

		return $data;
	}

	/**
	 * Adds the ingredients
	 *
	 * @param array $data The Recipe data.
	 *
	 * @return array $data The Recipe data.
	 */
	private function add_ingredients( $data ) {
		$ingredients = [];

		if ( have_rows( 'ingredienti-repeater', $this->context->post->ID ) ) {
			while ( have_rows( 'ingredienti-repeater', $this->context->post->ID ) ) {
				the_row();

				$term_id_name = get_sub_field( 'ingredienti-nome' );
				if ( ! $term_id_name ) {
					continue;
				}

				$ingr = get_term_by( 'id', (int) $term_id_name, 'ingredienti' );

				if ( ! $ingr || is_wp_error( $ingr ) ) {
					continue;
				}

				$name_ing = $ingr->name;
				$quantity = get_sub_field( 'ingredienti-quantita' );
				$quantity = str_replace( '1/2', '½', $quantity );
				$unit     = get_sub_field( 'ingredienti-unita' );
				$unit_ing = agrodolce_get_ingredienti_plural( $unit, (int) $quantity );

				$specific = ! empty( get_sub_field( 'ingredienti-spec' ) ) ? stripslashes( get_sub_field( 'ingredienti-spec' ) ) . ' ' : '';
				if ( ( isset( $unit ) && ( null !== $unit ) && ( $unit == "q.b." ) ) || ! isset( $quantity ) || empty( $quantity ) ) {
					$ingredients[] = ucfirst( $name_ing ) . ' ' . $specific . 'q.b.';
				} else {
					$ingredients[] = $quantity . ' ' . $unit_ing . ' ' . ucfirst( $name_ing ) . $specific;
				}

			}
		}
		if ( ! empty( $ingredients ) ) {
			$data['recipeIngredient'] = $ingredients;
		}

		return $data;
	}

	/**
	 * Adds recipe keywords
	 *
	 * @param array $data The Recipe data.
	 *
	 * @return array $data The Recipe data.
	 */
	private function add_keywords( $data ) {

		// Get the terms attached to the post
		$terms = wp_get_object_terms( $this->context->post->ID, array( 'categoria_ricetta', 'speciale' ) );

		if ( ! $terms || is_wp_error( $terms ) ) {
			return $data;
		}

		$data['keywords'] = wp_list_pluck( $terms, 'name' );

		return $data;
	}

	/**
	 * Adds instructions
	 *
	 * @param array $data The Recipe data.
	 *
	 * @return array $data The Recipe data.
	 */
	private function add_instructions( $data ) {

		$recipeUrl        = $this->context->canonical;
		$counter          = 1;
		$steps            = [];
		$prepInstructions = get_field( 'ricette-preparazione', $this->context->post->ID );
		if ( ! empty( $prepInstructions ) ) {
			$prepInstructions    = preg_replace( [
				"/<ol([\n\t\r\b]+[a-z]\"+)?>/",
				"/<\/ol>/",
				"/<p>/",
				"/<\/p>/",
			], "", $prepInstructions );
			$prepInstructionsArr = preg_split( "/<li class=\"steps__item\" id=\"step-(\d)\">/", $prepInstructions );
			if ( ! empty( $prepInstructionsArr ) ) {
				foreach ( $prepInstructionsArr as $step ) {
					$doc = new Document();
					$doc->setLibxmlOptions( LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
					$doc->html( $step );
					$image     = $doc->find( 'img' );
					$image_url = $image->attr( 'data-src' );
					$step      = trim( str_replace( [ '\n', '\t', '\r' ], '', strip_tags( $step ) ) );
					if ( ! empty( $step ) ) {
						$to_add = [
							'@type' => 'HowToStep',
							'name'  => $this->get_name() . ': step ' . $counter,
							'url'   => $recipeUrl . '#step-' . $counter,
							'text'  => $step
						];
						if ( ! empty( $image_url ) ) {
							$to_add['image'] = $image_url;
						}
						$steps[] = $to_add;
						$counter ++;
					}
				}
			}
		}

		// Conclusione
		$endInstructions    = get_field( 'ricette-conclusione', $this->context->post->ID );
		$endInstructionsTxt = trim( str_replace( [ '\n', '\t', '\r' ], '', strip_tags( $endInstructions ) ) );
		if ( ! empty( $endInstructionsTxt ) ) {
			$steps[] = [
				'@type' => 'HowToStep',
				'name'  => $this->get_name() . ': conclusione',
				'url'   => $recipeUrl . '#conclusione',
				'text'  => $endInstructionsTxt
			];
		}

		// Varianti
		$varInstructions     = get_field( 'ricette-variante', $this->context->post->ID );
		$varInstructionsText = trim( str_replace( [ '\n', '\t', '\r' ], '', strip_tags( $varInstructions ) ) );
		if ( ! empty( $varInstructionsText ) ) {
			$steps[] = [
				'@type' => 'HowToStep',
				'name'  => $this->get_name() . ': variante',
				'url'   => $recipeUrl . '#variante',
				'text'  => $varInstructionsText
			];
		}

		if ( $steps ) {
			$data['recipeInstructions'] = $steps;
		} else {
			$my_content                 = strip_shortcodes( $this->context->post->post_content );
			$data['recipeInstructions'] = apply_filters( 'the_content_feed', $my_content );
		}

		return $data;
	}

	/**
	 * Adds video to recipe schema
	 *
	 * @param array $data The Recipe data.
	 *
	 * @return array $data The Recipe data.
	 */
	private function add_video( $data ) {

		$video = [];

		// Get video ID
		$video_ricetta_id = get_field( "post-video-id", $this->context->post->ID );
		if ( ! $video_ricetta_id ) {
			return $data;
		}

		// Check if video is available
		$video_post_fields = get_post( $video_ricetta_id );
		if ( ! $video_post_fields ) {
			return $data;
		}

		// Get video fields
		$video_fields = tbm_get_videoid_field( $video_ricetta_id );
		if ( empty( $video_fields['url'] ) ) {
			return $data;
		}

		// Get video URL
		$content_url = get_field( 'url_video_elaborato_bassa', $video_ricetta_id ) ?: $video_fields['url'];

		$video[]       = [
			'@type'        => 'VideoObject',
			'name'         => get_the_title( $video_ricetta_id ),
			'description'  => get_the_excerpt( $video_ricetta_id ),
			'thumbnailUrl' => $this->context->main_image_url,
			'contentUrl'   => $content_url,
			'uploadDate'   => get_the_date( 'c', $video_ricetta_id )
		];
		$data['video'] = $video;


		return $data;
	}

	/**
	 * Returns Recipe data.
	 *
	 * @return array Recipe data.
	 */
	public function generate() {

		$recipeUrl = $this->context->canonical;
		$data      = [
			'@type'            => 'Recipe',
			'@id'              => $recipeUrl . '#recipe',
			'name'             => $this->get_name(),
			'isPartOf'         => [ '@id' => $recipeUrl . Schema_IDs::WEBPAGE_HASH ],
			'author'           => [ '@id' => $this->helpers->schema->id->get_user_schema_id( $this->context->post->post_author, $this->context ) ],
			'datePublished'    => $this->helpers->date->format( $this->context->post->post_date_gmt ),
			'dateModified'     => $this->helpers->date->format( $this->context->post->post_modified_gmt ),
			'mainEntityOfPage' => [ '@id' => $recipeUrl . Schema_IDs::WEBPAGE_HASH ],
		];

		if ( $this->context->site_represents_reference ) {
			$data['publisher'] = $this->context->site_represents_reference;
		}

		if ( ! empty( strip_tags( $this->context->post->post_excerpt ) ) ) {
			$data['description'] = strip_tags( $this->context->post->post_excerpt );
		}

		$prepTime = get_field( "tempo_preparazione", $this->context->post->ID );
		if ( ! empty( $prepTime ) ) {
			$data['totalTime'] = 'PT' . $prepTime . 'M';
		}

		$calories = get_field( 'calorie', $this->context->post->ID );
		if ( ! empty( $calories ) ) {
			$data['nutrition'] = [ '@type' => 'NutritionInformation', 'calories' => $calories ];
		}

		$yeld                = get_field( 'ricette-ingrediente-persone', $this->context->post->ID );
		$data['recipeYield'] = ! empty( $yeld ) && is_numeric( $yeld ) ? $yeld : 4;

		$data = $this->add_image( $data );
		$data = $this->add_category( $data );
		$data = $this->add_ingredients( $data );
		$data = $this->add_keywords( $data );
		$data = $this->add_instructions( $data );
		$data = $this->add_video( $data );
		$data = $this->helpers->schema->language->add_piece_language( $data );

		return $data;
	}

}

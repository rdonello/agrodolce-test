<?php
/**
 * Created by Vim.
 * User: Lino
 * Date: 01/09/2021
 * Time: 9:00
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class TaxonomyIngredienti extends Controller {

	use Partials\Pagination;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/taxonomy-ingrediente--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}

	public $acf_tax_id;

	public $term_id;

	public function __construct() {
		$this->run();
	}

	protected function run() {
		$queried_object = get_queried_object();

		if ( $queried_object ) {
			$term_id       = $queried_object->term_id;
			$taxonomy      = $queried_object->taxonomy;
			$this->term_id = $term_id;

			$this->acf_tax_id = $taxonomy . '_' . $term_id;
		}
	}

	/**
	 * Get the attached img ID
	 *
	 * @return int
	 */
	public function term_image_id() {
		$image_id = null;
		$arr_image = get_field( 'tbm_ingredienti_immagine_in_evidenza', $this->acf_tax_id );
		if ( !empty( $arr_image['ID'] )) {
			$image_id = $arr_image['ID'];
		}

		return $image_id;
	}

	public function term_content() {
		return apply_filters( 'the_content', get_field( 'tbm_ingredienti_descrizione', $this->acf_tax_id ) );
	}

	/**
	 * Set pagination defaults
	 *
	 * @return array
	 */
	public function pagination() {

		$out = array(
			'custom' => true,
			'link'   => get_term_link( get_queried_object_id() )
		);

		return $out;

	}

}

<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 17:29
 */

namespace App\Controllers;

use Sober\Controller\Controller;
use TbmCommon\Taxonomies\Sponsor;

class SingleNews extends Controller {

	use Partials\RelatedTaxonomy;
	use Partials\CommonSingle;

	/**
	 * Sponsor
	 *
	 * @return mixed
	 */
	public function sponsor() {
		$sponsor = new Sponsor();
		if ( $sponsor->name !== null || $sponsor->url !== null ) {
			return tbm_get_sponsor( $sponsor );
		}
	}

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/single-news--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;
	}


	/**
	 *
	 * Get the related posts
	 * SEO: Se l’articolo è associato ad una special_taxonomy (prioritario rispetto a categoria), dato l’articolo corrente, prendere 4 articoli della stessa special_taxonomy che lo precedono e 3 della stessa Storia che lo seguono per data di pubblicazione
	 *
	 * @param int $id The post ID
	 *
	 */
	public function get_related_posts() {

		$post_id = get_the_ID();

		// Set taxonomy to query
		$taxonomy = has_term( '', SITE_SPECIAL_TAXONOMY, $post_id ) ? SITE_SPECIAL_TAXONOMY : 'category';

		// Get the date
		$current_post_date = get_post_field( 'post_date', $post_id );

		// Check the cache
		$query_key = 'tbm_related_' . md5( $current_post_date . $post_id );
		$result    = wp_cache_get( $query_key, 'tbm' );

		if ( $result ) {
			return $result;
		}

		// Get the related terms
		$term_array = wp_get_object_terms( $post_id, $taxonomy, array( 'fields' => 'ids' ) );

		// Define the args for posts published after the current
		$args_after = array(
			'posts_per_page' => 3,
			'fields'         => 'ids',
			'tax_query'      => array(
				array(
					'taxonomy' => $taxonomy,
					'terms'    => $term_array
				),
			),
			'date_query'     => array(
				array(
					'after' => $current_post_date,
				),
			),
		);

		// Get posts
		$post_after_current = get_posts( $args_after );

		$post_after_current_count = count( $post_after_current );

		// Define the args for posts published before the current
		$args_before = array(
			'posts_per_page' => 6 - $post_after_current_count,
			'fields'         => 'ids',
			'tax_query'      => array(
				array(
					'taxonomy'         => $taxonomy,
					'terms'            => $term_array,
					'include_children' => false
				),
			),
			'date_query'     => array(
				array(
					'before' => $current_post_date,
				),
			),
		);

		// Get posts
		$post_before_current = get_posts( $args_before );

		// Merge IDS
		$post_ids = array_merge( $post_after_current, $post_before_current );

		wp_cache_set( $query_key, $result, 'tbm', 24 * HOUR_IN_SECONDS );

		return new \WP_Query( array( 'post__in' => $post_ids ) );

	}


}

<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class ArchiveMenu extends Controller {

	private $categories = array();
	private $navigation = array();

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/page-ricettario--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}

	// Get category with no children
	public function get_categoria_menu_categories() {

		// Get top level category
		$args       = array(
			'taxonomy' => 'categoria_menu'
		);
		$categories = get_terms( $args );

		$this->categories = $categories;
	}


	// Get only categories with children
	public function get_menu() {
		$template     = '<div id="%s" class="container list-block"><div class="col-3"><h6><a href="%s">%s</a></h6></div><div class="col-9"><ul class="list-items">%s</ul></div></div>';
		$nav_template = '<li><a href="#%s">%s</a></li>';

		foreach ( $this->categories as $category ) {

			$out = array();

			$menus = get_posts( array(
					'post_type'      => 'menu',
					'posts_per_page' => - 1,
					'tax_query'      => array(
						array(
							'taxonomy' => 'categoria_menu',
							'terms'    => $category->term_id
						),
					),
				)
			);

			// Fill the category list
			if ( $menus ) {
				foreach ( $menus as $menu ) {
					$out[] = '<li><a href="' . get_permalink( $menu ) . '">' . get_the_title( $menu ) . '</a></li>';
				}
			}

			if ( $out ) {
				$child = sprintf( $template, $category->slug, get_term_link( $category ), $category->name, implode( '', $out ) );
				$cat[] = $child;
			}

			$this->navigation[] = sprintf( $nav_template, $category->slug, $category->name );

		}

		return implode( '', $cat );
	}

	public function get_nav() {
		return sprintf( '<ul class="index-items">%s</ul>', implode( '', $this->navigation ) );
	}
}

<?php
/**
 * Created by Vim.
 * User: lino
 * Date: 12/10/2021
 * Time: 12:52
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class PageSpeciali extends Controller {

	use Partials\Pagination;

	protected $custom_pagination = [];

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/archive--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}

	public function last_terms() {
		$page = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

		$terms_per_page = get_option( 'posts_per_page' );
		$found_terms    = wp_count_terms( 'speciale' );
		$offset         = ( $page - 1 ) * $terms_per_page;

		$my_args = array(
			'taxonomy' => 'speciale',
			'number'   => $terms_per_page,
			'orderby'  => 'count',
			'count'    => true,
			'order'    => 'DESC',
			'offset'   => $offset
		);

		if ( false === ( $the_terms = get_transient( 'list_speciale_terms_' . $offset ) ) ) {
			$the_terms = new \WP_Term_Query( $my_args );
			set_transient( 'list_speciale_terms_' . $offset, $the_terms, 86400 );
		}

		$this->custom_pagination = array(
			'current'      => $page,
			'total'        => intval( $found_terms / $terms_per_page ),
			'is_paginable' => true,
			'custom'       => true
		);

		return $the_terms->get_terms();
	}

	public function pagination() {
		return $this->custom_pagination;
	}


	/**
	 * Returns the image attached to the term (with ACF)
	 *
	 * @param WP_Term $term Term object to get its image.
	 *
	 * @return string Image ID, or empty string if none available.
	 */
	public static function get_term_image( $term ) {

		if ( ! is_object( $term ) || ! property_exists( $term, 'term_id' ) || ! property_exists( $term, 'taxonomy' ) ) {
			return '';
		}

		$image_id = get_field( 'tbm_speciale_thumbnail', $term->taxonomy . '_' . $term->term_id );

		return $image_id;
	}

}

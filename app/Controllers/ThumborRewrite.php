<?php

namespace App\Controllers;

use DOMWrap\Document;

class ThumborRewrite {
	public $content = '';

	protected static function extractDimensionFromThumbor( $string = '' ) {

		if ( ! function_exists( 'tbm_get_thumbor_img' ) ) {
			return false;
		}

		$datas = tbm_get_thumbor_img( $string, 0, 0, true );

		if ( $datas ) {
			print_r( $datas );

			return false;
		}
	}

	protected static function extractDimensionsFromUrl( $image_tag = '' ) {

		if ( ! $image_tag ) {
			return false;
		}

		$home = function_exists( 'home_url' ) ? home_url() : 'https://www.agrodolce.it/';

		$regex = '#(?:' . preg_quote( $home, "/" ) . ')?\/(?:app|wp-content)\/uploads\/[0-9]{4}\/[0-9]{2}\/[^\\" ]+[_-](\d+)x(\d+)\.(?:jpe?g|png|gif)#i';
		preg_match( $regex, $image_tag, $match );

		if ( $match ) {
			$width  = isset( $match[1] ) ? (int) $match[1] : 0;
			$height = isset( $match[2] ) ? (int) $match[2] : 0;

			$out = array( 'width' => $width, 'height' => $height );

			return $out;
		}

		return false;
	}

	protected static function extractWidthOrHeigth( string $attribute = '', $node ) {

		// Check attribute
		if ( ! $attribute ) {
			return false;
		}

		// Check source
		$src = $node->attr( 'src' ) ?: 0;

		if ( ! $src ) {
			return false;
		}

		// Check width attribute first
		$attr = $node->attr( $attribute ) ?: 0;

		if ( $attr ) {
			return $attr;
		}

		// Extract data from URL
		$dimensions = ThumborRewrite::extractDimensionsFromUrl( $src );

		if ( $dimensions ) {
			return $dimensions[ $attribute ];
		}

		return false;

	}

	protected static function getThumborizedImage( $node, $width, $height ) {
		$src = $node->attr( 'src' );

		if ( ! function_exists( 'tbm_get_thumbor_img' ) ) {
			return $src;
		}

		// If the url is in CDN
		if ( strpos( $src, 'cdn.agrodolce.it' ) !== false ) {
			return $src;
		}

		$thumbor_image = tbm_get_thumbor_img( $src, $width, $height );

		if ( $thumbor_image ) {
			return $thumbor_image;
		}

		return $src;
	}

	public static function getThumborImage( $content ) {

		$doc = new Document();
		// Disable adding doctype and opening log
		$doc->setLibxmlOptions( LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
		// Load html
		$doc->html( $content );

		// Search <li> and add class
		$items = $doc->find( 'img' );

		// Loop through images
		$items->each( function ( $node ) {
			$max_width = 640;
			$width     = ThumborRewrite::extractWidthOrHeigth( 'width', $node );
			$height    = ThumborRewrite::extractWidthOrHeigth( 'height', $node );

			// Check max-width ad reformat accordingly
			if ( ! $width || $width > $max_width ) {
				$height = intval( ( $height * $max_width ) / $width );
				$width  = $max_width;
			}

			$src = ThumborRewrite::getThumborizedImage( $node, $width, $height );

			$node->attr( 'width', $width );
			$node->attr( 'height', $height );

			if ( ! tbm_is_amp() ) {
				$node->attr( 'data-src', $src );
				$node->removeAttr( 'src', '' );
				$node->addClass( 'lazyload' );
			} else {
				$node->attr( 'src', $src );
			}

		} );

		return $doc->html();

	}

}

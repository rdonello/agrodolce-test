<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class ArchiveGallery extends Controller {
	use Partials\Pagination;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/archive-gallery--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}

	public function pagination() {

		$out = array(
			'custom' => true,
			'link'   => get_post_type_archive_link( 'gallery' )
		);

		return $out;

	}

}

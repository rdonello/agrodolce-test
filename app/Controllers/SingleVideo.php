<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 01/07/2018
 * Time: 22:13
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleVideo extends Controller {

	use Partials\RelatedTaxonomy;
	use Partials\CommonSingle;

	private $video = '';

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/single-video--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}

	public function print_video() {

		/**
		 * If TBM plugin is active and activated
		 */
		if ( function_exists( 'html_video_generate_video_player' ) ) {
			$video       = html_video_generate_brid_video_player( get_the_ID(), 'single-video__embed', false );
			$this->video = $video;

			return $video;
		}

		return '';

	}

	public function print_video_class() {

		/**
		 * If video is a brid tbm video (identified by tbv-xxxx class)
		 */
		if ( strpos( $this->video, 'tbv-' ) ) {
			return 'single-video__embed--brid';
		}

		return '';

	}

	public function modal_close_link() {

		return get_post_type_archive_link( HTML_VIDEO_POST_TYPE );
	}


}

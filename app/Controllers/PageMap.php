<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 23/07/2018
 * Time: 11:47
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class PageMap extends Controller {

	use Partials\Pagination;

	private $parent_categories = array();
	private $child_categories = array();
	private $childless_categories = array();
	private $categories_with_stories = array();
	private $stories_related = array();

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/page-mappa-sito--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}

	// Get category with no children and no related stories
	public function get_childless_categories() {
		$args       = array(
			'taxonomy'   => 'category',
			'childless'  => true,
			'meta_query' => array(
				array(
					'key'     => 'tbm_cat_related_stories',
					'compare' => 'NOT EXISTS'
				)
			)
		);
		$categories = get_terms( $args );

		$this->childless_categories = $categories;
	}

	// Get category with no children and no related stories
	public function get_parent_categories() {

		// Get top level category
		$args       = array(
			'taxonomy' => 'category',
			'parent'   => 0,
		);
		$categories = get_terms( $args );

		$this->parent_categories = $categories;
	}

	// Get categories with children
	public function print_child_categories() {
		$template = '<div class="archive-categorie__list"><h4>%s</h4><ul>%s</ul></div>';


		foreach ( $this->parent_categories as $category ) {

			$out        = array();
			$stories_id = array();

			// Fill the stories list
			$stories = get_field( 'tbm_cat_related_stories', $category->taxonomy . '_' . $category->term_id );
			if ( $stories ) {
				foreach ( $stories as $story ) {
					$out[]        = '<li><a href="' . get_term_link( $story, SITE_SPECIAL_TAXONOMY ) . '">' . get_term_field( 'name', $story, SITE_SPECIAL_TAXONOMY ) . '</a></li>';
					$stories_id[] = $story;
				}
				$this->stories_related = array_merge( $this->stories_related, $stories_id );

			}

			// Fill the category list
			$childrens = get_term_children( $category->term_id, 'category' );
			if ( $childrens ) {
				foreach ( $childrens as $child ) {
					$out[] = '<li><a href="' . get_term_link( $child, 'category' ) . '">' . get_term_field( 'name', $child, 'category' ) . '</a></li>';
				}
			}

			if ( $childrens || $stories ) {
				$child = sprintf( $template, $category->name, implode( '', $out ) );
				$cat[] = $child;
			}

		}

		return implode( '', $cat );
	}

	public function print_parent_categories() {
		$template = '<div class="archive-categorie__list"><h4>Categorie</h4><ul>%s</ul></div>';

		$args       = array(
			'taxonomy'   => 'category',
		);
		$categories = get_terms( $args );

		foreach ( $categories as $category ) {
			$out[] = '<li><a href="' . get_term_link( $category ) . '">' . $category->name . '</a></li>';
		}

		return sprintf( $template, implode( '', $out ) );
	}

	public function print_orphan_stories() {
		$template = '<div class="archive-categorie__list"><h4>Speciali</h4><ul>%s</ul></div>';

		$args    = array(
			'taxonomy' => SITE_SPECIAL_TAXONOMY,
			'exclude'  => $this->stories_related
		);
		$stories = get_terms( $args );

		// Unset the printed stories


		foreach ( $stories as $story ) {
			$out[] = '<li><a href="' . get_term_link( $story ) . '">' . $story->name . '</a></li>';
		}

		return sprintf( $template, implode( '', $out ) );
	}

}

<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SinglePodcast extends Controller {
	/**
	 * Common functions for single post
	 */
	use Partials\RelatedTaxonomy;
	use Partials\CommonSingle;

	protected $spreaker_image = '';

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/single-podcast--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;
	}

	public function modal_close_link() {

		return get_post_type_archive_link( HTML_PODCAST_POST_TYPE );
	}

	public function get_podcast_embed() {

		/**
		 * Exclude if critical
		 */
		if ( isset( $_GET['critical'] ) && $_GET['critical'] === '1' ) {
			return '';
		}

		if ( get_field( 'tbm_podcast_spotify_id' ) ) :
			$id       = get_field( 'tbm_podcast_spotify_id' );
			$template = '<iframe src="https://open.spotify.com/embed/playlist/%s" width="100%%" height="400px" allowtransparency="true" allow="encrypted-media"></iframe>';
		elseif ( get_field( 'tbm_podcast_spreaker_id' ) ) :
			$id       = get_field( 'tbm_podcast_spreaker_id' );
			$template = '<iframe src="https://widget.spreaker.com/player?show_id=%s&amp;theme=dark&amp;autoplay=false&amp;playlist=show&amp;cover_image_url=%s"
					width="100%%" height="400px" ></iframe>';
		elseif ( get_field( 'tbm-ad-podcast' ) ) :
			$id       = get_field( 'tbm-ad-podcast' );
			$template = '<iframe src="https://widget.spreaker.com/player?episode_id=%s&amp;theme=light&amp;autoplay=false&amp;playlist=show&amp;cover_image_url=%s"
					width="100%%" height="400px" ></iframe>';

		else :
			$template = '';
		endif;

		return sprintf( $template, $id, '' );
	}

}

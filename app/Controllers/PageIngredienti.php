<?php
/**
 * Created by Vim.
 * User: lino
 * Date: 12/10/2021
 * Time: 12:40
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class PageIngredienti extends Controller {

	use Partials\Pagination;

	protected $term_objects = [];

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/archive--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}

	public function last_terms_query() {
		global $wpdb;
		$current_page       = get_query_var( 'letter' ) ? get_query_var( 'letter' ) : 'a';
		$query              = $wpdb->prepare( "SELECT taxonomy, wp1.term_id as term_id, wp1.name as name, wp1.slug as slug, wp2.description as description from  $wpdb->terms wp1 inner join $wpdb->term_taxonomy wp2 ON (wp1.term_id = wp2.term_id) where name like %s and (count>4 OR description<>'') and taxonomy='ingredienti'", $wpdb->esc_like( $current_page ) . '%' );
		$found_terms        = $wpdb->get_results( $query );
		$this->term_objects = $found_terms;

	}

	public function pagination() {
		return array();
	}

	public function last_terms() {

		// Define defaults fields
		$defaults = array(
			'name'        => '',
			'url'         => '',
			'description' => '',
			'image'       => '',
			'imagex2'     => '',
		);

		// Declare output array
		$out = array();

		$terms = $this->term_objects;

		if ( ! $terms || is_wp_error( $terms ) ) {
			return null;
		}

		foreach ( $terms as $term ) {
			$term_out = array();
			// Get the image
			$arr_image = get_field( 'tbm_ingredienti_immagine_in_evidenza', $term->taxonomy . '_' . $term->term_id );
			$image_id  = ! empty( $arr_image['ID'] ) ? $arr_image['ID'] : DEFAULT_IMAGE_INGREDIENTS_ID;

			$term_out['name']        = $term->name;
			$term_out['url']         = get_term_link( $term->slug, $term->taxonomy );
			$term_out['description'] = $term->description;
			$term_out['image']       = tbm_wp_get_attachment_image_url( $image_id, array( 430, 327 ) );
			$term_out['imagex2']     = tbm_wp_get_attachment_image_url( $image_id, array( 860, 654 ) );


			$out[] = wp_parse_args( $term_out, $defaults );
		}

		return $out;

	}

	public function alpha_pagination() {
		global $wpdb;
		$current_page = get_query_var( 'letter' ) ? get_query_var( 'letter' ) : 'a';
		$arr_alpha    = [];
		foreach ( range( 'a', 'z' ) as $letter ) {
			$query                             = $wpdb->prepare( "SELECT wp1.term_id as term_id, wp1.name as name, wp1.slug as slug from $wpdb->terms wp1 inner join $wpdb->term_taxonomy wp2 ON (wp1.term_id = wp2.term_id) where name like %s and (count>4 OR description<>'') and taxonomy='ingredienti'", $wpdb->esc_like( $letter ) . '%' );
			$found_terms                       = $wpdb->get_results( $query );
			$arr_alpha[ $letter ]['class']     = strtolower( $current_page ) === $letter ? 'class = "current"' : '';
			$arr_alpha[ $letter ]['has_posts'] = ( count( $found_terms ) > 0 ) ? 1 : 0;
			$arr_alpha[ $letter ]['link']      = get_page_link() . 'letter/' . $letter . '/';
		}
		//echo '<pre>';
		//print_r($arr_alpha);exit;
		return $arr_alpha;
	}


}

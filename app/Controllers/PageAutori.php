<?php
/**
 * Created by Vim.
 * User: Lino
 * Date: 20/09/2020
 * Time: 11:47
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class PageAutori extends Controller {

	use Partials\Pagination;

	protected $custom_pagination = [];

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/archive--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}

	public function authors_list(){
		global $wpdb;

		$authors_per_page = 15;
		$page = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$offset         = ( $page - 1 ) * $authors_per_page;

        $args = Array(
           'orderby' => 'display_name'
        );
        $author_to_exclude = array(
                1, // account admin senza poteri
                8, // account editore test
                9, // account autore test
                10, // account collaboratore
                11, // account traffiker
                12, // account redazione
        );

        $args['exclude'] = $author_to_exclude;
        $args['has_published_posts'] = true;
		$args['number'] = $authors_per_page;
		$args['paged'] = $page;
        $sublist_authors     = new \WP_User_Query( $args );
        $sublist_authors     = $sublist_authors->results;

		//Retrieve authors total count
		$tot_authors_args = wp_array_slice_assoc($args, ['exclude', 'has_published_posts']);
		$tot_authors = new \WP_User_Query( $tot_authors_args );
		$tot_authors = $tot_authors->results;
        $tot_users = count( $tot_authors );

		$this->custom_pagination = array(
            'page'          => $page,
            'max_num_pages' => intval( $tot_users / $authors_per_page ),
            'is_paginable'  => true,
            'custom'        => true,
            'link'          => get_page_link()
        );

		//Get meta foreach authors
		foreach($sublist_authors as $author){
			$author->page_url = get_author_posts_url( $author->ID );
			$author->description = get_the_author_meta('description', $author->ID );
			$author->image  = get_field( 'immagine_dellautore', 'user_'.$author->ID );
			$author->userfoto = 'https://www.agrodolce.it/app/uploads/userphoto/'.get_field('userphoto_thumb_file', 'user_'.$author->ID);
		}
		//echo '<pre>';
		//print_r($sublist_authors);Exit;
		return $sublist_authors;
	}

	public function pagination() {
        return $this->custom_pagination;
    }
}

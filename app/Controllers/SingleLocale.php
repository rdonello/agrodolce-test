<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 17:29
 */

namespace App\Controllers;

use Sober\Controller\Controller;
use TbmCommon\Taxonomies\Sponsor;

class SingleLocale extends Controller {

	use Partials\RelatedTaxonomy;
	use Partials\CommonSingle;

	private $ids_to_exclude = [];
	private $primary_city_id = 0;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/single-ristorante--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;
	}


	public function related_cities_term() {
		$primary_term = agrodolce_get_primary_term( 'citta', get_the_ID() );

		$out = array();

		if ( $primary_term ) {
			$this->primary_city_id = $primary_term->term_id;
			$out['name']           = $primary_term->name;
			$out['url']            = get_term_link( $primary_term );
		}

		return $out;
	}

	public function related_cities_object() {

		$post_id = get_the_ID();

		$term_id = $this->primary_city_id ?: '';

		// Get the yoast primary term
		return tbm_get_related_chronological( $post_id, 'citta', $term_id, 'locale' );

	}

}

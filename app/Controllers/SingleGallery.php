<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 17/11/2018
 * Time: 17:59
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleGallery extends Controller {

	use Partials\RelatedTaxonomy;
	use Partials\CommonSingle;

	public $photo_array;
	public $page;
	public $nav = array();
	public $next_gallery = '';
	public $photos = array();

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/single-gallery--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;
	}

	public function photos_acf() {

		$photos = get_field( "tbm_gallery" );

		if ( ! $photos ) {
			return false;
		}

		$this->photos = $photos;

		// Save the public var
		$page              = ( get_query_var( 'img' ) ) ?: 1;
		$photo_id          = $page - 1;
		$this->photo_array = $this->photos[ $photo_id ];

		return $this->photos;

	}

	public function photos_urls() {

		if ( $this->photo_array ) {
			return false;
		}

		$photos = get_field( "tbm_gallery_urls" );

		if ( ! $photos ) {
			return false;
		}

		$this->photos = $photos[0];

		// Save the public var
		$page              = ( get_query_var( 'img' ) ) ?: 1;
		$photo_id          = $page - 1;
		$this->photo_array = $this->photos[ $photo_id ];

		return $this->photos;

	}

	public function photos_number() {

		return count( $this->photos );

	}

	public function photo_array() {

		return $this->photo_array;

	}

	public function is_slider() {
		if ( isset( $_GET['g'] ) && $_GET['g'] == 's' ) {
			return true;
		}

		return false;

	}

	public function paged() {

		$page       = ( get_query_var( 'img' ) ) ?: 1;
		$this->page = $page;

		return $page;

	}

	public function get_related_posts() {

		$articles = tbm_get_custom_related_posts( null, 1, array(), 'storia', 'post', 'object' );

		return $articles;

	}

	public function get_related_gallery_small() {

		$articles = tbm_get_custom_related_posts( null, 3, array(), 'storia', 'gallery', 'object' );

		return $articles;

	}

	public function get_related_videos() {

		$videos = tbm_get_custom_related_posts( null, 3, array(), 'storia', 'video', 'object' );

		return $videos;

	}

	public function show_captions() {
		return get_field( "tbm_gallery_show_captions" );
	}

	public function get_nav() {

		$prev = '';
		$next = '';

		$page_array = paginate_links( array(
			'base'    => get_permalink( get_the_ID() ) . "img/%#%",
			'format'  => '/%#%',
			'current' => $this->page,
			'total'   => count( $this->photos ),
			'type'    => 'array'
		) );

		$page_array = is_array( $page_array ) ? $page_array : array();

		if ( preg_match( '#href="([^"]+)"#', $page_array[0], $matches ) ) {
			$prev = isset( $matches[1] ) ? $matches[1] : '';
		}

		if ( preg_match( '#href="([^"]+)"#', $page_array[ count( $page_array ) - 1 ], $matches ) ) {
			$next = isset( $matches[1] ) ? $matches[1] : '';
		}

		if ( $this->page == 2 ) {
			$prev = get_permalink( get_the_ID() );
		}

		if ( (int) $this->page === count( $this->photos ) ) {
			$articles = tbm_get_custom_related_posts( get_the_ID(), 1, array(), 'trend', array(
				HTML_GALLERY_POST_TYPE
			), true );

			if ( $articles ) {
				$article = array_shift( $articles );
			}

			$next = get_permalink( $article->ID );
		}

		$nav       = array( 'prev' => $prev, 'next' => $next );
		$this->nav = $nav;

		return $nav;
	}

	public function modal_close_link() {

		return get_post_type_archive_link( HTML_GALLERY_POST_TYPE );
	}

}

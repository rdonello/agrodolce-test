<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 17:29
 */

namespace App\Controllers;

use Sober\Controller\Controller;
use TbmCommon\Taxonomies\Sponsor;

class Single extends Controller {

	use Partials\RelatedTaxonomy;
	use Partials\CommonSingle;

	/**
	 * Sponsor
	 *
	 * @return mixed
	 */
	public function sponsor() {
		$sponsor = new Sponsor();
		if ( $sponsor->name !== null || $sponsor->url !== null ) {
			return tbm_get_sponsor( $sponsor );
		}

		return '';
	}

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/single-post--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;
	}

}

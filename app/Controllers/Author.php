<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 04/07/2018
 * Time: 22:13
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class Author extends Controller {

	use Partials\Pagination;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {
		$css = tbm_critical_css( [
			'/dist/css/critical/author--critical.min.css',
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;

	}

	public function pagination() {

		$out = array(
			'custom' => true
		);

		return $out;

	}

	/**
	 * @return false|\WP_User
	 */

	public function author_data() {

		$defaults = array(
			'display_name' => 'Redattore HTML.it',
			'description'  => '',
			'user_email'   => 'redazione@html.it',
			'thumb'        => \App\asset_path( 'images/ico-author.svg' ),
			'twitter'      => 'Redattore HTML.it',
			'facebook'     => 'Redattore HTML.it'
		);

		global $authordata;
		$meta = array();

		$usermeta   = get_user_meta( $authordata->ID );
		$userdata   = get_userdata( $authordata->ID );
		$gravatar   = get_avatar_url( $authordata->ID );
		$thumb      = get_field( 'immagine_dellautore', 'user_' . $authordata->ID );
		$show_email = get_field( 'mostra_email', 'user_' . $authordata->ID );
		$image_url = isset( $thumb['ID'] ) && ! empty( $thumb['ID'] ) ? tbm_wp_get_attachment_image_url( $thumb['ID'], array(
			300,
			300
		) ) : $gravatar;

		$image_url_2x = isset( $thumb['ID'] ) && ! empty( $thumb['ID'] ) ? tbm_wp_get_attachment_image_url( $thumb['ID'], array(
			600,
			600
		) ) : $gravatar;


		// Array to return
		$meta['display_name'] = isset( $userdata->data->display_name ) ? $userdata->data->display_name : $defaults['display_name'];
		$meta['description']  = isset( $usermeta['description'][0] ) ? $usermeta['description'][0] : $defaults['description'];
		$meta['user_email']   = isset( $userdata->data->user_email ) && $show_email ? $userdata->data->user_email : $defaults['user_email'];
		$meta['thumb']        = $image_url ?: $defaults['thumb'];
		$meta['thumb_2x']     = $image_url_2x ?: $defaults['thumb'];
		$meta['twitter']      = isset( $usermeta['twitter'][0] ) ? $usermeta['twitter'][0] : $defaults['twitter'];
		$meta['facebook']     = isset( $usermeta['facebook'][0] ) ? $usermeta['facebook'][0] : $defaults['facebook'];

		return (object) $meta;

	}

}

<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 23/11/2018
 * Time: 19:24
 */

namespace App\Controllers\Partials;

trait RelatedTaxonomy {

	public function get_related_storie() {

		$terms = wp_get_object_terms( get_the_ID(), 'storia' );

		return $terms;

	}

	public function get_related_tags() {

		$terms = wp_get_object_terms( get_the_ID(), 'post_tag' );

		return $terms;

	}

}
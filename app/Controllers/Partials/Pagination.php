<?php

namespace App\Controllers\Partials;

trait Pagination {
	public function navigator_data() {
		$page          = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$max_num_pages = absint( $GLOBALS['wp_query']->max_num_pages );

		$is_paginable = $max_num_pages > 1 ? true : false;

		$out = array(
			'current'      => $page,
			'total'        => $max_num_pages,
			'is_paginable' => $is_paginable,
			'end_size'     => 0,
			'mid_size'     => 2,
			'type'         => 'list',
			'next_text'    => 'Successiva &rsaquo;',
			'prev_text'    => '&lsaquo; Precedente',
			'show_all'     => false,
		);

		return $out;
	}

}

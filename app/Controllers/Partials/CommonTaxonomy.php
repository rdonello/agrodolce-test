<?php

namespace App\Controllers\Partials;

trait CommonTaxonomy {

	public function share_link() {
		$url = get_term_link(get_queried_object_id());
		
		if ( empty( $url ) || is_wp_error( $url ) ) {
			return '';
		}

		return rawurlencode( $url );

	}

}

<?php

namespace App\Controllers\Partials;

trait CommonSingle {

	private $main_trend;
	private $post_trends;

	public function share_link() {
		$url = get_permalink( get_the_ID() );

		if ( empty( $url ) || is_wp_error( $url ) ) {
			return '';
		}

		return rawurlencode( $url );

	}

	/**
	 * Get category
	 */
	public function related_categories() {

		/**
		 * Get the trends tag
		 */
		$post_tags = wp_get_object_terms( get_the_ID(), array( 'category' ), array(
			'orderby' => 'count',
			'count'   => true,
			//'number'  => 3,
			'order'   => 'DESC'
		) );

		$output = [];

		if ( empty( $post_tags ) || is_wp_error( $post_tags ) ) {
			return $output;
		}

		foreach ( $post_tags as $post_tag ) {
			$output[] = [
				'link' => get_term_link( $post_tag ),
				'name' => $post_tag->name
			];
		}

		return $output;
	}

	/**
	 * Get trends
	 */
	public function related_trends() {

		/**
		 * Get the trends tag
		 */
		$post_trends = wp_get_object_terms( get_the_ID(), array( SITE_SPECIAL_TAXONOMY ), array(
			'orderby' => 'count',
			'count'   => true,
			'number'  => 3,
			'order'   => 'DESC'
		) );

		$output = [];

		if ( empty( $post_trends ) || is_wp_error( $post_trends ) ) {
			return $output;
		}

		$this->post_trends = wp_list_pluck( $post_trends, 'term_id' );

		foreach ( $post_trends as $post_trend ) {
			$output[] = [
				'link'        => get_term_link( $post_trend ),
				'description' => $post_trend->description,
				'name'        => $post_trend->name
			];
		}

		$this->main_trend = $output[0];

		return $output;
	}

	public function main_trend() {
		if ( $this->main_trend ) {
			return $this->main_trend;
		}

		return null;
	}

	public function get_breadcrumb() {
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			return yoast_breadcrumb( '', '', false );
		}

		return '';
	}

	/**
	 * Get Highlighted video
	 */

	public function print_highlight_video() {

		/**
		 * If TBM plugin is active and activated
		 */
		if ( function_exists( 'html_video_generate_video_player' ) ) {
			$video_id = get_field( "post-video-id", get_the_ID() );
			if ( ! empty( $video_id ) ) {
				$video = html_video_generate_brid_video_player( $video_id, 'single-video__embed', false );

				return $video;
			}
		}

		return '';

	}

	public function get_source() {
		$res = '';
		if ( ! empty( ad_get_post_source( get_the_ID() ) ) || ! empty ( ad_get_image_source( get_the_ID() ) ) ) {
			$res = '<div class="more-fields">';
			$res .= ad_get_post_source();
			$res .= ad_get_image_source();
			$res .= '</div>';
		}

		return $res;
	}

	public function image_credit() {
		$credit = tbm_get_thumb_credit( get_post_thumbnail_id() );
		$out    = '';

		if ( $credit ) {
			$out = sprintf( '<div class="credits"><p>Fonte immagine: %s</p></div>', $credit );
		}

		return $out;
	}
}

<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 16:00
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class TaxonomySpeciale extends Controller {

	use Partials\Pagination;
	use Partials\CommonTaxonomy;

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {

		if ( is_paged() ) {
			$css = tbm_critical_css( [
				'/dist/css/critical/archive--critical.min.css',
				'/dist/css/custom-critical.min.css'
			], '/dist/css/custom.min.css' );
		} else {
			$css = tbm_critical_css( [
				'/dist/css/critical/taxonomy-speciale--critical.min.css',
				'/dist/css/custom-critical.min.css'
			], '/dist/css/custom.min.css' );
		}
		return $css;

	}

	public $acf_tax_id;

	public $term_id;

	public function __construct() {
		$this->run();
	}

	protected function run() {
		$queried_object = get_queried_object();

		if ( $queried_object ) {
			$term_id       = $queried_object->term_id;
			$taxonomy      = $queried_object->taxonomy;
			$this->term_id = $term_id;

			$this->acf_tax_id = $taxonomy . '_' . $term_id;
		}
	}

	/**
	 * Get the attached img ID
	 *
	 * @return int
	 */
	public function term_image_id() {
		$image_id = get_field( 'tbm_speciale_thumbnail', $this->acf_tax_id );

		return $image_id;
	}

	/**
	 * Get Sponsor
	 */
	public function sponsor() {
		$sponsor      = [];
		$sponsor_name = get_field( 'tbm_speciale_sponsor', $this->acf_tax_id );
		if ( ! empty( $sponsor_name ) ) {
			$sponsor['name'] = $sponsor_name;
			$sponsor_link    = get_field( 'tbm_speciale_sponsor_link', $this->acf_tax_id );
			if ( ! empty( $sponsor_link ) ) {
				$sponsor['link'] = $sponsor_link;
			}
		}

		return $sponsor;
	}

	public function term_content() {
		$content = get_field( 'tbm_speciale_content', $this->acf_tax_id );

		if ( $content ) {
			return '';
		}

		return apply_filters( 'the_content', $content );
	}

	public function special_featured_post() {
		$number_of_posts = 2;
		$selected_posts  = get_field( 'tbm_speciale_post', $this->acf_tax_id );
		$not_in          = array();
		$selected_posts  = explode( ',', $selected_posts );
		if ( ( $selected_posts ) ) {
			$my_args = array(
				'post__in'       => (array) $selected_posts,
				'orderby'        => 'post__in',
				'posts_per_page' => $number_of_posts,
				'post_type'      => 'any'
			);

			$posts  = new \WP_Query( $my_args );
			$not_in = array_merge( $not_in, wp_list_pluck( $posts->posts, 'ID' ) );
			set_query_var( 'notin', $not_in );

			return $posts;
		}

		return false;
	}

	/**
	 * Set pagination defaults
	 *
	 * @return array
	 */
	public function pagination() {

		$out = array(
			'custom' => true,
			'link'   => get_term_link( get_queried_object_id() )
		);

		return $out;

	}

}

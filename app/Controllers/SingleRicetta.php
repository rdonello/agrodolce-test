<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 17:29
 */

namespace App\Controllers;

use Sober\Controller\Controller;
use TbmCommon\Taxonomies\Sponsor;

class SingleRicetta extends Controller {

	use Partials\RelatedTaxonomy;
	use Partials\CommonSingle;

	private $primary_ingredient_id = 0;

	/**
	 * Get ACF fields
	 *
	 * @var string[]
	 */
	protected $acf = [
		'tempo_preparazione',
		'calorie',
		'nome_ricetta',
		'difficolta',
		'ricette-ingrediente-persone',
		'ricette-preparazione',
		'ricette-conclusione',
		'ricette-variante'
	];

	/**
	 * Sponsor
	 *
	 * @return mixed
	 */
	public function sponsor() {
		$sponsor = new Sponsor();
		if ( $sponsor->name !== null || $sponsor->url !== null ) {
			return tbm_get_sponsor( $sponsor );
		}
	}

	/**
	 * Get critical code and return it
	 */
	public function tbm_critical() {

		// If post has video, generate video critical css
		$post_critical = get_field( "post-video-id", get_the_ID() ) ? '/dist/css/critical/single-videoricetta--critical.min.css' : '/dist/css/critical/single-ricetta--critical.min.css';

		$css = tbm_critical_css( [
			$post_critical,
			'/dist/css/custom-critical.min.css'
		], '/dist/css/custom.min.css' );

		return $css;
	}

	public function related_recipes_term() {
		$primary_term = agrodolce_get_primary_term( 'ingredienti', get_the_ID() );

		$out = array();

		if ( $primary_term ) {
			$this->primary_ingredient_id = $primary_term->term_id;
			$out['name']                 = $primary_term->name;
			$out['url']                  = get_term_link( $primary_term );
		}

		return $out;
	}

	public function related_recipes_object() {

		$post_id = get_the_ID();

		$term_id = $this->primary_ingredient_id ?: '';

		// Get the yoast primary term
		return tbm_get_related_chronological( $post_id, 'ingredienti', $term_id, 'ricetta' );

	}

	public function get_ingredients() {

		$post_id = get_the_ID();

		$ingredients = get_field( 'ingredienti-repeater', $post_id );

		// Declare final results array2
		$results = array();

		foreach ( $ingredients as $ingredient ) {

			$term = get_term( (int) $ingredient['ingredienti-nome'], 'ingredienti' );

			if ( ! $term || is_wp_error( $term ) ) {
				continue;
			}

			$out['nome']     = $term->name;
			$out['url']      = get_term_link( $term );
			$out['spec']     = $ingredient['ingredienti-spec'];
			$out['quantita'] = $ingredient['ingredienti-quantita'];
			$out['quantita'] = str_replace( '1/2', '½', $out['quantita'] );
			$out['unita']    = agrodolce_get_ingredienti_plural( $ingredient['ingredienti-unita'], (int) $ingredient['ingredienti-quantita'] );

			// Push array in main array
			$results[] = $out;

		}

		return $results;
	}

	public function get_products_list() {
		$product_list_id = get_field( 'tbm_related_products', get_the_ID() );

		$products = array();

		if ( empty( $product_list_id ) ) {
			return array();
		}

		foreach ( $product_list_id as $post_id ) {
			$out['seller']       = 'Amazon';
			$out['name']         = get_post_field( 'post_title', $post_id );
			$out['price']        = get_field( 'tbm_amazon_price', $post_id );
			$out['link']         = get_field( 'tbm_amazon_link', $post_id );
			$out['featured_url'] = get_the_post_thumbnail_url( $post_id );

			// Push the products in final array
			$products[] = $out;
		}

		return $products;
	}

}

<?php
/**
 * Theme assets
 */

use function App\asset_path;

/**
 * Add and remove
 */
add_action( 'wp_enqueue_scripts', function () {

	if ( isset( $_GET['critical'] ) && $_GET['critical'] === '1' ) {
		return '';
	}

	$ver = wp_get_theme()->get( 'Version' );

	/**
	 * Dequeue
	 */
	wp_dequeue_style( 'wp-block-library' );

	/**
	 * Enqueue our jQuery (only in frontend)
	 */
	if ( ! is_user_logged_in() ) {
		wp_deregister_script( 'jquery' );
	}
	wp_enqueue_script( 'tbm', asset_path( 'js/tbm.min.js' ), array(), $ver, true );
	wp_localize_script( 'tbm', 'tbm', array(
		'ajaxurl'     => admin_url( 'admin-ajax.php' ),
		'photolabels' => array( "Foto ", " di " )
	) );

} );

<?php

use DOMWrap\Document;

add_filter( 'tbm_dashboard_post_types_admin', function ( $enabled_post_types ) {

	unset( $enabled_post_types['autori'] );
	unset( $enabled_post_types['playlist-video'] );
	unset( $enabled_post_types['sponsor'] );

	return $enabled_post_types;
} );

/**
 * Add drafts to related post object
 */
add_filter( 'acf/fields/post_object/query/name=tbm_gallery_to_post', function ( $args ) {
	$args['post_status'] = array( 'pending', 'draft', 'future', 'publish' );

	return $args;
}, 10, 3 );

/**
 * Filter the Preparazione ricette custom field to adjust markup and add lazyloading
 */
add_filter( 'acf/load_value/name=ricette-preparazione', function ( $value ) {

	if ( is_admin() ) {
		return $value;
	}

	// Apply shortcode
	$content = do_shortcode( $value );

	try {
		$doc = new Document();
		// Disable adding doctype and opening log
		$doc->setLibxmlOptions( LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
		// Load html
		$doc->html( $content );

		/**
		 * Rewrite images with Thumbor
		 */
		$images = $doc->find( 'img' );

		// If images were found
		if ( $images->count() > 0 ) {
			$images->each( function ( $node ) {
				$src         = $node->attr( 'src' );
				$thumbor_src = tbm_get_thumbor_img( $src, 640 );
				$node->attr( 'data-src', $thumbor_src );
				$node->removeAttr( 'src', '' );
				$node->attr( 'width', '640' );
				$node->attr( 'height', '427' );
				$node->addClass( 'lazyload' );
			} );
		}

		/**
		 * Add <p> in <li>. Move the image after <li>
		 */
		$elements = $doc->find( 'li' );

		if ( $elements->count() > 0 ) {
			$elements->each( function ( $node ) use ( &$i ) {
				$i ++;
				$images = $node->find( 'img' )->detach();
				$node->wrapInner( '<p/>' );
				$node->follow( $images );
				$node->addClass( 'steps__item' );
				$node->attr( 'id', 'step-' . $i );
			} );

			$elements->each( function ( $node ) use ( &$i ) {
				$i ++;
				$divs = $node->find( 'div.partial-innerrelated-playervideo--inside' )->detach();
				$node->follow( $divs );
			} );
		}

		$content = $doc->html();

		return tbm_minify_html( $content );
	} catch ( Exception $e ) {
		tbm_write_log( 'error in acf/load_value/name=ricette-preparazione', array( get_the_ID() ) );
	}

	return do_shortcode( $value );
} );

/**
 * Filter the Content list custom field to adjust markup and add lazyloading
 */
add_filter( 'acf/load_value/name=content-list', function ( $value ) {

	if ( is_admin() ) {
		return $value;
	}

	try {
		$doc = new Document();
		// Disable adding doctype and opening log
		$doc->setLibxmlOptions( LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
		// Load html
		$doc->html( $value );

		// get list type (ASC|2/DESC|3/UNORDERED|1)
		$type_list = get_field( 'type-list' );

		switch ( $type_list ) {
			//UNORDERED
			case 1 :
				$ol = $doc->find( 'ol' );
				$ol->removeAttr( 'class' );
				$ol->addClass( 'unordered' );
				break;
			//DESC (4,3,2,1)
			case 3:
				$doc->find( 'ol' )->addClass( 'ranking-list reversed' );
				$items_li = $doc->find( 'li' );
				$i        = $items_li->count();
				$doc->find( 'ol' )->attr( 'style', 'counter-reset:rank ' . (string) ( $i + 1 ) );
				$items_li->each( function ( $node ) use ( &$i ) {
					$node->removeAttr( 'class' );
					$node->addClass( 'ranking-item position-' . $i );
					$i --;
				} );
				break;
			//ASC (1, 2, 3, 4)
			default:
				break;
		}


		/**
		 * Rewrite images with Thumbor
		 */
		$images = $doc->find( 'img' );

		// If images were found
		if ( $images->count() > 0 ) {
			$images->each( function ( $node ) {
				$src         = $node->attr( 'src' );
				$thumbor_src = tbm_get_thumbor_img( $src, 640 );
				if ( tbm_is_amp() ) {
					$node->attr( 'src', $thumbor_src );
				} else {
					$node->attr( 'data-src', $thumbor_src );
					$node->removeAttr( 'src' );
				}
				$node->attr( 'width', '640' );
				$node->attr( 'height', '427' );
				$node->addClass( 'lazyload' );
			} );
		}

		// Convert dom to string
		$content = $doc->html();

		return $content;

	} catch ( Exception $e ) {
		tbm_write_log( 'error in acf/load_value/name=ricette-preparazione', array( get_the_ID() ) );
	}

	return $value;
} );

<?php

use Gridonic\JsonResponse\ErrorJsonResponse;
use Gridonic\JsonResponse\SuccessJsonResponse;
use PostTypes\PostType;
use PostTypes\Taxonomy;
use function App\sage;

/**
 * Delete Trend created by users
 */
add_action( 'create_term', function ( $term_id, $tt_id, $taxonomy ) {
	if (! current_user_can( 'wpseo_manage_options' ) && ! current_user_can( 'activate_plugins' ) ) {
		if ( $taxonomy === SITE_SPECIAL_TAXONOMY || $taxonomy === 'category' ) {
			tbm_enable_error_message( sprintf( '<ul style="list-style: inherit;padding: 0 20px;"><li>' . __( 'Non hai permessi per creare un termine nella tassonomia <strong>%s</strong>. Il termine è stato cancellato', 'agrodolce' ) . '.</li></ul>', get_taxonomy( $taxonomy )->name ) );
			wp_delete_term( $term_id, $taxonomy );
		}
	}
}, 10, 3 );

/**
 * Change login page background
 */
add_action( 'login_enqueue_scripts', function () {
	?>
	<style>
		body {
			background: #2d0008d9 !important;
		}

		.login #backtoblog a, .login #nav a {
			color: white !important;
		}

		.login form {
			box-shadow: 0 1px 3px rgb(0 0 0 / 4%) !important;

		}

	</style>
<?php } );

/**
 * Add feed in category and tags
 */
add_action( 'wp_head', function () {
	if ( is_category() ) {
		$term = get_queried_object();

		if ( $term ) {
			$title = sprintf( __( '%1$s %2$s %3$s Category Feed' ), get_bloginfo( 'name' ), _x( '&raquo;', 'feed link' ), $term->name );
			$href  = get_category_feed_link( $term->term_id );
		}
	} elseif ( is_tax( SITE_SPECIAL_TAXONOMY ) ) {
		$term = get_queried_object();

		if ( $term ) {
			$tax   = get_taxonomy( $term->taxonomy );
			$title = sprintf( __( '%1$s %2$s %3$s %4$s Feed' ), get_bloginfo( 'name' ), _x( '&raquo;', 'feed link' ), $term->name, $tax->labels->singular_name );
			$href  = get_term_feed_link( $term->term_id, $term->taxonomy );
		}
	}
	if ( isset( $title ) && isset( $href ) ) {
		echo '<link rel="alternate" type="' . feed_content_type() . '" title="' . esc_attr( $title ) . '" href="' . esc_url( $href ) . '" />' . "\n";
	}
} );

/**
 * Remove unused box from all sites
 */
add_action( 'add_meta_boxes', function () {
	remove_meta_box( 'trackbacksdiv', get_post_types(), 'normal' ); // Trackbacks meta box
	remove_meta_box( 'postcustom', 'post', 'normal' ); // Custom fields meta box
	remove_meta_box( 'commentsdiv', 'post', 'normal' ); // Comments meta box
	remove_meta_box( 'formatdiv', 'post', 'normal' ); // Post format meta box
	remove_meta_box( 'commentstatusdiv', 'post', 'normal' ); // Comment status meta box

	remove_meta_box( 'gdrts-metabox', 'post', 'normal' ); // GD Star Rating
}, 11 );

/**
 * Set content width
 */
add_action( 'after_setup_theme', function () {
	$GLOBALS['content_width'] = apply_filters( 'motori_content_width', 640 );
}, 0 );

/**
 * Clone Ingredienti custom field in Ingredienti taxonomy
 */
add_action( 'acf/save_post', function ( $post_id ) {

	// Act only in recipes and cocktails
	if ( ! in_array( get_post_type( $post_id ), array( 'ricetta', 'cocktail' ) ) ) {
		return;
	}

	// If there's no ingredients, remove all terms from ingredienti taxonomy
	if ( empty( $_POST['acf']['field_5c69af756ef59'] ) ) {
		wp_set_object_terms( $post_id, '', 'ingredienti' );

		return;
	}

	// Declare variables
	$rows = $_POST['acf']['field_5c69af756ef59'];
	$out  = array();

	// Loop the variables
	foreach ( $rows as $row ) {
		if ( isset( $row['field_5c69afd16ef5a'] ) && ! empty( $row['field_5c69afd16ef5a'] ) ) {
			$out[] = (int) $row['field_5c69afd16ef5a'];
		}
	}

	// Save the terms in the ingredienti taxonomy
	$result = wp_set_object_terms( $post_id, $out, 'ingredienti' );

	if ( $result && ! is_wp_error( $result ) ) {
		return true;
	}

	return false;

}, 5 );

/**
 * Register page endpoint
 */
add_action( 'init', function () {
	add_rewrite_endpoint( 'rp', EP_PERMALINK );
} );

/**
 * Register page endpoint for letter pagination
 */
add_action( 'init', function () {
	add_rewrite_endpoint( 'letter', EP_PAGES );
} );

/**
 * Remove menu post type from not admin not seo users
 */
add_action( 'admin_menu', function () {
	if ( ! current_user_can( 'wpseo_manage_options' ) && ! current_user_can( 'activate_plugins' ) ) {
		remove_menu_page( 'edit.php?post_type=menu' );
	}
} );

/**
 * Print concatenated CSS in footer
 */
add_action( 'wp_footer', function () {
	// Get the css list
	$css_raw = get_query_var( 'tbm_css_queue', array() );

	// Get the theme path to rewrite the css
	$theme_path = get_theme_file_path();

	// Rewrite the css path queue
	$css = array_map( function ( $val ) use ( $theme_path ) {
		return $theme_path . '/dist/' . $val;
	}, $css_raw );

	// apply the filter to check other css
	$csses = apply_filters( 'tbm_css_array', $css );

	// Get the concatenated csses
	$rel_link = tbm_inline_files( $csses );

	// Return
	if ( $rel_link ) {
		echo $rel_link;

		return '';
	}

	// If concatenation fails, print all the css one by one
	$ver   = wp_get_theme()->get( 'Version' );
	$link  = '<link rel="stylesheet" href="%s" media="print" onload="this.media=\'all\'">';
	$csses = array_unique( $css_raw );
	foreach ( $csses as $css ) {
		$url = sage( 'assets' )->getUri( $css );
		echo sprintf( $link, add_query_arg( 'ver', $ver, $url ) );
	}


} );

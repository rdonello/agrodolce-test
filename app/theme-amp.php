<?php
add_filter( 'amp_post_template_data', function ( $data ) {
	$data['font_urls'] = [ 'titillum' => 'https://fonts.googleapis.com/css2?family=Titillium+Web:wght@400;600;700&display=swap' ];

	return $data;
} );

/**
 * Recipe preparation
 */
function tbm_recipe_preparation() {

	$tempo_preparazione = get_field( "tempo_preparazione", get_the_ID() );
	if ( isset( $tempo_preparazione ) && ! empty( $tempo_preparazione ) ) {
		$tempo_preparazione = (int) $tempo_preparazione;
		if ( $tempo_preparazione <= 90 ) {
			$tempo_preparazione = $tempo_preparazione . " minuti";
		} else {
			$ore                = floor( $tempo_preparazione / 60 );
			$minuti             = $tempo_preparazione - ( $ore * 60 );
			$tempo_preparazione = $ore . "h";
			if ( $minuti > 0 ) {
				$tempo_preparazione .= " " . $minuti . "'";
			} else {
				$tempo_preparazione = $ore . " ore";
			}
		}
		$tempo_preparazione = "<li>
									<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" viewbox=\"0 0 20 24\">
										<g transform=\"translate(-2)\"><path d=\"M13,4.051V2h3a1,1,0,0,0,0-2H8A1,1,0,0,0,8,2h3V4.051A10.013,10.013,0,0,0,2,14v9a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V14a10.013,10.013,0,0,0-9-9.949ZM12,6a8,8,0,1,1-8,8,8,8,0,0,1,8-8Z\" fill=\"#ff007b\"/><rect width=\"7\" height=\"2\" transform=\"translate(11 13)\" fill=\"#ff007b\"/></g>
									</svg>
									<span>$tempo_preparazione</span>
								</li>";
	} else {
		$tempo_preparazione = "";
	}

	$calorie = get_field( "calorie", get_the_ID() );
	if ( isset( $calorie ) && ! empty( $calorie ) && $calorie > 0 ) {
		$calorie = "<li>
						<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" viewbox=\"0 0 28 24\">
							<g transform=\"translate(-0.6 -2)\"><rect width=\"4\" height=\"24\" rx=\"1\" transform=\"translate(0.6 2)\" fill=\"#ff007b\"/><rect width=\"4\" height=\"18\" rx=\"1\" transform=\"translate(6.6 8)\" fill=\"#ff007b\"/><rect width=\"4\" height=\"18\" rx=\"1\" transform=\"translate(18.6 8)\" fill=\"#ff007b\"/><rect width=\"4\" height=\"12\" rx=\"1\" transform=\"translate(12.6 14)\" fill=\"#ff007b\"/><rect width=\"4\" height=\"12\" rx=\"1\" transform=\"translate(24.6 14)\" fill=\"#ff007b\"/></g>
						</svg>
						<span>$calorie cal x 100g</span>
					</li>";
	} else {
		$calorie = "";
	}

	$difficolta = get_field( "difficolta", get_the_ID() );
	if ( isset( $difficolta ) && ! empty( $difficolta ) ) {
		$difficolta = "<li>
							<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" viewbox=\"0 0 24 24\"><path d=\"M13,21H5v2a.945.945,0,0,0,1,1H18a.945.945,0,0,0,1-1V21Z\" fill=\"#ff007b\"/><path d=\"M18,4h-.3A5.99,5.99,0,0,0,12,0,6.087,6.087,0,0,0,6.3,4H6A5.991,5.991,0,0,0,5,15.9V19h6V15h2v4h6V15.9A5.991,5.991,0,0,0,18,4Z\" fill=\"#ff007b\"/></svg>
							<span>Difficoltà $difficolta</span>
						</li>";
	}

	$result = '<div class="preparation__content">
		<div class="preparation__heading">
			<ul>' .
			  $tempo_preparazione .
			  $calorie .
			  $difficolta .
			  '</ul>
		</div></div>';

	echo tbm_amp_sanitize_content( $result );
}

// action called in tbm-common/templates/single-amp.php
add_action( 'tbm_amp_post_recipe_preparation', 'tbm_recipe_preparation' );


/**
 * Recipe Ingredients
 *
 * @return string
 */
function tbm_recipe_ingredients() {
	$ingredienti = array();

	if ( ! have_rows( 'ingredienti-repeater' ) ) {
		return '';
	}

	// Loop ingredienti
	while ( have_rows( 'ingredienti-repeater' ) ) {
		the_row();

		// Variabili
		$unita_ingrediente = '';
		$quantita          = get_sub_field( 'ingredienti-quantita' );
		$quantita          = str_replace( '1/2', '½', $quantita );
		$unita             = get_sub_field( 'ingredienti-unita' );
		$template          = '<li class="ingredients__item">%s</li>';
		$unita_ingrediente = agrodolce_get_ingredienti_plural( $unita, (int) $quantita );

		// Nomi e link
		$ingrediente_name = '';
		$term_id_nome     = get_sub_field( 'ingredienti-nome' );
		if ( null !== $term_id_nome ) {
			$ingr             = get_term_by( 'id', (int) $term_id_nome, 'ingredienti' );
			$ingrediente_name = $ingr->name;

			$link = "#";
			if ( isset( $ingr ) && ! empty( $ingr ) ) {
				if ( ( $ingr->count >= 5 ) || ! empty( $ingr->description ) ) {
					$link = get_term_link( $ingr );
				}
			}
		}

		// Specifica
		$specifica = ! empty( get_sub_field( 'ingredienti-spec' ) ) ? " " . stripslashes( get_sub_field( 'ingredienti-spec' ) ) : '';

		// Unità
		if ( ( isset( $unita ) && ( null !== $unita ) && ( $unita == "q.b." ) ) || ! isset( $quantita ) || empty( $quantita ) ) {
			$valore = "quanto basta";
		} else {
			$valore = $quantita . " " . $unita_ingrediente;
		}

		// Crea linea ingrediente
		$ingrediente = sprintf( '<a href="%s">%s</a> %s %s', $link, $ingrediente_name, $specifica, $valore );

		$ingredienti[] = sprintf( $template, $ingrediente );

	}

	// Ricette persone
	$ricette_ingrediente_persone = get_field( 'ricette-ingrediente-persone', get_the_ID() );
	if ( ! isset( $ricette_ingrediente_persone ) || empty( $ricette_ingrediente_persone ) || ! is_numeric( $ricette_ingrediente_persone ) ) {
		$ricette_ingrediente_persone = 4;
	}

	// Print ingredienti
	if ( count( $ingredienti ) > 1 ) {
		$content = '<div class="ingredients__content">
		<h2>Ingredienti per ' . $ricette_ingrediente_persone . ' persone</h2>
		<ul class="ingredients__list">' . implode( '', $ingredienti ) . '</ul>
		</div>';

		echo tbm_amp_sanitize_content( $content );
	}

}

// action called in tbm-common/templates/single-amp.php
add_action( 'tbm_amp_post_recipe_ingredients', 'tbm_recipe_ingredients' );


/**
 * Recipe footer
 */
function tbm_recipe_footer() {
	ob_start();

	// NOME RICETTA
	$post_title   = get_field( 'nome_ricetta' ) ?: get_the_title( get_the_ID() );
	$conclusione  = '';
	$preparazione = '';
	$variante     = '';


	// RICETTA PREPARAZIONE
	$preparazione = trim( get_field( 'ricette-preparazione' ) );
	if ( ! empty( $preparazione ) ) {
		?>
		<div class="single-post__content">
			<h2>Preparazione <?php echo $post_title; ?></h2>
			<?php
			$preparazione = do_shortcode( $preparazione );
			echo tbm_amp_sanitize_content( $preparazione );
			?>
		</div>
		<?php
	}

	// RICETTA CONCLUSIONE
	$conclusione = trim( get_field( 'ricette-conclusione' ) );
	if ( ! empty( $conclusione ) ) {
		?>
		<div class="single-post__content">
			<?php
			$conclusione = do_shortcode( $conclusione );
			echo tbm_amp_sanitize_content( $conclusione );
			?>
		</div>
		<?php
	}

	// RICETTA VARIANTE
	$variante = trim( get_field( 'ricette-variante' ) );
	if ( ! empty( $variante ) ) {
		?>
		<div class="single-post__content">
			<?php
			$variante = do_shortcode( $variante );
			echo tbm_amp_sanitize_content( $variante );
			?>
		</div>
		<?php
	}

	$fonte          = ad_get_post_source( get_the_ID() );
	$fonte_immagini = ad_get_image_source( get_the_ID() );
	if ( ! empty( $fonte ) || ! empty ( $fonte_immagini ) ) {
		echo '<div class="more-fields">';
		echo $fonte;
		echo $fonte_immagini;
		echo '</div>';
	}
}

// action called in tbm-common/templates/single-amp.php
add_action( 'tbm_amp_post_recipe_footer', 'tbm_recipe_footer' );


/** filter to generate amp page footer
 * called in tbm-common/templates/partials/amp/footer.php
 */
add_filter( 'tbm_amp_footer', function () {

	$logo          = tbm_wp_get_attachment_image_url( get_field( 'tbm_site_amp_logo', 'options' ), array( 0, 58 ) );
	$theme_version = wp_get_theme()->get( 'Version' );
	$newsletter    = get_page_link( get_page_by_path( 'newsletter' ) );

	$footer = '
		<div class="partial-amp-footer">
			<div class="footer__list">
				<ul>
					<li><a href="https://www.agrodolce.it/contatti/" rel="nofollow">Redazione e Contatti</a></li>
					<li><a href="https://www.agrodolce.it/mappa-del-sito/">Mappa del sito</a></li>
					<li><a href="https://www.agrodolce.it/notifiche-editoriali/" rel="nofollow">Notifiche editoriali</a></li>
					<li><a href="mailto:redazione@agrodolce.it">Collabora con noi</a></li>
					<li><a href="https://www.agrodolce.it/note-legali/" rel="nofollow">Note legali e copyright</a></li>
					<li><a href="https://www.agrodolce.it/policy-contatti/" rel="nofollow">Policy Contatti</a></li>
					<li><a href="https://www.agrodolce.it/cookie-policy/" rel="nofollow">Cookie Policy</a></li>
				</ul>
			</div>

			<div class="partial-cta-newsletter">
				<h3>Rimani sempre aggiornato</h3>
				<a class="cta--newsletter" href="' . $newsletter . '">Iscriviti alla newsletter</a>
			</div>

			<a class="logo" href="/">
				<amp-img src="' . $logo . '" layout="fill" alt="https://www.agrodolce.it" noloading></amp-img>
			</a>

			<p class="credits">Agrodolce &egrave; un supplemento di <a href="https://www.blogo.it" target="_blank"
																	   rel="nofollow noopener">Blogo</a>. <br>
				Blogo è una testata giornalistica registrata. Registrazione ROC n. 22649<br>
				&copy; Agrodolce 2013-' . date( "Y" ) . ' | T-Mediahouse – P. IVA 06933670967 | ' . $theme_version . '</p>
		</div>';

	return $footer;
} );

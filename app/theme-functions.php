<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package pmi
 */

use function App\asset_path;
use DOMWrap\Document;

/**
 * Get the date of the post
 *
 * @param null $post
 *
 * @return string
 */
function agrodolce_snippet_post_date( $post = null ) {

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	if ( get_field( 'tbm_hide_date', $post->ID ) ) {
		return '';
	}

	$lang = function_exists( 'pll_current_language' ) ? pll_current_language() : 'it';

	$html = '<label class="post__date post__date--' . $lang . '"><time class="entry-date published" datetime="%s">%s</time></label>';
	$date = get_the_date( get_option( 'date_format' ), $post->ID );

	/**
	 * Lovercase date
	 */
	$date = strtolower( $date );

	return sprintf( $html, get_the_date( 'c', $post ), $date );
}

/**
 * Return the label for cards (speciale, tag taxonomy or category). To use in the loop
 *
 * @param string $class optional Class to add to the returned anchor tag. Default none
 * @param string $type optional Type of the link: name, link, id, url. Default link
 * @param int $limit optional Maximun numbers of items to return. Default 1
 * @param int|object $post Post id or Wp Post Object
 * @param string
 *
 * @return bool|string
 */
function agrodolce_tbm_get_label( $class = '', $type = 'link', $limit = 1, $post = 0, $taxonomy = '' ) {
	$post  = get_post( $post );
	$terms = '';

	if ( empty( $post ) ) {
		return '';
	}

	$out = array();

	// Set the taxonomies to search in
	$taxonomies = array(
		$taxonomy,
		SITE_SPECIAL_TAXONOMY,
		'category',
		'categoria_ricetta',
		'categoria_cocktail',
		'rubrica',
		'filters',
		'post_tag'
	);

	foreach ( $taxonomies as $taxonomy ) {

		if ( function_exists( 'yoast_get_primary_term' ) && is_taxonomy_hierarchical( $taxonomy ) ) {
			// Check primary term in yoast
			$term_id = yoast_get_primary_term_id( $taxonomy, $post );

			if ( $term_id ) {
				$terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'include' => $term_id ) );
			}

			if ( is_wp_error( $terms ) || empty( $terms ) ) {
				$terms = wp_get_object_terms( $post->ID, $taxonomy );
			}
		} else {
			$terms = wp_get_object_terms( $post->ID, $taxonomy );
		}

		if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {
			break;
		}

	}

	foreach ( $terms as $term ) {
		if ( $type === 'link' ) :
			$out[] = '<a class="' . $class . '" title="' . $term->name . '" href ="' . get_term_link( $term->term_id ) . '" > ' . $term->name . '</a >';
		elseif ( $type === 'name' ) :
			$out[] = '<span class="' . $class . '">' . $term->name . '</span>';
		elseif ( $type === 'url' ) :
			$out[] = get_term_link( $term );
		elseif ( $type === 'id' ) :
			$out[] = (int) $term->term_id;
		endif;
	}

	return join( ' ', array_slice( $out, 0, $limit ) );


	return false;
}

/**
 * Get the HTML snippet from the sponsor term
 *
 * @param $term
 *
 * @return string
 */
function agrodolce_sponsor_badge( $term ) {
	$term = get_term( $term );

	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	/**
	 * Get the sponsor data
	 */
	$sponsor_array = tbm_get_sponsor( $term );

	if ( empty( $sponsor_array ) ) {
		return '';
	}

	/**
	 * Get the html
	 */
	$html = agrodolce_html_sponsor_badge( $sponsor_array );

	if ( ! $html ) {
		return '';
	}

	return $html;
}

/**
 * Get the HTML snippet from the sponsored (not sponsor) term
 *
 * @param $sponsor WP Term object relative to the sponsor term
 *
 * @return string
 */
function agrodolce_snippet_tax_sponsor_badge( $term ) {

	$term = get_term( $term );

	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	$id = $term->taxonomy . '_' . $term->term_id;

	/**
	 * Get the sponsor term
	 */
	$sponsor_term = get_field( 'post_sponsor', $id );

	if ( ! $sponsor_term ) {
		return '';
	}

	/**
	 * Get the sponsor data
	 */
	$sponsor_array = tbm_get_sponsor( $sponsor_term );

	if ( empty( $sponsor_array ) ) {
		return '';
	}

	/**
	 * Get the html
	 */
	$html = agrodolce_html_sponsor_badge( $sponsor_array );

	if ( ! $html ) {
		return '';
	}

	return $html;

}

function agrodolce_snippet_post_sponsor_badge( $post = null ) {

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$id = $post->ID;

	/**
	 * Get the sponsor term
	 */
	$sponsor_term = get_field( 'post_sponsor', $id );

	if ( ! $sponsor_term ) {
		return '';
	}

	/**
	 * Get the sponsor data
	 */
	$sponsor_array = tbm_get_sponsor( $sponsor_term );

	if ( empty( $sponsor_array ) ) {
		return '';
	}

	/**
	 * Get the html
	 */
	$html = agrodolce_html_sponsor_badge( $sponsor_array );

	if ( ! $html ) {
		return '';
	}

	return $html;

}

function agrodolce_html_sponsor_badge( $sponsor_array ) {

	$name  = '';
	$image = '';

	if ( empty( $sponsor_array['name'] ) && empty( $sponsor_array['image'] ) ) {
		return '';
	}

	if ( ! empty( $sponsor_array['imageId'] ) ) {
		$image_url    = tbm_wp_get_attachment_image_url( $sponsor_array['imageId'], array( 70, 70 ) );
		$image_url_2x = tbm_wp_get_attachment_image_url( $sponsor_array['imageId'], array( 140, 140 ) );
		if ( ! empty( $sponsor_array['url'] ) ) {
			$title = empty( $sponsor_array['title'] ) ? '' : $sponsor_array['title'];
			$image = '<a href="' . $sponsor_array['url'] . '" target="_blank" rel="nofollow sponsored"><div class="partner__logo"><figure><img class="lazyload" data-srcset="' . $image_url . ', ' . $image_url_2x . ' 2x" alt="Logo ' . $title . '" title="' . $title . '" /></figure></div></a>';
		} else {
			$image = '<div class="partner__logo"><figure><img class="lazyload" data-srcset="' . $image_url . ', ' . $image_url_2x . ' 2x" alt="Logo ' . $sponsor_array['title'] . '" title="' . $sponsor_array['title'] . '"/></figure></div>';
		}
	}


	if ( isset( $sponsor_array['type'] ) && $sponsor_array['type'] === 'tbm_sponsor_partner' ) {
		if ( function_exists( 'pll__' ) ) {
			$html = "<div class=\"partner\"><span>" . pll__( 'a cura di', 'agrodolce' ) . " %s</span>%s</div>";
		} else {
			$html = "<div class=\"partner\"><span>" . __( 'a cura di', 'agrodolce' ) . " %s</span>%s</div>";
		}

	} else {
		if ( function_exists( 'pll__' ) ) {
			$html = "<div class=\"partner\"><span>" . pll__( 'sponsorizzato da', 'agrodolce' ) . " %s</span>%s</div>";
		} else {
			$html = "<div class=\"partner\"><span>" . __( 'sponsorizzato da', 'agrodolce' ) . " %s</span>%s</div>";
		}
	}

	return sprintf( $html, $name, $image );
}

/**
 * The taxonomy this term is part of.
 *
 * @param string $taxonomy_name Taxonomy name for the term.
 * @param int $post_id Post ID for the term.
 */
function agrodolce_get_primary_term( $taxonomy_name = 'category', $post ) {

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return false;
	}

	$post_id = $post->ID;

	$terms = get_the_terms( $post_id, $taxonomy_name );

	// Check if post has a tax term assigned.
	if ( ! $terms || is_wp_error( $terms ) ) {
		return false;
	}

	if ( ! class_exists( 'WPSEO_Primary_Term' ) ) {
		return reset( $terms );
	}


	// Show the post's 'Primary' term.
	// Check that the feature is available and that a primary term is set.
	$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy_name, $post_id );
	$wpseo_primary_term = $wpseo_primary_term->get_primary_term();

	// Set the term object.
	$primary_term = get_term( $wpseo_primary_term );

	if ( ! $primary_term || is_wp_error( $primary_term ) ) {
		$primary_term = reset( $terms );
	}

	return $primary_term;

}

/**
 * Get Budget by field id
 */
function agrodolce_get_locale_budget( $post_id ) {

	$budget = get_field( 'spesa', $post_id );

	if ( ! $budget ) {
		return '';
	}

	switch ( $budget ) {
		case "1":
			$budget = "entro i 30€";
			break;
		case "2":
			$budget = "entro i 50€";
			break;
		case "3":
			$budget = "oltre i 50€";
			break;
		default:
			$budget = '';
			break;
	}

	return $budget;
}

/**
 * Get all custom values from `locale` and format in a standard array
 *
 * @param $post_id The post ID
 *
 * @return array
 */
function get_ristorante_data_object( $post_id ) {

	// Define default values
	$defaults = array(
		'address_complete' => '',
		'address'          => '',
		'city'             => '',
		'city_link'        => '',
		'city_maps'        => '',
		'address_link'     => '',
		'recapiti'         => '',
		'chiusura'         => '',
		'budget'           => '',
		'chef'             => '',
		'gallery'          => array()
	);

	// Get data from ACF fields
	$arr_indirizzo = get_field( 'indirizzo', $post_id );
	$arr_recapiti  = get_field( 'recapiti', $post_id );
	$chiusura      = get_field( 'chiusura', $post_id );
	$budget        = agrodolce_get_locale_budget( $post_id );
	$gallery       = get_field( 'gallery_locale', $post_id );
	$term          = agrodolce_get_primary_term( 'citta', $post_id );
	$chef          = get_field( 'chef', $post_id );

	// Address
	if ( $arr_indirizzo ) {
		$out['address']          = isset( $arr_indirizzo['name'] ) && ! empty( $arr_indirizzo['name'] ) ? $arr_indirizzo['name'] : '';
		$out['address_complete'] = isset( $arr_indirizzo['address'] ) && ! empty( $arr_indirizzo['address'] ) ? $arr_indirizzo['address'] : '';
		$out['city_maps']        = isset( $arr_indirizzo['city'] ) && ! empty( $arr_indirizzo['city'] ) ? $arr_indirizzo['city'] : '';
		$out['address_link']     = isset( $arr_indirizzo['place_id'] ) && ! empty( $arr_indirizzo['place_id'] ) ? 'https://www.google.com/maps/place/?q=place_id:' . $arr_indirizzo['place_id'] : '';
	}

	// Recapiti
	if ( $arr_recapiti ) {
		$out['recapiti'] = $arr_recapiti;
	}

	// Chiusura
	if ( $chiusura ) {
		$out['chiusura'] = $chiusura;
	}

	// Budget
	if ( $budget ) {
		$out['budget'] = $budget;
	}

	// Chef
	if ( $chef ) {
		$out['chef'] = $chef;
	}

	// Gallery
	if ( $gallery ) {
		$out['gallery'] = $gallery;
	}

	// Taxonomy `citta`
	if ( $term && ! is_wp_error( $term ) ) {
		$out['city']      = $term->name;
		$out['city_link'] = get_term_link( $term );
	}

	return wp_parse_args( $out, $defaults );
}

function ad_get_post_type( $post = null ) {

	$post = get_post( $post );
	if ( ! $post ) {
		return false;
	}

	if ( get_field( 'post-video-id', $post->ID ) && $post->post_type === 'ricetta' ) {
		return 'videoricetta';
	}

	return $post->post_type;

}

/*
 * Check if object is in menu <menu term name>
 * Returns: bool
 */
function is_object_in_menu( $menu = '' ) {
	if ( empty( $menu ) ) {
		return false;
	}
	$menu_object = wp_get_nav_menu_items( $menu );
	$menu_items  = wp_list_pluck( $menu_object, 'object_id' );
	$object_id   = get_queried_object_id();

	return in_array( (int) $object_id, $menu_items );
}

/**
 * Get the fonte for the post
 *
 * @param $post
 * @param string $before
 * @param string $after
 *
 * @return false|string
 */
function ad_get_post_source( $post = null ) {

	$res  = '';
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$names = get_field( 'not-fonte', $post->ID );
	$urls  = get_field( 'not-urlfonte', $post->ID );

	if ( ! $names ) {
		return '';
	}

	$names_array = explode( "|", $names );
	$urls_array  = explode( "|", $urls );
	$out         = array();

	foreach ( $names_array as $key => $name ) {

		if ( ! $name ) {
			continue;
		}

		$url = isset( $urls_array[ $key ] ) ? $urls_array[ $key ] : '';

		if ( filter_var( $url, FILTER_VALIDATE_URL ) === false ) {
			$url = '';
		}

		if ( $name && $url ) {
			$out[] = sprintf( '<li><a href="%s" target="_blank" rel="nofollow noopener">%s</a></li>', esc_url( $url ), esc_html( $name ) );
		}

		if ( $name && ! $url ) {
			$out[] = sprintf( '<li>%s</li>', $name );
		}

	}

	if ( count( $out ) > 0 ) {
		$res = '<div class="field-list">
				<ul>
					<li><span>FONTE</span></li>';
		$res .= implode( '', $out );
		$res .= '</ul>
			</div>';
	}

	return $res;
}

/**
 * Get the fonte for the post
 *
 * @param $post
 * @param string $before
 * @param string $after
 *
 * @return false|string
 */
function ad_get_image_source( $post = null ) {

	$res  = '';
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$names = get_field( 'not-ext-galleria', $post->ID );
	$urls  = get_field( 'not-ext-urlgalleria', $post->ID );

	if ( ! $names ) {
		return '';
	}

	$names_array = explode( "|", $names );
	$urls_array  = explode( "|", $urls );
	$out         = array();

	foreach ( $names_array as $key => $name ) {

		if ( ! $name ) {
			continue;
		}

		$url = isset( $urls_array[ $key ] ) ? $urls_array[ $key ] : '';

		if ( filter_var( $url, FILTER_VALIDATE_URL ) === false ) {
			$url = '';
		}

		if ( $name && $url ) {
			$out[] = sprintf( '<li><a href="%s" target="_blank" rel="nofollow noopener">%s</a></li>', esc_url( $url ), esc_html( $name ) );
		}

		if ( $name && ! $url ) {
			$out[] = sprintf( '<li>%s</li>', $name );
		}

	}

	if ( count( $out ) > 0 ) {
		$res = '<div class="field-list">
				<ul>
					<li><span>IMMAGINE</span></li>';
		$res .= implode( '', $out );
		$res .= '</ul>
			</div>';
	}

	return $res;

}

/**
 * Get post type to search
 */
function ad_get_post_type_to_search( $search_type ) {

	if ( $search_type === 'all' || $search_type === 'ad' || $search_type === 'page-404' ) {
		return array(
			'post',
			'news',
			'ricetta',
			'menu',
			'cocktail',
			'locale',
			HTML_GALLERY_POST_TYPE,
			HTML_PODCAST_POST_TYPE
		);
	}

	if ( $search_type === 'page-citta' ) {
		return array( 'locale' );
	}

	if ( $search_type === 'page-news' ) {
		return array( 'news' );
	}

	if ( $search_type === 'page-lifestyle' ) {
		return array(
			'post',
			'news',
			HTML_GALLERY_POST_TYPE,
			HTML_PODCAST_POST_TYPE
		);
	}

	if ( $search_type === 'ristorante' ) {
		return array( 'locale' );
	}

	if ( $search_type === 'page-ricetta' || $search_type === 'ricetta' ) {
		return array( 'ricetta', 'menu' );
	}

	return $search_type;
}

/**
 * Get ingredienti singolar or plural word based on quantity
 *
 * @param string $ingrediente Name of the ingredient
 * @param int $quantity Quantity of the ingredient
 *
 * @return false|string
 */
function agrodolce_get_ingredienti_plural( string $ingrediente = '', int $quantity = 1 ) {
	$items = array(
		'g'           => array( 'singular' => 'g', 'plural' => 'g' ),
		'kg'          => array( 'singular' => 'kg', 'plural' => 'kg' ),
		'ml'          => array( 'singular' => 'ml', 'plural' => 'ml' ),
		'cl'          => array( 'singular' => 'cl', 'plural' => 'cl' ),
		'dl'          => array( 'singular' => 'dl', 'plural' => 'dl' ),
		'l'           => array( 'singular' => 'l', 'plural' => 'l' ),
		'oz'          => array( 'singular' => 'oz', 'plural' => 'oz' ),
		'albume'      => array( 'singular' => 'albume', 'plural' => 'albumi' ),
		'bacca'       => array( 'singular' => 'bacca', 'plural' => 'bacche' ),
		'bicchiere'   => array( 'singular' => 'bicchiere', 'plural' => 'bicchieri' ),
		'bicchierino' => array( 'singular' => 'bicchierino', 'plural' => 'bicchierini' ),
		'bustina'     => array( 'singular' => 'bustina', 'plural' => 'bustine' ),
		'cespo'       => array( 'singular' => 'cespo', 'plural' => 'cespi' ),
		'ciuffo'      => array( 'singular' => 'ciuffo', 'plural' => 'ciuffi' ),
		'cucchiaio'   => array( 'singular' => 'cucchiaio', 'plural' => 'cucchiai' ),
		'cucchiaino'  => array( 'singular' => 'cucchiaino', 'plural' => 'cucchiaini' ),
		'fetta'       => array( 'singular' => 'fetta', 'plural' => 'fette' ),
		'filetto'     => array( 'singular' => 'filetto', 'plural' => 'filetti' ),
		'fill up'     => array( 'singular' => 'fill up', 'plural' => 'fill up' ),
		'foglia'      => array( 'singular' => 'foglia', 'plural' => 'foglie' ),
		'foglio'      => array( 'singular' => 'foglio', 'plural' => 'fogli' ),
		'gambo'       => array( 'singular' => 'gambo', 'plural' => 'gambi' ),
		'goccia'      => array( 'singular' => 'goccia', 'plural' => 'gocce' ),
		'manciata'    => array( 'singular' => 'manciata', 'plural' => 'manciate' ),
		'mazzo'       => array( 'singular' => 'mazzo', 'plural' => 'mazzi' ),
		'noce'        => array( 'singular' => 'noce', 'plural' => 'noci' ),
		'pezzo'       => array( 'singular' => 'pezzo', 'plural' => 'pezzi' ),
		'pizzico'     => array( 'singular' => 'pizzico', 'plural' => 'pizzichi' ),
		'rametto'     => array( 'singular' => 'rametto', 'plural' => 'rametti' ),
		'scorza'      => array( 'singular' => 'scorza', 'plural' => 'scorze' ),
		'spicchio'    => array( 'singular' => 'spicchio', 'plural' => 'spicchi' ),
		'stecca'      => array( 'singular' => 'stecca', 'plural' => 'stecche' ),
		'stimma'      => array( 'singular' => 'stimma', 'plural' => 'stimmi' ),
		'tazza'       => array( 'singular' => 'tazza', 'plural' => 'tazze' ),
		'tazzina'     => array( 'singular' => 'tazzina', 'plural' => 'tazzine' ),
		'top'         => array( 'singular' => 'top', 'plural' => 'top' ),
		'tuorlo'      => array( 'singular' => 'tuorlo', 'plural' => 'tuorli' ),
		'vasetto'     => array( 'singular' => 'vasetto', 'plural' => 'vasetti' )
	);

	if ( ! $ingrediente ) {
		return false;
	}

	if ( ! isset ( $items[ $ingrediente ] ) || empty( $items[ $ingrediente ] ) ) {
		return false;
	}

	if ( $quantity > 1 ) {
		return $items[ $ingrediente ]['plural'];
	}

	return $items[ $ingrediente ]['singular'];
}

/**
 * Old decrypt function to save video
 *
 * @param $str
 *
 * @return false|string
 */
function video_id_decrypt( $str ) {
	$key    = 'jiR2834jnADd093bnd293';
	$str    = base64_decode( $str );
	$result = '';
	for ( $i = 0; $i < strlen( $str ); $i ++ ) {
		$char    = substr( $str, $i, 1 );
		$keychar = substr( $key, ( $i % strlen( $key ) ) - 1, 1 );
		$char    = chr( ord( $char ) - ord( $keychar ) );
		$result  .= $char;
	}
	$result = substr( $result, 2, - 2 );

	return $result;
}

/**
 * Return the label for cards (speciale, tag taxonomy or category). To use in the loop
 *
 * @param string $class optional Class to add to the returned anchor tag. Default none
 * @param string $type optional Type of the link: name or link. Default link
 *
 * @return bool|string
 */
function tbm_agrodolce_get_rubrica() {
	global $post;
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}
	$out = array();

	$terms         = wp_get_object_terms( $post->ID, 'rubrica' );
	$template      = '<label class="category">%s</label>';
	$link_template = '<a title="%s" href ="%s">%s</a>';

	if ( is_wp_error( $terms ) || empty( $terms ) ) {
		return false;
	}

	foreach ( $terms as $term ) {
		$out[] = sprintf( $link_template, $term->name, get_term_link( $term->term_id ), $term->name );
	}

	return sprintf( $template, join( '', array_slice( $out, 0, 1 ) ) );

}

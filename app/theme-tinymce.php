<?php
add_filter( 'mce_external_plugins', "tinyplugin_register" );
add_filter( 'mce_buttons', 'tinyplugin_add_button', 0 );
add_action( 'admin_print_footer_scripts', '_cax_editorhtml', 100 );

function tinyplugin_add_button( $buttons ) { 
    array_push( $buttons, '|', 'cax_important_phrase' );
    array_push( $buttons, '|', 'cax_last_update' );

    return $buttons;
}

function tinyplugin_register( $plugin_array ) { 
    $plugin_array['cax_important_phrase'] = '';
    $plugin_array['cax_last_update']      = '';

    return $plugin_array;
}

function _cax_editorhtml() {
    $req_uri = $_SERVER['REQUEST_URI'];
    if ( strpos( $req_uri, "post-new.php" ) !== false || ( strpos( $req_uri, "post.php" ) !== false && $_REQUEST['action'] == "edit" ) ) { 
        ?>
        <script type="text/javascript">
			function cax_important_phrase() {
				return "<p>[BANNER_CODE]</p>";
			}

			(function() {
				tinymce.create('tinymce.plugins.cax_important_phrase', {
					init : function(ed, url){
						ed.addButton('cax_important_phrase', {
							title : 'Includi una frase di rilievo',
							onclick : function() {
								var textprompt = prompt("Inserisci la frase");
								if (textprompt) {
									//tinyMCE.execInstanceCommand("mce_editor_0", "mceFocus");
									ed.execCommand('mceInsertContent', false, '[PHRASE value="'+textprompt+'"]' );
									}
							},
							image: "<?php echo get_template_directory_uri() . '/../dist/images'?>" + "/important_phrase.png"
						});
					},
				});
				tinymce.PluginManager.add('cax_important_phrase', tinymce.plugins.cax_important_phrase);
			})();

			(function() {
				tinymce.create('tinymce.plugins.cax_last_update', {
					init : function(ed, url){
									ed.addButton('cax_last_update', {
										title : 'Inserisci un aggiornamento',
										onclick : function() {
											var textprompt = prompt("Inserisci la frase");
											if (textprompt) {
												//tinyMCE.execInstanceCommand("mce_editor_0", "mceFocus");
												ed.execCommand('mceInsertContent', false, '[UPDATE]'+textprompt+'[/UPDATE]' );
												}


										},
										image: "<?php echo get_template_directory_uri() . '/../dist/images'?>"  + "/last_update.png"
									});
					},
				});
				tinymce.PluginManager.add('cax_last_update', tinymce.plugins.cax_last_update);
			})();


            QTags.addButton('cax_important_phrase', 'Important Phrase', '[PHRASE]', null, null, 'Inserisci la frase da porre in rilievo');
            QTags.addButton('cax_last_update', 'Last update', '[UPDATE]', null, null, 'Inserisci un aggiornamento');
        </script>
    <?php }

}
?>

<?php

/* Rewrite author slug*/
add_action( 'init', function () {
	global $wp_rewrite;
	$wp_rewrite->author_base      = 'autore';
	$wp_rewrite->author_structure = '/' . $wp_rewrite->author_base . '/%author%';
} );

/**
 * action to replace taxonomy urls
 */
add_action( 'init', function () {

	$taxonomies = [ 'citta', 'categoria_ricetta', 'categoria_cocktail', 'filters' ];

	foreach ( $taxonomies as $tax ) {
		$terms = get_terms( $tax, array( 'hide_empty' => false ) );
		if ( sizeof( $terms ) ) {
			foreach ( $terms as $term ) {
				add_rewrite_rule( '^' . $term->slug . '(?:/page/([0-9]+))?/?$', 'index.php?' . $term->taxonomy . '=' . $term->slug . '&paged=$matches[1]', 'top' );
				add_rewrite_rule( '^' . $term->slug . '/(feed|rdf|rss|rss2|atom|flipboard|gp)/?$', 'index.php?' . $term->taxonomy . '=' . $term->slug . '&feed=$matches[1]', 'top' );
			}
		}
	}
}, 11 );

<?php
/**
 * Add custom post type to author, tag, feed archive
 *
 * @param $query
 *
 * @return mixed
 */
add_action( 'pre_get_posts', function ( $query ) {

	if ( is_admin() ) {
		return;
	}

	if ( get_query_var( 'post_type' ) ) {
		return;
	}

	$post_types = array(
		HTML_GALLERY_POST_TYPE,
		'post',
		'locale',
		'ricetta',
		'cocktail'
	);

	// Author archive
	if ( is_author() && $query->is_main_query() ) {
		$query->set( 'post_type', $post_types );
	}

	// Category and tag Archive
	if ( ( is_category() || is_tag() ) && $query->is_main_query() ) {

		// Handle post_type:  paginated results
		$query->set( 'post_type', $post_types );
	}

	// Feed
	if ( is_feed() && $query->is_main_query() ) {
		$query->set( 'post_type', $post_types );
		$query->set( 'posts_per_page', 20 );
	}

	return;
} );


/**
 * Add custom post type to search query
 *
 * @param $wp_query
 */
add_action( 'pre_get_posts', function ( $query ) {

	// If we are in admin area, exit
	if ( is_admin() ) {
		return;
	}

	// If is not main query, exit
	if ( ! $query->is_main_query() ) {
		return;
	}

	// If is not a search, exit
	if ( ! $query->is_search() ) {
		return;
	}

	// Common search
	$search_type = isset( $_GET['search_type'] ) && $_GET['search_type'] ? sanitize_text_field( $_GET['search_type'] ) : '';

	if ( $search_type ) {
		$query->set( 'post_type', ad_get_post_type_to_search( $search_type ) );
	}

	// Innersearch
	$innersearch_type = isset( $_GET['innersearch_type'] ) && ! empty( $_GET['innersearch_type'] ) ? sanitize_text_field( $_GET['innersearch_type'] ) : '';

	if ( $innersearch_type ) {
		$query->set( 'post_type', ad_get_post_type_to_search( $innersearch_type ) );
	}
} );

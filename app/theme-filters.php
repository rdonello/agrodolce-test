<?php



/**
 * Disable BOX_MOBILE_INSIDE_CONTENT
 */
add_filter( 'tbm_disable_inside_box', '__return_true' );

/**
 * Disable TEADS
 */
add_filter( 'tbm_disable_teads', '__return_true' );

/**
 * Enable INCONTENT
 */
add_filter( 'tbm_disable_incontent', '__return_false' );

/**
 * Filter the evolution feed tag
 */
add_filter( 'tbm_evolution_feed_template_tags', function ( $tags, $post ) {
	$image                   = tbm_get_the_post_thumbnail_url( $post, array( 640, 360 ) );
	$tags['feed_post_image'] = $image;

	return $tags;
}, 10, 2 );

/**
 * Enable evolution infinite in news
 */
add_filter( 'tbm_enabled_cr_post_type', function () {
	return array( 'post', 'news' );
} );

/**
 * Filter the post type to show in evolution infinite scroll
 */
add_filter( 'tbm_queried_cr_post_type', function () {
	return array( 'post', 'news' );
} );

/**
 * Add the content to Evolution feed template
 */
add_filter( 'tbm_evolution_feed_template', function () {
	$template = file_get_contents( HTML_TBM_COMMON_TEMPLATE_PATH . '/custom/evolution_template.html' );
	$template = tbm_minify_html( $template, true );

	return $template;
} );

/**
 * Filter AMP image in schema.org
 */
add_filter( 'amp_post_template_metadata', function ( $metadata = array(), $post = 0 ) {

	if ( ! $post ) {
		return $metadata;
	}

	/**
	 * Check the image size
	 */
	$thumb_id = get_post_thumbnail_id( $post );

	if ( function_exists( 'tbm_get_thumbor_img_original_size' ) ) {
		$image = tbm_get_thumbor_img_original_size( $thumb_id );

		if ( ! $image || $image[0] < 1200 ) {
			$image_orig = tbm_get_the_post_thumbnail_url( $post, array(
				1200,
				800
			) );
			$image_1x1  = tbm_get_the_post_thumbnail_url( $post, array(
				1200,
				1200
			) );
			$image_4x3  = tbm_get_the_post_thumbnail_url( $post, array(
				1200,
				900
			) );
			$image_16x9 = tbm_get_the_post_thumbnail_url( $post, array(
				1200,
				675
			) );
		} else {
			$image_orig = tbm_get_the_post_thumbnail_url( $post, array(
				$image[0],
				$image[1]
			) );
			$image_1x1  = tbm_get_the_post_thumbnail_url( $post, array(
				1200,
				1200
			) );
			$image_4x3  = tbm_get_the_post_thumbnail_url( $post, array(
				$image[0],
				$image[0] * 0.75
			) );
			$image_16x9 = tbm_get_the_post_thumbnail_url( $post, array(
				$image[0],
				( $image[0] * 9 ) / 16
			) );
		}

		$metadata['image'] = array( $image_orig, $image_1x1, $image_4x3, $image_16x9 );

		return $metadata;
	}

	$image_orig        = tbm_get_the_post_thumbnail_url( $post, array(
		1200,
		800
	) );
	$image_1x1         = tbm_get_the_post_thumbnail_url( $post, array(
		1200,
		1200
	) );
	$image_4x3         = tbm_get_the_post_thumbnail_url( $post, array(
		1200,
		900
	) );
	$image_16x9        = tbm_get_the_post_thumbnail_url( $post, array(
		1200,
		675
	) );
	$metadata['image'] = array( $image_orig, $image_1x1, $image_4x3, $image_16x9 );

	return $metadata;
}, 10, 2 );

/**
 * Disable more in excerpt
 */
add_filter( 'excerpt_more', function ( $more ) {
	return '';
} );

/**
 * Replace whitespace with %20 in images
 */
add_filter( 'wp_get_attachment_image_src', function ( $image ) {
	if ( isset( $image[0] ) && ! empty( $image[0] ) ) {
		$image[0] = str_replace( ' ', '%20', $image[0] );
	}

	return $image;
} );

/**
 * Remove <p> from excerpt
 */
remove_filter( 'the_excerpt', 'wpautop' );

/**
 * Remove <p> from archive description
 */
add_filter( 'get_the_archive_description', function ( $title ) {
	if ( is_paged() ) {
		return '';
	}

	return strip_tags( $title, '<strong><a><em>' );
}, 10, 3 );

/**
 * Change the title with "titolo strillo". Only in HP
 */
add_filter( 'the_title', function ( $title, $id ) {

	if ( ! $id ) {
		return $title;
	}

	$strillo = get_field( 'titolo-strillo', $id );

	if ( ! $strillo ) {
		return $title;
	}

	if ( is_front_page() ) {
		return $strillo;
	}

	return $title;

}, 10, 2 );

/**
 * Add excerpt to page
 */
add_post_type_support( 'page', 'excerpt' );

/**
 * Filters 'img' elements in post content to add image credit (copy of wp_make_content_images_responsive)
 *
 * @param string $content The raw post content to be filtered.
 *
 * @return string Converted content with 'srcset' and 'sizes' attributes added to images.
 */
add_filter( 'the_content', function ( $content ) {
	if ( ! preg_match_all( '/<img [^>]+>/', $content, $matches ) ) {
		return $content;
	}

	$selected_images = $attachment_ids = array();

	foreach ( $matches[0] as $image ) {
		if ( false === strpos( $image, ' srcset=' ) && preg_match( '/wp-image-([0-9]+)/i', $image, $class_id ) &&
		     ( $attachment_id = absint( $class_id[1] ) ) ) {

			/*
			 * If exactly the same image tag is used more than once, overwrite it.
			 * All identical tags will be replaced later with 'str_replace()'.
			 */
			$selected_images[ $image ] = $attachment_id;
			// Overwrite the ID when the same image is included more than once.
			$attachment_ids[ $attachment_id ] = true;
		}
	}

	if ( count( $attachment_ids ) > 1 ) {
		/*
		 * Warm the object cache with post and meta information for all found
		 * images to avoid making individual database calls.
		 */
		_prime_post_caches( array_keys( $attachment_ids ), false, true );
	}

	foreach ( $selected_images as $image => $attachment_id ) {
		$image_credit = tbm_get_thumb_credit( $attachment_id );

		if ( has_shortcode( $content, 'caption' ) && $image_credit ) {
			$content = str_replace( '[/caption]', ' &copy; ' . $image_credit . '[/caption]', $content );
		}

		if ( ! has_shortcode( $content, 'caption' ) && $image_credit ) {
			$content = str_replace( $image, '<figure>' . $image . '<figcaption>&copy; ' . $image_credit . '</figcaption></figure>', $content );
		}
	}

	return $content;
}, 1 );

/**
 * Change default newsletter Jquery dependency
 */
add_filter( 'tbm_newsletter_js_dep', function ( $dep ) {

	$dep = array( 'tbm' );

	return $dep;

} );

/**
 * Added ajax latest news call to purge list (the url is hard cached in Cloudflare)
 */
add_filter( 'tbm_add_url_to_invalidate', function ( array $urls, $id ) {

	if ( ! $id ) {
		return $urls;
	}

	$post_type = get_post_type( $id );

	if ( ! $post_type ) {
		return $urls;
	}

	if ( $post_type === 'post' ) {
		$urls[] = 'https://www.agrodolce.it/lifestyle/';
	}

	if ( $post_type === 'news' ) {
		$urls[] = 'https://www.agrodolce.it/news/';
	}

	if ( $post_type === 'locale' ) {
		$urls[] = 'https://www.agrodolce.it/guida-agrodolce/';
	}

	$urls[] = get_post_type_archive_link( $post_type );

	return $urls;
}, 10, 2 );

/**
 * YOAST Enable pretty print in development
 */
if ( defined( 'WP_ENV' ) && WP_ENV !== 'production' ) {
	add_filter( 'yoast_seo_development_mode', '__return_true' );
}

/**
 * YOAST change search schema path
 */
add_filter( 'wpseo_json_ld_search_url', function () {
	return 'https://www.agrodolce.it/?s={search_term_string}';
} );

/**
 * YOAST Disable schema.org if RECIPE name OR RECIPE image are missing
 */
add_filter( 'wpseo_json_ld_output', function ( $show ) {
	if ( is_singular( array( 'ricetta', 'cocktail' ) ) ) {
		$recipeId    = get_post()->ID;
		$recipe_name = ! empty( get_field( 'nome_ricetta', $recipeId ) ) ? get_field( 'nome_ricetta', $recipeId ) : get_post()->post_title;
		$image       = tbm_get_the_post_thumbnail_url( $recipeId, array( 1200, 0 ) );
		if ( ! $recipe_name || ! $image ) {
			return false;
		}
	}

	return $show;
} );

/**
 * YOAST update schema
 */
add_filter( 'wpseo_schema_graph_pieces', function ( $pieces, $context ) {

	if ( is_singular( array( 'ricetta', 'cocktail' ) ) ) {
		$pieces[] = new \App\Controllers\SchemaRecipe( $context );
	}

	return $pieces;

}, 11, 2 );

/**
 * YOAST Add Author to Recipe schema
 */
add_filter( 'wpseo_schema_needs_author', function ( $is_needed ) {
	if ( is_singular( array( 'ricetta', 'cocktail' ) ) ) {
		return true;
	}

	return $is_needed;
} );

/**
 * YOAST Change breadcrumbs link
 */
add_filter( 'wpseo_breadcrumb_links', function ( $links ) {

	// Add ingredienti page to ingredienti taxonomy
	if ( is_tax( 'ingredienti' ) ) {
		$home = array(
			array(
				'url'  => get_page_link( get_page_by_path( 'ingredienti' ) ),
				'text' => 'Ingredienti'
			)
		);
		array_splice( $links, 1, 0, $home );
	}

	// Add citta page to citta taxonomy
	if ( is_tax( 'citta' ) ) {
		$home = array(
			array(
				'url'  => get_page_link( get_page_by_path( 'citta' ) ),
				'text' => 'Citt&agrave;'
			)
		);
		array_splice( $links, 1, 0, $home );
	}

	return $links;
} );


/**
 * Filter the author when "Autore fittizio" is defined
 */
add_filter( 'tbm_get_the_author', function ( $author ) {

	$fake_author = stripslashes( get_field( 'follow_author_name' ) );

	if ( $fake_author ) {
		$author = sprintf( '<span class="author vcard">%s</span>', $fake_author );
	}

	return $author;
} );

/**
 * Filter the max-width of the ACF WYSIWYG textarea to avoid excessive image dimension
 */
add_filter( 'tiny_mce_before_init', function ( $mceInit ) {
	$styles = '.acf_content.post-type-ricetta img {max-width:640px}';
	if ( isset( $mceInit['content_style'] ) ) {
		$mceInit['content_style'] .= ' ' . $styles . ' ';
	} else {
		$mceInit['content_style'] = $styles . ' ';
	}

	return $mceInit;
} );

add_filter( 'wp_calculate_image_srcset_meta', '__return_false' );
add_filter( 'wp_lazy_loading_enabled', '__return_false' );

/**
 * Filter images in content
 */
add_filter( 'the_content', function ( $content ) {
	$new_content = \App\Controllers\ThumborRewrite::getThumborImage( $content );

	if ( $new_content ) {
		return $new_content;
	}

	return $content;
} );

/**
 * Filter to strip parent categories from category urls
 * NB: DOES YOAST PLUGIN MISS THIS???
 */
add_filter( 'category_rewrite_rules', function ( $category_rewrite ) {

	$categories = get_categories( array( 'hide_empty' => false ) );
	foreach ( $categories as $category ) {
		$category_nicename                                                                        = $category->slug;
		$category_rewrite[ '(' . $category_nicename . ')/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$' ] = 'index.php?category_name=$matches[1]&feed=$matches[2]';
		$category_rewrite[ '(' . $category_nicename . ')/page/?([0-9]{1,})/?$' ]                  = 'index.php?category_name=$matches[1]&paged=$matches[2]';
		$category_rewrite[ '(' . $category_nicename . ')/?$' ]                                    = 'index.php?category_name=$matches[1]';
	}
	// Redirect support from Old Category Base
	global $wp_rewrite;
	$old_base                            = $wp_rewrite->get_category_permastruct();
	$old_base                            = str_replace( '%category%', '(.+)', $old_base );
	$old_base                            = trim( $old_base, '/' );
	$category_rewrite[ $old_base . '$' ] = 'index.php?category_redirect=$matches[1]';

	return $category_rewrite;
}, 11 );

add_filter( 'category_link', function ( $catlink, $category_id ) {
	$category = get_category( $category_id );
	if ( is_wp_error( $category ) ) {
		return $category;
	}
	$category_nicename = $category->slug;

	$catlink = trailingslashit( get_option( 'home' ) ) . user_trailingslashit( $category_nicename, 'category' );

	return $catlink;
}, 10, 2 );

/**
 * Filter to add 'category_redirect' query variable
 */
add_filter( 'query_vars', function ( $public_query_vars ) {
	$public_query_vars[] = 'category_redirect';

	return $public_query_vars;
} );

/**
 * Filter to redirect if 'category_redirect' is set
 */
add_filter( 'request', function ( $query_vars ) {
	if ( isset( $query_vars['category_redirect'] ) ) {
		$catlink = trailingslashit( get_option( 'home' ) ) . user_trailingslashit( $query_vars['category_redirect'], 'category' );
		status_header( 301 );
		header( "Location: $catlink" );
		exit();
	}

	return $query_vars;
} );

/**
 * Filter to replace taxonomy links
 *
 * @param $link
 * @param $term
 * @param $taxonomy
 *
 * @return mixed
 */
add_filter( 'term_link', function ( $link, $term, $taxonomy ) {
	if ( in_array( $taxonomy, [ 'citta', 'categoria_ricetta', 'filters', 'categoria_cocktail' ] ) ) {
		return str_replace( $taxonomy . '/', '', $link );
	}

	return $link;
}, 10, 3 );

/**
 * Filter the menu title in menu archive
 */
add_filter( 'the_title', function ( $title, $id ) {

	if ( ! is_post_type_archive( 'menu' ) ) {

		return $title;
	}

	/** @var string $title_new The title from ACF field */
	$title_new = get_field( 'titolo_breve', $id );

	if ( $title_new ) {
		return $title_new;
	}

	return $title;
}, 10, 2 );

/**
 * Filter paginate links only in search page
 */
add_filter( 'paginate_links', function ( $link ) {

	if ( is_search() ) {
		$kw    = $_GET['s'] ?? '';
		$inner = $_GET['innersearch_type'] ?? '';
		$type  = $_GET['search_type'] ?? '';

		$link = add_query_arg( array( 's' => $kw, 'innersearch_type' => $inner, 'search_type' => $type ), $link );
	}

	return $link;

} );

/**
 * Disable adv VIDEO in pages with editorial video
 */
add_filter( 'tbm_tribooadv', function ( $tribooadv ) {

	// Act only in single page
	if ( ! is_single() ) {
		return $tribooadv;
	}

	// Check single video
	if ( is_singular( HTML_VIDEO_POST_TYPE ) ) {
		$tribooadv['disableVideo'] = true;
	}

	// Check video shortcode
	$content = get_post_field( 'post_content' );

	if ( strpos( $content, 'ghrandvideo' ) !== false || strpos( $content, 'ghvideo' ) !== false ) {
		$tribooadv['disableVideo'] = true;
	}

	// Check editorial video in recipe
	$editorial_video = get_field( "post-video-id", get_the_ID() );

	if ( $editorial_video ) {
		$tribooadv['disableVideo']    = true;
		$tribooadv['seedtagDisabled'] = true;
	}

	return $tribooadv;

} );

/**
 * Set the correct canonical in menu pagination
 */
add_filter( 'wpseo_canonical', function ( $canonical ) {

	if ( ! is_singular( 'menu' ) ) {
		return $canonical;
	}

	$page = ( get_query_var( 'rp' ) ) ? absint( get_query_var( 'rp' ) ) : 1;

	if ( $page > 1 ) {
		return get_permalink() . 'rp/' . $page . '/';
	}

	return $canonical;
} );

/**
 * Add to <body> classes nex page class
 */
add_filter( 'body_class', function ( array $classes ) {
	/** Add class if sidebar is active */
	if ( is_paged() || get_query_var( 'rp' ) ) {
		$classes[] = 'next-page';
	}

	return array_filter( $classes );
} );

/**
 * Force 404 page when needed
 */
add_filter( 'template_include', function ( $template ) {
	if ( ! is_page( 'ingredienti' ) ) {
		return $template;
	}

	$current_page = get_query_var( 'letter' ) ? get_query_var( 'letter' ) : 'a';

	if ( ! preg_match( '/^[a-zA-Z]$/', $current_page, $matches ) ) {
		global $wp_query;

		$wp_query->set_404();

		status_header( 404 );

		$template = get_404_template();
	}

	return $template;

} );

<?php
/**
 * Execute a fast SQL search query (for posts)
 *
 * @param $term string The word or phrase to search for. Required
 * @param $post_type string The post type (exact name or supported name) to search in
 *
 * @return array
 */
function ad_search_posts( $term, $post_type = '' ) {
	global $wpdb;

	// Check $term
	if ( ! $term ) {
		return array();
	}

	// Formatto $term
	$regexp = "[[:<:]]" . $term;
	$where  = "WHERE post_title REGEXP '$regexp'";

	// Set array
	$return_arr = array();

	// Formatto $post_type
	if ( is_array( $post_type ) ) {
		$post_explode    = "'" . implode( "','", $post_type ) . "'";
		$post_type_query = "AND post_type IN ($post_explode)";
	} else {
		$post_type_query = "AND post_type = '$post_type'";
	}

	$gallery = HTML_GALLERY_POST_TYPE;

	/**
	 * If $post_type is not defined, returns a global search
	 */
	if ( ! $post_type ) {
		//Compongo la seconda query
		$query        = "SELECT ID, post_title, post_type, guid, post_date FROM $wpdb->posts $where AND post_type IN ('post','video','$gallery','podcast','glossario','card') ";
		$query        .= " AND post_parent = 0 AND post_status IN ('publish')";
		$query        .= " ORDER BY post_date DESC LIMIT 30";
		$results = $wpdb->get_results( $query );
	} else {
		// Prendo i primi risultati
		$query   = "SELECT ID, post_title, post_type, guid, post_date FROM $wpdb->posts $where $post_type_query ";
		$query   .= " AND post_parent = 0 AND post_status IN ('publish')";
		$query   .= " ORDER BY post_date DESC LIMIT 20";
		$results = $wpdb->get_results( $query );
	}


	// Loop results
	if ( $results ) {
		foreach ( $results as $result ) {
			$row_array['id']    = $result->ID;
			$row_array['title'] = $result->post_title;
			$row_array['type']  = $result->post_type;
			$row_array['data']  = date( 'd/m/Y', strtotime( $result->post_date ) );
			$row_array['link']  = get_permalink( $result->ID );
			array_push( $return_arr, $row_array );
		}

		return $return_arr;
	}

	return array();
}

/**
 * Ajax endpoint for site search
 *
 * @throws Exception
 */
add_action( 'wp_ajax_nopriv_ad_q', 'ad_tbm_ad_q' );
add_action( 'wp_ajax_ad_q', 'ad_tbm_ad_q' );
function ad_tbm_ad_q() {

	$gump = new GUMP();

	$_GET = $gump->sanitize( $_GET );

	$gump->validation_rules( array(
		'search_term' => 'required',
	) );

	$gump->filter_rules( array(
		'search_term'      => 'sanitize_string',
		'search_type'      => 'sanitize_string',
		'innersearch_type' => 'sanitize_string',
	) );

	/**
	 * Run the method to validate data
	 */
	$validated_data = $gump->run( $_GET );

	/**
	 * If data are not valid
	 */
	if ( $validated_data === false ) {
		tbm_gump_send_error_response( $gump );

		exit;
	}

	/**
	 * Set the query
	 */
	$search_type = $validated_data['search_type'] ?: $validated_data['innersearch_type'];

	/**
	 * Send request to Web Service
	 */
	$response = ad_search_posts( $validated_data['search_term'], ad_get_post_type_to_search( $search_type ) );

	/**
	 * Send the response
	 */
	tbm_gump_send_success_response( [
		'results' => $response
	], 'search results', 'search_posts' );

	exit;

}
